package com.testproject.sign.cameratest.camera.util;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;

/**
 * Created by user on 10/30/17.
 */

public class BitmapToFileOpenCV {

    public static final int QUALITY = 100;
    public static final String PREVIEW_FILE_NAME = "preview.png";

    /**
     * @param compression form 0-9, 0 - best quality and speed
     * @return path of the saved bitmap
     */
    public static String saveImageAsPNG(Bitmap image, String path, String fileName, int compression) {
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, fileName + ".png");

        Mat mat = new Mat();
        Utils.bitmapToMat(image, mat);

        MatOfInt compression_params = new MatOfInt();
        compression_params.fromArray(Imgcodecs.CV_IMWRITE_PNG_COMPRESSION, 0);
        Imgcodecs.imwrite(file.getAbsolutePath(), mat, compression_params);

        mat.release();
        compression_params.release();

        return file.getAbsolutePath();
    }

}
