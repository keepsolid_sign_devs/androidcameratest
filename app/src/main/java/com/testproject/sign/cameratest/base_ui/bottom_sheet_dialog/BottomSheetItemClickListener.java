package com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog;


public interface BottomSheetItemClickListener {

    void onItemClick(BottomSheetMenuItem item);

}
