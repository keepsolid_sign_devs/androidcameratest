package com.testproject.sign.cameratest.camera.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.RecyclingImageView;
import com.testproject.sign.cameratest.camera.options.ImageProcessor;
import com.testproject.sign.cameratest.camera.util.PxDpMetrics;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.util.ArrayList;


public class EditImageView extends RecyclingImageView implements View.OnTouchListener {

    private enum Mode {
        RESIZE,
        MOVE,
        NONE
    }

    private static final String LOG_TAG = EditImageView.class.getSimpleName();

    private Path fillPath;

    private ArrayList<Point> mCircles;
    private ArrayList<Point> relativeCorners;

    private boolean showCropCircles = true;
    private static float MIN_DISTANCE_TO_POINT = PxDpMetrics.convertDpToPixel(20);
    private float circleRadius;

    private float strokeWidth;
    private double scaleRatio = 1;
    private Paint mCirclePaint;
    private Paint mCircleBorderPaint;
    private Paint mLinePaint;
    private boolean enableMagnifier = true;

    private boolean showMagnifier = false;
    private Paint magnifierPaint;
    private Matrix magnifierMatrix;
    private BitmapShader magnifierShader;
    private int sizeOfMagnifier = PxDpMetrics.convertDpToPixel(50);

    private Mode mode = Mode.NONE;

    private boolean enableCropProportions = false;

    public static double CROP_ASPECT_RATIO = 3d / 4d; //height / width
    int xTouch;

    int yTouch;
    int defWidth;

    public int bitmapRotation = 360;

    private Point newLeftCornerPoint;
    private Point newRightCornerPoint;

    private int minWidth = 300;

    public EditImageView(Context context) {
        super(context);
        init();
    }

    public EditImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setAdjustViewBounds(true);
        setOnTouchListener(this);
        setDrawingCacheEnabled(true);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        magnifierMatrix = new Matrix();
        fillPath = new Path();
        mCircles = new ArrayList<>(4);
        relativeCorners = new ArrayList<>(4);

        circleRadius = PxDpMetrics.convertDpToPixel(5);
        strokeWidth = PxDpMetrics.convertDpToPixel(2);

        initPaints();

        newLeftCornerPoint = new Point();
        newRightCornerPoint = new Point();
    }

    private void initPaints() {
        mLinePaint = new Paint();
        mCirclePaint = new Paint();
        mCircleBorderPaint = new Paint();
        magnifierPaint = new Paint();

        mLinePaint.setAntiAlias(true);
        mLinePaint.setColor(getResources().getColor(R.color.camera_edit_color));
        mLinePaint.setStyle(Paint.Style.STROKE);
        mLinePaint.setStrokeWidth(strokeWidth);

        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(Color.WHITE);
        getResources().getColor(R.color.camera_edit_color);
        mCirclePaint.setStyle(Paint.Style.FILL);

        mCircleBorderPaint.setAntiAlias(true);
        mCircleBorderPaint.setColor(getResources().getColor(R.color.camera_edit_color));
        mCircleBorderPaint.setStyle(Paint.Style.STROKE);
        mCircleBorderPaint.setStrokeWidth(strokeWidth);

        magnifierPaint.setShader(magnifierShader);
    }

    public boolean isShowCropCircles() {
        return showCropCircles;
    }

    public void setShowCropCircles(boolean showCropCircles) {
        this.showCropCircles = showCropCircles;
        invalidate();
    }

    public void setEnableCropProportions(boolean enableCropProportions) {
        this.enableCropProportions = enableCropProportions;
    }

    // <editor-fold desc="image processing">

    public void findImageCorners(Bitmap bitmap) {
        relativeCorners = ImageProcessor.findObject(bitmap, true);

        setParams(bitmap);

        addCirclesFromRelativeCorners();

        invalidate();
    }

    public void findImageCorners() {
        if (showCropCircles) {
            findImageCorners(getBitmap());
        }
    }

    public void setImageCorners(ArrayList<Point> relativeCorners) {
        this.relativeCorners = relativeCorners;

        if (showCropCircles) {
            setParams(getBitmap());

            addCirclesFromRelativeCorners();

            invalidate();
        }
    }

    public void resetImageCorners() {
        if (showCropCircles) {
            if (enableCropProportions) {
                setDefaultCornersWithProportions();
            } else {
                setDefaultCorners();
            }

            setParams(getBitmap());
            addCirclesFromRelativeCorners();

            invalidate();
        }
    }

    private void setDefaultCorners() {
        relativeCorners = new ArrayList<>();

        Bitmap bitmap = getBitmap();
        ImageProcessor.addDefaultCorners(relativeCorners, bitmap.getWidth(), bitmap.getHeight());
    }

    private void setDefaultCornersWithProportions() {
        relativeCorners = new ArrayList<>();

        Bitmap bitmap = getBitmap();
        ImageProcessor.addDefaultCornersWithProportions(relativeCorners, bitmap.getWidth(),
                bitmap.getHeight(), CROP_ASPECT_RATIO);
    }

    private Bitmap getBitmap() {
        return ((BitmapDrawable) this.getDrawable()).getBitmap();
    }

    private void addCirclesFromRelativeCorners() {
        mCircles.clear();

        float viewW = getBitmap().getWidth();
        float viewH = getBitmap().getHeight();

        for (Point point : relativeCorners) {
            Point temp = new Point((point.x * viewW * scaleRatio), ((point.y * viewH * scaleRatio)));
            Log.v(LOG_TAG, temp.toString());
            mCircles.add(temp);
        }
    }

    private void setParams(Bitmap bitmap) {
        int imgW = bitmap.getWidth();
        int imgH = bitmap.getHeight();
        Log.v(LOG_TAG, "imgW = " + imgW + ", imgH = " + imgH);

        View container = ((View) getParent());
        float viewW = container.getWidth();
        float viewH = container.getHeight();
        Log.v(LOG_TAG, "viewW = " + viewW + ", viewH = " + viewH);

        if ((double) bitmap.getWidth() / (double) bitmap.getHeight() < (double) viewW / (double) viewH) {
            double k = (double) bitmap.getWidth() / (double) bitmap.getHeight();
            defWidth = (int) (k * viewH);
            getLayoutParams().width = defWidth;
            getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            scaleRatio = (double) viewH / (double) imgH;
            requestLayout();
        } else {
            getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            scaleRatio = (double) viewW / (double) imgW;
            requestLayout();
        }

        minWidth = (int) (viewW / 3);
        Log.v(LOG_TAG, "scaleRatio = " + scaleRatio);
    }


//    public ArrayList<Point> getCorners() {
//        ArrayList<Point> tmpCorners = new ArrayList<>(4);
//        for (Point point : mCircles) {
//            Point newPoint = point.clone();
//            newPoint.x /= scaleRatio;
//            newPoint.y = (point.y) / scaleRatio;
//            tmpCorners.add(newPoint);
//        }
//        return tmpCorners;
//    }

    public ArrayList<Point> getCorners() {
        ArrayList<Point> tmpCorners = new ArrayList<>(4);
        for (Point point : mCircles) {
            Point newPoint = point.clone();
            newPoint.x /= getWidth();
            newPoint.y /= getHeight();
            tmpCorners.add(newPoint);
        }
        return tmpCorners;
    }

    public Bitmap rotateImageView(int angle) {
        return rotateBitmap(angle, getBitmap(), true);
    }

    public Bitmap rotateBitmap(int angle, Bitmap bitmap, boolean showResult) {
        bitmapRotation += angle;

        Mat src = new Mat();
        Mat dst = new Mat(bitmap.getHeight(), bitmap.getWidth(), CvType.CV_8UC4);

        Utils.bitmapToMat(bitmap, src);

        switch (angle) {
            case -270:
            case 90:
                Core.flip(src.t(), dst, 1);
                break;
            case -180:
            case 180:
                Core.flip(src, dst, -1);
                break;
            case -90:
            case 270:
                Core.flip(src.t(), dst, 0);
                break;
            case 360:
            case 0:
                dst = src;
                break;
        }

        Bitmap output = Bitmap.createBitmap(dst.width(), dst.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(dst, output);

        if (showResult) {
            setImageBitmap(output);
        }
        bitmap.recycle();
        return output;
    }

    public void resetBitmapRotation() {
        rotateImageView(bitmapRotation % 360);
        bitmapRotation = 360;
    }

    public void rotate(int degree) {
        setPivotX(getWidth() / 2);
        setPivotY(getHeight() / 2);
        setRotation(degree);
    }

    public Bitmap cropImage(ArrayList<Point> corners) {
        Bitmap currentImage = getBitmap();

        setParams(currentImage);

        for (Point item : corners) {
            item.x /= scaleRatio;
            item.y /= scaleRatio;
        }

        Bitmap croppedImg = ImageProcessor.warp(currentImage, corners);
        Log.v(LOG_TAG, "croppedImgW = " + croppedImg.getWidth() + ", croppedImgH = " + croppedImg.getHeight());
        setImageBitmap(croppedImg);

        return croppedImg;
    }

    public Bitmap adjustContrast(double contrast) {
        Bitmap currentImage = getBitmap();
        Bitmap editedImg = ImageProcessor.adjustContrast(currentImage, contrast);
        setImageBitmap(editedImg);
        return editedImg;
    }

    public Bitmap adjustBrightness(double brightness) {
        Bitmap currentImage = getBitmap();
        Bitmap editedImg = ImageProcessor.adjustBrightness(currentImage, brightness);
        setImageBitmap(editedImg);
        return editedImg;
    }

    public Bitmap cvt() {
        Bitmap currentImage = getBitmap();
        Bitmap editedImg = ImageProcessor.cvt(currentImage);
        setImageBitmap(editedImg);
        return editedImg;
    }

    public Bitmap binarization() {
        Bitmap currentImage = getBitmap();
        Bitmap editedImg = ImageProcessor.binarization(currentImage);
        setImageBitmap(editedImg);
        return editedImg;
    }

    // </editor-fold>

    // <editor-fold desc="draw">

    @Override
    public void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);
        if (showCropCircles && !mCircles.isEmpty()) {
            mCircles = ImageProcessor.getConvexHull(mCircles);
            drawLines(canvas);
            drawCircles(canvas);
        }
//        if (showMagnifier) {
//            Bitmap bitmap = getDrawingCache(true);
//            magnifierShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
//
//            magnifierPaint.setShader(magnifierShader);
//            magnifierMatrix.reset();
//            magnifierMatrix.postScale(2f, 2f, xTouch, yTouch);
//            magnifierPaint.getShader().setLocalMatrix(magnifierMatrix);
//            RectF src = new RectF(xTouch-sizeOfMagnifier, yTouch-sizeOfMagnifier, xTouch+sizeOfMagnifier, yTouch+sizeOfMagnifier);
//            RectF dst = new RectF(0, 0, sizeOfMagnifier * 2, sizeOfMagnifier * 2);
//            magnifierMatrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);
//            magnifierMatrix.postScale(2f, 2f);
//            magnifierPaint.getShader().setLocalMatrix(magnifierMatrix);
//
//            canvas.drawCircle(sizeOfMagnifier * 2, sizeOfMagnifier * 2, sizeOfMagnifier, magnifierPaint);
//            canvas.drawCircle(sizeOfMagnifier * 2, sizeOfMagnifier * 2, sizeOfMagnifier + 2, mCircleBorderPaint);
//
////            canvas.drawCircle(getWidth() - sizeOfMagnifier, sizeOfMagnifier, sizeOfMagnifier, magnifierPaint);
////            canvas.drawCircle(getWidth() - sizeOfMagnifier, sizeOfMagnifier, sizeOfMagnifier + 2, mCircleBorderPaint);
//        }
    }

    private void drawCircles(@NonNull Canvas canvas) {
        for (Point circle : mCircles) {
            canvas.drawCircle((float) circle.x, (float) circle.y, circleRadius, mCirclePaint);
            canvas.drawCircle((float) circle.x, (float) circle.y, circleRadius + 1, mCircleBorderPaint); //border
        }
    }

    private void drawLines(@NonNull Canvas canvas) {
        fillPath.reset();
        fillPath.moveTo((float) mCircles.get(0).x, (float) mCircles.get(0).y);
        for (int i = 0; i < 4; i++) {
            int end = i == 3 ? 0 : i + 1;
            fillPath.lineTo((float) mCircles.get(end).x, (float) mCircles.get(end).y);
        }

        //draw dimming
        canvas.clipPath(fillPath, Region.Op.DIFFERENCE);
        canvas.drawColor(Color.parseColor("#80313131"));
        canvas.clipRect(new RectF(0, 0, canvas.getWidth(), canvas.getHeight()), Region.Op.REPLACE);

        canvas.drawPath(fillPath, mLinePaint);
    }

    // </editor-fold>

    // <editor-fold desc="points translation">

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        boolean handled = false;

        Point touchedCircle;
        int xTouchOld = xTouch;
        int yTouchOld = yTouch;
        int actionIndex;
        int touchedCircleIndex;

        if (!showCropCircles || mCircles.isEmpty())
            return false;

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                xTouch = (int) event.getX();
                yTouch = (int) event.getY();

                touchedCircleIndex = getTouchedCircleIndex(xTouch, yTouch);
                touchedCircle = mCircles.get(touchedCircleIndex);
                Log.v(LOG_TAG, "touchedCircle = " + touchedCircle.toString());
                if (touchedCircle != null && checkDistance(touchedCircle, xTouch, yTouch)) {
                    changeCornerPosition(touchedCircleIndex, xTouch, yTouch);
                    invalidate();
                }
                handled = true;
                showMagnifier = true;
                break;

            case MotionEvent.ACTION_MOVE:
                xTouch = (int) event.getX();
                yTouch = (int) event.getY();

                touchedCircleIndex = getTouchedCircleIndex(xTouch, yTouch);
                touchedCircle = mCircles.get(touchedCircleIndex);

                int xCalculatedTouch;
                int yCalculatedTouch;

                if (xTouch >= 0 && xTouch <= getWidth()) {
                    xCalculatedTouch = xTouch;
                } else {
                    if (xTouch < 0) {
                        xCalculatedTouch = 0;
                    } else {
                        xCalculatedTouch = getWidth();
                    }
                }
                if (yTouch >= 0 && yTouch <= getHeight()) {
                    yCalculatedTouch = yTouch;
                } else {
                    if (yTouch < 0) {
                        yCalculatedTouch = 0;
                    } else {
                        yCalculatedTouch = getHeight();
                    }
                }
                if (touchedCircle != null && checkDistance(touchedCircle, xTouch, yTouch) && mode != Mode.MOVE) {
                    mode = Mode.RESIZE;
                    changeCornerPosition(touchedCircleIndex, xCalculatedTouch, yCalculatedTouch);
                } else if (enableCropProportions && mode != Mode.RESIZE) {
                    mode = Mode.MOVE;
                    moveCropBorders(xTouchOld, yTouchOld, xCalculatedTouch, yCalculatedTouch);
                }
                showMagnifier = true;
                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_UP:
                showMagnifier = false;
                invalidate();
                handled = true;
                mode = Mode.NONE;
                break;

            case MotionEvent.ACTION_CANCEL:
                showMagnifier = false;
                handled = true;
                mode = Mode.NONE;
                break;

            default:
                // do nothing
                break;
        }
        return handled;
    }

    private void moveCropBorders(int oldX, int oldY, int newX, int newY) {
        Log.v(LOG_TAG, "oldY = " + oldY + ", newY = " + newY);
        int width = getWidth();
        int height = getHeight();

        if (oldY > height || oldY < 0 || oldX > width || oldX < 0) {
            return;
        }

        double deltaX = newX - oldX;
        double deltaY = newY - oldY;

        int borderWidth = (int) (mCircles.get(3).x - mCircles.get(2).x);
        int borderHeight = (int) (mCircles.get(1).y - mCircles.get(2).y);

        int maxX = width - borderWidth;
        int maxY = height - borderHeight;

        int realX = Math.min((int) (mCircles.get(2).x + deltaX), maxX);

        if (realX < 0) {
            realX = 0;
        }

        int realY = Math.min((int) (mCircles.get(2).y + deltaY), maxY);

        if (realY < 0) {
            realY = 0;
        }

        Log.v(LOG_TAG, "realY = " + realY);


        deltaX = realX - mCircles.get(2).x;
        deltaY = realY - mCircles.get(2).y;

        Log.v(LOG_TAG, "deltaY = " + deltaY);


        for (Point corner : mCircles) {
            corner.y += deltaY;
            corner.x += deltaX;
        }
    }

    private void changeCornerPosition(int circleIndex, int xTouch, int yTouch) {
        Log.d(LOG_TAG, "changeCornerPosition at " + circleIndex);
        Point circle = mCircles.get(circleIndex);

        if (enableCropProportions) {
            int oppositeCornerIndex = (circleIndex + 2) % 4;
            int leftCornerIndex = (circleIndex + 1) % 4;
            int rightCornerIndex = circleIndex - 1 < 0 ? 3 : circleIndex - 1;

            Point oppositeCorner = mCircles.get(oppositeCornerIndex);
            Point leftCorner = mCircles.get(leftCornerIndex);
            Point rightCorner = mCircles.get(rightCornerIndex);

            //left and right corners before applying proportionality
            newLeftCornerPoint.x = leftCorner.x;
            newLeftCornerPoint.y = yTouch;
            newRightCornerPoint.x = xTouch;
            newRightCornerPoint.y = rightCorner.y;

            double rectangleWidth = Math.max(newRightCornerPoint.x, oppositeCorner.x) - Math.min(newRightCornerPoint.x, oppositeCorner.x);
            //check minimal size
            rectangleWidth = Math.max((double) minWidth, rectangleWidth);
            //calculate height according to with changes
            double rectangleHeight = rectangleWidth * CROP_ASPECT_RATIO;

            //better to leave as is
            if (circleIndex == 0) {
                rightCorner.x = oppositeCorner.x + rectangleWidth;
                leftCorner.y = oppositeCorner.y + rectangleHeight;
                circle.x = rightCorner.x;
                circle.y = leftCorner.y;

            } else if (circleIndex == 1) {
                leftCorner.x = oppositeCorner.x - rectangleWidth;
                rightCorner.y = oppositeCorner.y + rectangleHeight;
                circle.x = leftCorner.x;
                circle.y = rightCorner.y;

            } else if (circleIndex == 2) {
                leftCorner.y = oppositeCorner.y - rectangleHeight;
                rightCorner.x = oppositeCorner.x - rectangleWidth;
                circle.x = rightCorner.x;
                circle.y = leftCorner.y;

            } else {
                leftCorner.x = oppositeCorner.x + rectangleWidth;
                rightCorner.y = oppositeCorner.y - rectangleHeight;
                circle.x = leftCorner.x;
                circle.y = rightCorner.y;
            }

        } else {
            circle.x = xTouch;
            circle.y = yTouch;
        }

    }

    private boolean checkDistance(Point closestPoint, final int tapX, final int tapY) {
        double distanceX = ((double) tapX - closestPoint.x);
        double distanceY = ((double) tapY - closestPoint.y);
        double len = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        return len <= MIN_DISTANCE_TO_POINT;
    }

    private Point getTouchedCircle(final int xTouch, final int yTouch) {
        return mCircles.get(getTouchedCircleIndex(xTouch, yTouch));
    }

    private int getTouchedCircleIndex(final int xTouch, final int yTouch) {
        int closestIndex = 0;
        double closestLength = Math.pow((mCircles.get(0).x - xTouch), 2) + Math.pow((mCircles.get(0).y - yTouch), 2);
        for (int i = 0; i < 4; i++) {
            double currentLength = Math.pow((mCircles.get(i).x - xTouch), 2) + Math.pow((mCircles.get(i).y - yTouch), 2);
            if (currentLength < closestLength) {
                closestLength = currentLength;
                closestIndex = i;
            }
        }
        return closestIndex;
    }

    // </editor-fold>
}
