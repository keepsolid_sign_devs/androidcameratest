package com.testproject.sign.cameratest.camera.util;

import org.json.JSONObject;

public class Log {
    public static boolean LOG_ENABLED = true;

    public static void init(boolean isEnabled) {
        LOG_ENABLED = isEnabled;
    }

    public static void i(String tag, String string) {
        if (LOG_ENABLED) android.util.Log.i(tag, string);
    }

    public static void e(String tag, String string) {
        if (LOG_ENABLED) android.util.Log.e(tag, string);
    }

    public static void d(String tag, String string) {
        if (LOG_ENABLED) android.util.Log.d(tag, string);
    }

    public static void v(String tag, String string) {
        if (LOG_ENABLED) android.util.Log.v(tag, string);
    }

    public static void w(String tag, String string) {
        if (LOG_ENABLED) android.util.Log.w(tag, string);
    }

    public static void wtf(String tag, String string) {
        if (LOG_ENABLED) android.util.Log.wtf(tag, string);
    }

    public static void json(String tag, String string, JSONObject obj) {
        String logJson = obj.toString().replace("{", "\n{\n").replace("}", "\n}").replace(",", ",\n");
        Log.d(tag, string + logJson);
    }

}