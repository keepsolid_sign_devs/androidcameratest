package com.testproject.sign.cameratest.camera.callbacks;

public interface OnCropEventListener {
    void onRotationButtonPressed(int direction);
    void onApplyBtnPressed();
    void onRotationDegreeChanged(int degree);
}
