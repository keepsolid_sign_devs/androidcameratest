package com.testproject.sign.cameratest.camera.callbacks;

public interface CameraOperationListener<T> {
    void onSuccess(T result);
}
