package com.testproject.sign.cameratest.camera.options;

public interface Trackable {
    void redo();

    void restore();

    void undo();
}