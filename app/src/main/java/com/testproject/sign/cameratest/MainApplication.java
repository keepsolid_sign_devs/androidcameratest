package com.testproject.sign.cameratest;


import android.app.Application;

import com.testproject.sign.cameratest.camera.util.BitmapCache;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.Log;
import com.testproject.sign.cameratest.camera.util.ViewUtils;

import org.opencv.android.OpenCVLoader;

public class MainApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        BitmapCache.init(this);
        KSImageCache.getInstance().init(this);
        ViewUtils.init(this);
        OpenCVLoader.initDebug();
        Log.init(true);
    }
}
