package com.testproject.sign.cameratest.camera.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.callbacks.CameraOperationListener;
import com.testproject.sign.cameratest.camera.callbacks.OnCropEventListener;
import com.testproject.sign.cameratest.camera.callbacks.OnEditModeChangeListener;
import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;
import com.testproject.sign.cameratest.camera.options.EffectsConstants;
import com.testproject.sign.cameratest.camera.options.EffectsControl;
import com.testproject.sign.cameratest.camera.util.ImageLoadingObserver;
import com.testproject.sign.cameratest.camera.util.ImageSize;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.MemoryHelper;
import com.testproject.sign.cameratest.camera.util.RecyclingBitmapDrawable;
import com.testproject.sign.cameratest.util.ProgressDialogController;

import org.opencv.core.Point;

import java.util.ArrayList;

import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_DEF_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_MAX_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_MIN_VALUE;

public class CameraOptionsFragment extends AbstractFragment {
    private static final String LOG_TAG = CameraOptionsFragment.class.getSimpleName();

    public static final String DISPLAY_IMAGE_NAME_KEY = "DISPLAY_IMAGE_NAME_KEY";
    public static final int RECOMMENDED_PAGE_HEIGHT = 842;
    public static final int RECOMMENDED_PAGE_WIDTH = 595;

    private KSImageCache imageCache;

    private CameraMode cameraMode = CameraMode.DOCUMENT;

    private OnTuneEventListener.TuneMode tuneMode = OnTuneEventListener.TuneMode.NONE;

    private PhotoEditOptionsView optionsView;
    private EditImageView docImage;
    private Bitmap originalImage;
    private Bitmap previewImage;
    private String imageName;

    public static CameraOptionsFragment newInstance() {
        return new CameraOptionsFragment();
    }

    @Nullable
    @Override
    public void onCreateView(LayoutInflater inflater) {
        if (getArguments() != null) {
            Bundle args = getArguments();
//            cameraMode = (CameraMode) args.getSerializable(CameraNewPhotoFragment.CAMERA_MODE_KEY);
        }
        setContentView(R.layout.camera_options_fragment);
        setToolbar();
        setImage();
        initOptionsView();
    }

    @Override
    public void onLockActions() {

    }

    // <editor-fold desc="init">

    private void initOptionsView() {
        optionsView = (PhotoEditOptionsView) findViewById(R.id.photo_options_view);
        optionsView.setOnEditModeChangeListener(onEditModeChangeListener);
        optionsView.setOnCropEventListener(onCropEventListener);
        optionsView.setOnTuneEventListener(onTuneEventListener);
    }


    private void setImage() {
        imageCache = KSImageCache.getInstance();
        docImage = (EditImageView) findViewById(R.id.editing_document_image_view);
        docImage.setShowCropCircles(false);

        if (getArguments() != null) {
            imageName = getArguments().getString(DISPLAY_IMAGE_NAME_KEY);
            ProgressDialogController.getInstance().showTransparentProgressBar(false);
            imageCache.getImgAsync(imageName, ImageSize.MEDIUM, new CameraOperationListener<RecyclingBitmapDrawable>() {
                @Override
                public void onSuccess(RecyclingBitmapDrawable result) {
                    ProgressDialogController.getInstance().hideProgressDialog();
                    if (result != null) {
                        docImage.setImageDrawable(result);
                        docImage.setShowCropCircles(true);
                        docImage.findImageCorners();

                        originalImage = MemoryHelper.setNewBitmap(originalImage, result.getBitmap().copy(result.getBitmap().getConfig(), true));
                        previewImage = MemoryHelper.setNewBitmap(previewImage, result.getBitmap().copy(result.getBitmap().getConfig(), true));
//                        EffectsControl.getInstance().setOriginalImage(originalImage);
                    }
                }
            });
        }
    }

    private void setToolbar() {
        Toolbar cameraToolbar = (Toolbar) findViewById(R.id.camera_options_toolbar);
        cameraToolbar.inflateMenu(R.menu.camera_options_menu);
        cameraToolbar.setOnMenuItemClickListener(onMenuItemClickListener);
        cameraToolbar.setNavigationIcon(R.drawable.back_icon);
        cameraToolbar.setNavigationOnClickListener(onBackClickListener);
    }

    // </editor-fold>

    // <editor-fold desc="image processing">

    private void showViewCorners(boolean show) {
        docImage.setShowCropCircles(show);
    }

    private void rotateBitmap(int angle, boolean isConfirmed) {
        if (isConfirmed) {
//            EffectsControl.getInstance().executeAndSave(
//                    new RotationEffect(docImage, previewImage, angle, EffectsConstants.ROTATION_IMAGEVIEW));
        } else {
//            EffectsControl.getInstance().execute(
//                    new RotationEffect(docImage, previewImage, angle, EffectsConstants.ROTATION_IMAGEVIEW));
        }
    }

    private void rotateImage(int degree, boolean isConfirmed) {
        if (isConfirmed) {
//            EffectsControl.getInstance().executeAndSave(
//                    new RotationEffect(docImage, previewImage, degree, EffectsConstants.ROTATION_IMAGEVIEW));
        } else {
//            EffectsControl.getInstance().execute(
//                    new RotationEffect(docImage, previewImage, degree, EffectsConstants.ROTATION_IMAGEVIEW));
        }
    }

    private void cropImage(ArrayList<Point> corners) {
//        EffectsControl.getInstance().executeAndSave(new CropEffect(docImage, previewImage, corners));
        docImage.findImageCorners();
    }

    private void changeColor(double value, boolean isConfirmed) {
        if (value < 25) {
            optionsView.setTuneValue(0);
            executeColorChange(EffectsConstants.COLOR_CVT, isConfirmed);
        } else if (value >= 25 && value < 75) {
            optionsView.setTuneValue(50);
            executeColorChange(EffectsConstants.COLOR_DEF_VALUE, isConfirmed);//IIN?
        } else if (value >= 75) {
            optionsView.setTuneValue(100);
            executeColorChange(EffectsConstants.COLOR_BINARIZATION, isConfirmed);
        }
    }

    private void executeColorChange(int type, boolean isConfirmed) {
        if (isConfirmed) {
//            EffectsControl.getInstance().executeAndSave(
//                    new ColorEffect(docImage, previewImage, type));
        } else {
//            EffectsControl.getInstance().execute(
//                    new ColorEffect(docImage, previewImage, type));
        }
    }


    private void changeBrightness(double value, boolean isConfirmed) {
        if (isConfirmed) {
//            EffectsControl.getInstance().executeAndSave(
//                    new BrightnessEffect(docImage, previewImage, value));
        } else {
//            EffectsControl.getInstance().execute(
//                    new BrightnessEffect(docImage, previewImage, value));
        }
    }

    private void changeContrast(double value, boolean isConfirmed) {
        if (isConfirmed) {
//            EffectsControl.getInstance().executeAndSave(
//                    new ContrastEffect(docImage, previewImage, value));
        } else {
//            EffectsControl.getInstance().execute(
//                    new ContrastEffect(docImage, previewImage, value));
        }
    }

    // </editor-fold>

    // <editor-fold desc="listeners">

    private OnEditModeChangeListener onEditModeChangeListener = new OnEditModeChangeListener() {
        @Override
        public void onTuneModeChosen() {
            showViewCorners(false);
        }

        @Override
        public void onCropModeChosen() {
            showViewCorners(true);
            docImage.findImageCorners();
        }
    };

    private OnCropEventListener onCropEventListener = new OnCropEventListener() {

        @Override
        public void onRotationButtonPressed(int direction) {
            Log.v(LOG_TAG, "onRotationButtonPressed");
            rotateBitmap(direction, false);
            docImage.findImageCorners();
        }

        @Override
        public void onApplyBtnPressed() {
            Log.v(LOG_TAG, "onApplyBtnPressed");
            int rotation = docImage.bitmapRotation;
            rotateImage(0, false);
            rotateBitmap(rotation % 360, true);
            docImage.bitmapRotation = 360;

            cropImage(docImage.getCorners());

            optionsView.selectTuneModeTab();
        }

        @Override
        public void onRotationDegreeChanged(int degree) {
            Log.v(LOG_TAG, "onRotationDegreeChanged " + degree);
//            rotateImage(degree, false);
        }
    };

    private OnTuneEventListener onTuneEventListener = new OnTuneEventListener() {
        @Override
        public int onTuneModeChanged(TuneMode mode) {
            Log.v(LOG_TAG, "onTuneModeChanged " + mode.name());
            tuneMode = mode;
            return getDefaultProgressValue(tuneMode);
        }

        @Override
        public void onTuneValueChanged(double value, boolean isConfirmed) {
            Log.v(LOG_TAG, "onTuneValueChanged " + value);
            switch (tuneMode) {
                case COLOR:
                    optionsView.setTuneValue(EffectsConstants.COLOR_DEF_VALUE);
                    changeColor(value, isConfirmed);
                    break;
                case BRIGHTNESS:
                    changeBrightness(value, isConfirmed);
                    break;
                case CONTRAST:
                    changeContrast(value, isConfirmed);
                    break;
            }
        }

        @Override
        public void onConfirmButtonPressed() {
            Log.v(LOG_TAG, "onConfirmButtonPressed");
            onTuneValueChanged(optionsView.getSeekBarProgress(), true);
        }

        @Override
        public void onCancelTune() {
//            docImage.setImageBitmap(EffectsControl.getInstance().getOriginalImage());
            if (docImage != null) {
                docImage.rotate(0);
            }
        }

        private int getDefaultProgressValue(TuneMode mode) {
            switch (mode) {
                case CONTRAST:
                    return (int) ((CONTRAST_DEF_VALUE - CONTRAST_MIN_VALUE) * 100 / (CONTRAST_MAX_VALUE - CONTRAST_MIN_VALUE));
                case COLOR:
                case BRIGHTNESS:
                default:
                    return DEFAULT_TUNE_VALUE;
            }
        }
    };

    private Toolbar.OnMenuItemClickListener onMenuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.camera_done_btn:
                    ProgressDialogController.getInstance().showTransparentProgressBar(true);
                    imageCache.getImgAsync(imageName, ImageSize.MAX, new CameraOperationListener<RecyclingBitmapDrawable>() {
                        @Override
                        public void onSuccess(RecyclingBitmapDrawable result) {
                            if (EffectsControl.getInstance().getEditedImagesSize() == 0) {
                                goBack();
                                ProgressDialogController.getInstance().hideProgressDialog();
                                return;
                            }
                            Bitmap editedBitmap = EffectsControl.getInstance().applyAllExecutedEffects(imageName, result.getBitmap());
                            EffectsControl.getInstance().cleanEffects();
                            imageCache.setSerializationQuality(100);
                            imageCache.setImage(editedBitmap, imageName);
                            imageCache.registerObserver(new ImageLoadingObserver() {
                                @Override
                                public void notifyLoadStarted(String imageTag) {

                                }

                                @Override
                                public void notifyLoadFinished(String imageTag) {
                                    if (isAdded() && isVisible() && getActivity() != null && !getActivity().isFinishing()
                                            && imageTag.equals(imageName)) {
                                        imageCache.unregisterObserver(this);
                                        goBack();
                                        ProgressDialogController.getInstance().hideProgressDialog();
                                    }
                                }
                            });
                        }
                    });
                    return true;
            }
            return false;
        }
    };

    private void cancelRotation() {
        if (docImage != null) {
            docImage.resetBitmapRotation();
        }
    }

    private void recycleImages() {
        if (originalImage != null && !originalImage.isRecycled()) {
            originalImage.recycle();
        }
        if (previewImage != null && !previewImage.isRecycled()) {
            previewImage.recycle();
        }
    }

    private View.OnClickListener onBackClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };

    @Override
    public void goBack() {
        recycleImages();
        EffectsControl.getInstance().cleanEffects();
        CoreSignFragmentManager.getInstance().showCameraGalleryFragment(cameraMode);
    }

    // </editor-fold>

}
