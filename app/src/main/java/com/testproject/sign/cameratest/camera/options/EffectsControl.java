package com.testproject.sign.cameratest.camera.options;

/**
 * Created by prog on 03.08.16.
 */

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//prev version uses stack - can be found in prev commits if needed
public class EffectsControl {
    private static EffectsControl instance;

    public static EffectsControl getInstance(){
        if (instance == null){
            instance = new EffectsControl();
        }
        return instance;
    }

    private Map<String, Set<Effect>> editedImages;

    public EffectsControl() {
        editedImages = new HashMap<>();
    }

    public Bitmap execute(Bitmap originalImage, Effect effect) {
        return effect.execute(originalImage);
    }

    public void save(String imageId, Effect effect) {
        if (!editedImages.containsKey(imageId)) {
            editedImages.put(imageId, new LinkedHashSet<Effect>());
        }
        Set<Effect> executedEffects = editedImages.get(imageId);
        if (executedEffects != null) {
            executedEffects.add(effect);
        }
    }

    public Bitmap executeAndSave(String imageId, Bitmap originalImage, Effect effect) {
        if (effect instanceof RotationEffect) {
            effect.setType(EffectsConstants.ROTATION_BITMAP);
        }
        originalImage = effect.execute(originalImage);
        if (!editedImages.containsKey(imageId)) {
            editedImages.put(imageId, new LinkedHashSet<Effect>());
        }
        Set<Effect> executedEffects = editedImages.get(imageId);
        executedEffects.add(effect);

        return originalImage;
    }

    public Bitmap applyAllExecutedEffects(String imageId, Bitmap input) {
        Set<Effect> executedEffects = editedImages.get(imageId);
        if (executedEffects != null) {
            for (Effect effect : executedEffects) {
                if (effect instanceof RotationEffect) {
                    effect.setType(EffectsConstants.ROTATION_BITMAP);
                }
                input = effect.execute(input);
            }
        }
        return input;
    }

    public Bitmap applyExecutedEffects(String imageId, Bitmap input, List<EffectType> effectTypes) {
        Set<Effect> executedEffects = editedImages.get(imageId);
        if (executedEffects != null) {
            for (Effect effect : executedEffects) {
                if (effect instanceof RotationEffect) {
                    effect.setType(EffectsConstants.ROTATION_BITMAP);
                }
                if (effectTypes.contains(effect.getEffectType())) {
                    input = effect.execute(input);
                }
            }
        }
        return input;
    }

    public Bitmap applyDiffExecutedEffects(String imageId, Bitmap input, List<EffectType> effectTypesToIgnore) {
        Set<Effect> executedEffects = editedImages.get(imageId);
        if (executedEffects != null) {
            for (Effect effect : executedEffects) {
                if (effect instanceof RotationEffect) {
                    effect.setType(EffectsConstants.ROTATION_BITMAP);
                }
                if (!effectTypesToIgnore.contains(effect.getEffectType())) {
                    input = effect.execute(input);
                }
            }
        }
        return input;
    }

    public int getEditedImagesSize() {
        return editedImages.size();
    }

    public int getExecutedEffectsSize(String imageId) {
        if (!editedImages.containsKey(imageId)) {
            return 0;
        }
        return editedImages.get(imageId).size();
    }

    public @Nullable Effect getEffectForImage(String imageId, EffectType effectType) {
        if (editedImages.containsKey(imageId)) {
            for (Effect effect : editedImages.get(imageId)) {
                if (effect.getEffectType() == effectType) {
                    return effect;
                }
            }
        }
        return null;
    }

    public boolean isEffectForImageExists(String imageId, EffectType effectType) {
        return getEffectForImage(imageId, effectType) != null;
    }

    public boolean discardEffect(String imageId, EffectType effectType) {
        if (editedImages.containsKey(imageId)) {
            for (Effect effect : editedImages.get(imageId)) {
                if (effect.getEffectType() == effectType) {
                    return editedImages.values().remove(effect);
                }
            }
        }
        return false;
    }

    public void cleanEffects() {
        editedImages.clear();
    }
}
