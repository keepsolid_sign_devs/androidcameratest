package com.testproject.sign.cameratest.camera.camera_api;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureView;

/**
 * Created by yoba on 25.07.16.
 */
public class AutoFitTextureView extends TextureView {

    private int mRatioWidth = 0;
    private int mRatioHeight = 0;

    public AutoFitTextureView(Context context) {
        this(context, null);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     * @param width  Relative horizontal size
     * @param height Relative vertical size
     */
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
        mRatioWidth = width;
        mRatioHeight = height;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width < height * mRatioWidth / mRatioHeight) {
                int countedHeight = width * mRatioHeight / mRatioWidth;
                if (countedHeight < height) {
                    double diffY = height - countedHeight;
                    double coeff = diffY / (diffY + (double) countedHeight);
                    double scale = 1 + coeff;
                    setMeasuredDimension((int) (width * scale), height);
                } else {
                    setMeasuredDimension(width, countedHeight);
                }
            } else {
                int countedWidth = height * mRatioWidth / mRatioHeight;
                if (countedWidth < width) {
                    double diffX = width - countedWidth;
                    double coeff = diffX /  (diffX + (double) countedWidth);
                    double scale = 1 + coeff;
                    setMeasuredDimension(width, (int) (height * scale));
                } else {
                    setMeasuredDimension(countedWidth, height);
                }
            }
        }
    }
}
