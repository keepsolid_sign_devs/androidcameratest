package com.testproject.sign.cameratest.camera.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.base_ui.NpaGridLayoutManager;
import com.testproject.sign.cameratest.base_ui.RecyclingImageView;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.options.EditedImagesManager;
import com.testproject.sign.cameratest.camera.util.ImageLoadingObserver;
import com.testproject.sign.cameratest.camera.util.ImageSize;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.RecyclingBitmapDrawable;
import com.testproject.sign.cameratest.camera.util.ViewUtils;
import com.testproject.sign.cameratest.util.UIThreadHelper;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.testproject.sign.cameratest.camera.ui.GalleryMainFragment.CAMERA_MODE_KEY;

public class CameraGalleryFragment extends AbstractFragment {
    private static final String LOG_TAG = CameraGalleryFragment.class.getSimpleName();

    private KSImageCache cacheManager;
    private RecyclerView recyclerView;
    private GalleryRecyclerAdapter adapter;

    private CameraMode cameraMode = CameraMode.DOCUMENT;

    public static CameraGalleryFragment newInstance() {
        return new CameraGalleryFragment();
    }

    @Nullable
    @Override
    public void onCreateView(LayoutInflater inflater) {
        if (getArguments() != null) {
            Bundle args = getArguments();
            cameraMode = (CameraMode) args.getSerializable(CAMERA_MODE_KEY);
        }
        cacheManager = KSImageCache.getInstance();
        initView();
        setAdapter();
    }

    private void initView() {
        setContentView(R.layout.camera_gallery_fragment);
        prepareToolbar("Gallery", R.id.gallery_toolbar, R.drawable.back_icon);
        recyclerView = (RecyclerView) findViewById(R.id.camera_gallery_recycler);
        final GridLayoutManager lm = new NpaGridLayoutManager(getContext(), ViewUtils.countGridViewColumns());
        recyclerView.setLayoutManager(lm);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(-ViewUtils.countGridViewColumns());
        recyclerView.setDrawingCacheEnabled(true);
    }

    private void setAdapter() {
        adapter = new GalleryRecyclerAdapter(cacheManager.getCachedImagesKeySet());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void goBack() {
        CoreSignFragmentManager.getInstance().showCameraNewPhotoFragment(cameraMode);
    }

    @Override
    public void onLockActions() {
        //do nothing yet
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.camera_options_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.camera_done_btn:
                createDocumentFromImages();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void createDocumentFromImages() {
        final Set<String> progressImages = cacheManager.getProgressImagesKeySet();
        if (!progressImages.isEmpty()) {
            cacheManager.registerObserver(new ImageLoadingObserver() {
                @Override
                public void notifyLoadStarted(String imageTag) {
                }

                @Override
                public void notifyLoadFinished(String imageTag) {
                    if (progressImages.isEmpty()) {
                        createNewDocument();
                    }
                }
            });
        } else {
            createNewDocument();
        }
    }

    private void createNewDocument() {
        Set<String> imageKeys = cacheManager.getCachedImagesKeySet();
        String[] imageTags = new String[imageKeys.size()];
    }

    private void showImageOptionsPopup(final String imageTag, final int position) {
        final PopupMenu popupMenu = new PopupMenu(getContext(), recyclerView.findViewHolderForAdapterPosition(position).itemView, Gravity.END);
        final Menu menu = popupMenu.getMenu();
        popupMenu.getMenuInflater().inflate(R.menu.camera_gallery_options_menu, menu);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_edit:
                        showEditPhotoFragment(imageTag);
                        break;
                    case R.id.action_delete:
                        deleteImage(imageTag, position);
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }


    private void showEditPhotoFragment(String docTag) {
        CoreSignFragmentManager.getInstance().showCameraOptionsFragment(docTag, cameraMode);
    }

    private void deleteImage(String imageTag, int position) {
        cacheManager.removeImage(imageTag);
        EditedImagesManager.getInstance().remove(imageTag);
        adapter.refreshPhotos(cacheManager.getCachedImagesKeySet());
        adapter.notifyItemRemoved(position);
    }

    private class GalleryRecyclerAdapter extends RecyclerView.Adapter<GalleryViewHolder> {

        private String[] imageTags;
        ExecutorService executorService = Executors.newFixedThreadPool(6);

        GalleryRecyclerAdapter(Set<String> imageKeys) {
            refreshPhotos(imageKeys);
        }

        void refreshPhotos(Set<String> imageKeys) {
            imageTags = new String[imageKeys.size()];
            imageTags = imageKeys.toArray(imageTags);
        }

        @Override
        public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.gallery_item, parent, false);
            return new GalleryViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final GalleryViewHolder holder, final int position) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    RecyclingBitmapDrawable result = cacheManager.getImage(imageTags[position], ImageSize.MIN);
                    updateImage(holder, result, position);
                }
            });

            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = holder.getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        showImageOptionsPopup(imageTags[position], position);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return imageTags.length;
        }

        @Override
        public void onViewRecycled(GalleryViewHolder holder) {
            holder.imageView.setImageDrawable(null);
        }
    }

    private void updateImage(final GalleryViewHolder holder, final RecyclingBitmapDrawable result, final int position) {
        UIThreadHelper.runOnUIThread(getContext(), new Runnable() {
            @Override
            public void run() {
                if (position == holder.getAdapterPosition()) {
                    final RecyclingBitmapDrawable photoDrawable = new RecyclingBitmapDrawable(getResources(),
                            result.getBitmap().copy(Bitmap.Config.ARGB_8888, true));
                    holder.imageView.setImageDrawable(photoDrawable);
                }
            }
        });
    }

    private class GalleryViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout container;
        RecyclingImageView imageView;

        GalleryViewHolder(View itemView) {
            super(itemView);
            container = (RelativeLayout) itemView.findViewById(R.id.gallery_item_layout_main);
            imageView = (RecyclingImageView) itemView.findViewById(R.id.gallery_item_image);
        }
    }

}
