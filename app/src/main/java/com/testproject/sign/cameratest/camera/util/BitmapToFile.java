package com.testproject.sign.cameratest.camera.util;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapToFile {

    public static final int QUALITY = 100;
    public static final String PREVIEW_FILE_NAME = "preview.png";

    public static String saveImage(Bitmap image) {
        File dir = new File(FileSystemHelper.imageTempPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, PREVIEW_FILE_NAME);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, QUALITY, fOut);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }

    public static String saveImage(Bitmap image, String path, String fileName, Bitmap.CompressFormat compressFormat) {
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, fileName + "." + compressFormat.name().toLowerCase());

        try {
            FileOutputStream fOut = new FileOutputStream(file);
            image.compress(compressFormat, QUALITY, fOut);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }

}
