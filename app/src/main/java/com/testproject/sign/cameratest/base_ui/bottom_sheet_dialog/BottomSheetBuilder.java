package com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.View;

public class BottomSheetBuilder {
    private BottomSheetAdapterBuilder mAdapterBuilder;
    private Context mContext;
    private BottomSheetItemClickListener mItemClickListener;


    public BottomSheetBuilder(Context context) {
        mContext = context;
        mAdapterBuilder = new BottomSheetAdapterBuilder(mContext);
    }

    public BottomSheetBuilder setMode(BottomSheetMode mode) {
        mAdapterBuilder.setMode(mode);
        return this;
    }

    public BottomSheetBuilder setItemClickListener(BottomSheetItemClickListener listener) {
        mItemClickListener = listener;
        return this;
    }

    public BottomSheetBuilder addTitleItem(@StringRes int title) {
        return addTitleItem(mContext.getString(title));
    }

    public BottomSheetBuilder addTitleItem(String title) {
        mAdapterBuilder.addTitleItem(title);
        return this;
    }

    public BottomSheetBuilder addItem(int id, @StringRes int title, Drawable icon) {
        mAdapterBuilder.addItem(id, mContext.getString(title), icon);
        return this;
    }

    public BottomSheetBuilder addItem(int id, String title, @DrawableRes int icon) {
        mAdapterBuilder.addItem(id, title, ContextCompat.getDrawable(mContext, icon));
        return this;
    }

    public BottomSheetMenuDialog createDialog() {
        BottomSheetMenuDialog dialog = new BottomSheetMenuDialog(mContext);

        View sheet = mAdapterBuilder.createView(dialog);
        dialog.setBottomSheetItemClickListener(mItemClickListener);
        dialog.setContentView(sheet);

        return dialog;
    }

    // </editor-fold>

}
