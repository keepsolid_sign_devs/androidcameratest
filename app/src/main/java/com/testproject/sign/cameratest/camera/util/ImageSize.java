package com.testproject.sign.cameratest.camera.util;

public enum ImageSize {
    MAX,
    MEDIUM,
    MIN
}
