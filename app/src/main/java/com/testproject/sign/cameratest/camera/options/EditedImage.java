package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;

import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by user on 11/6/17.
 */

public class EditedImage {

    private String imageId;

    private double contrast = EffectsConstants.CONTRAST_DEF_VALUE;
    private double brightness = EffectsConstants.BRIGHTNESS_DEF_VALUE;
    private double color = EffectsConstants.COLOR_DEF_VALUE;

    private int rotation;

    //relative coordinates 0-1
    private ArrayList<Point> corners;

    public EditedImage() {
        corners = new ArrayList<>(4);
    }

    public EditedImage(String imageId) {
        this.imageId = imageId;
        corners = new ArrayList<>(4);
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public double getContrast() {
        return contrast;
    }

    public void setContrast(double contrast) {
        this.contrast = contrast;
    }

    public double getBrightness() {
        return brightness;
    }

    public void setBrightness(double brightness) {
        this.brightness = brightness;
    }

    public double getColor() {
        return color;
    }

    public void setColor(double color) {
        this.color = color;
    }

    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = (this.rotation + rotation) % 360;
    }

    public ArrayList<Point> getCorners() {
        return corners;
    }

    public void setCorners(ArrayList<Point> corners) {
        this.corners = corners;
    }

    public Bitmap executeColorEffects(Bitmap input) {
        return executeContrastEffect(executeBrightnessEffect(executeColorEffect(input)));
    }

    public Bitmap executeBrightnessEffect(Bitmap input) {
        if (brightness == EffectsConstants.BRIGHTNESS_DEF_VALUE) {
            return input;
        }
        return EffectsControl.getInstance().execute(input, new BrightnessEffect(brightness));
    }

    public Bitmap executeColorEffect(Bitmap input) {
        if (color == EffectsConstants.COLOR_DEF_VALUE) {
            return input;
        }
        return EffectsControl.getInstance().execute(input, new ColorEffect(color));
    }

    public Bitmap executeContrastEffect(Bitmap input) {
        if (contrast == EffectsConstants.CONTRAST_DEF_VALUE) {
            return input;
        }
        return EffectsControl.getInstance().execute(input, new ContrastEffect(contrast));
    }

    public Bitmap transformImage(Bitmap input) {
        return rotateImage(cropImage(input));
    }

    public Bitmap rotateImage(Bitmap input) {
        if (rotation == 0 || rotation == 360) {
            return input;
        }
        return EffectsControl.getInstance().execute(input, new RotationEffect(rotation));
    }

    public Bitmap cropImage(Bitmap input) {
        if (corners.isEmpty()) {
            return input;
        }
        return EffectsControl.getInstance().execute(input, new CropEffect(corners));
    }

    public Bitmap executeAllEffects(Bitmap input) {
        return executeColorEffects(transformImage(input));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EditedImage that = (EditedImage) o;

        return imageId != null ? imageId.equals(that.imageId) : that.imageId == null;

    }

    @Override
    public int hashCode() {
        return imageId != null ? imageId.hashCode() : 0;
    }

}
