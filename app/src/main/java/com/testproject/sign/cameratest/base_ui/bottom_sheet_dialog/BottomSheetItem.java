package com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog;

interface BottomSheetItem {

    String getTitle();
}
