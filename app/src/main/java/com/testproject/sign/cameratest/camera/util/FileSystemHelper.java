package com.testproject.sign.cameratest.camera.util;

import android.content.Context;

import java.io.File;

public class FileSystemHelper {

	public FileSystemHelper() {
		
	}

	public static String appPath;
	public static final String PDF_EXTENSION = ".pdf";
	public static final String JPEG_EXTENSION = ".jpeg";
	public static final String PNG_EXTENSION = ".png";

	public static void init(Context context)
	{
		dbPath = context.getFilesDir().getAbsolutePath();
		tempPath = context.getCacheDir().getAbsolutePath();
		imageTempPath = tempPath + "/images";
//		pdfTempPath = tempPath + "/PDFs";
		filesPath = "/fs";
		File iF = new File(imageTempPath);
		iF.mkdir();
//		File iP = new File(pdfTempPath);
//		iP.mkdir();
		appPath = context.getFilesDir().getAbsolutePath();
	}
	public static final String getDBPath(Context context)
	{
		Log.e("Lool", "getDBPath = " + context.getFilesDir().getAbsolutePath());
		return context.getFilesDir().getAbsolutePath();
	}
	public static final String getTempPath(Context context)
	{
		return context.getCacheDir().getAbsolutePath();
	}
	public static final String getDBPath(Context context, String userName)
	{
		File userDir = new File(context.getFilesDir().getAbsolutePath() + "/" + userName);
		userDir.mkdir();
		return userDir.getAbsolutePath();
	}
	public static final String getTempPath(Context context, String userName)
	{
		File userDir = new File(context.getCacheDir().getAbsolutePath() + "/" + userName);
		userDir.mkdir();
		return userDir.getAbsolutePath();
	}
	public static String dbPath;
	public static String tempPath;
	public static String dropboxPath;
	public static String imageTempPath;
//	public static String pdfTempPath;
	public static String filesPath;

	private static void deleteDir(File file) { 
        if (file.isDirectory()) {
            for (String child : file.list()) {
                deleteDir(new File(file, child));
            }
        }
        file.delete();  
    }
	
	public static void deleteTempPath(Context context) {
		File file = new File(getTempPath(context));
		deleteDir(file);
	}

	public static void deleteTempPath(Context context, String userId) {
		File file = new File(getTempPath(context), userId);
		deleteDir(file);
	}

	public static void deleteDBPath(Context context) {
		File file = new File(getDBPath(context));
		deleteDir(file);
	}

	public static void deleteDBPath(Context context, String userId) {
		File file = new File(getDBPath(context, userId));
		deleteDir(file);
	}
}
