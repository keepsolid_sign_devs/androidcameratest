package com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.testproject.sign.cameratest.R;

import java.util.List;

class BottomSheetAdapter extends RecyclerView.Adapter<BottomSheetAdapter.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_HEADER = 1;

    private List<BottomSheetItem> items;
    private BottomSheetMode mode;
    private BottomSheetItemClickListener itemClickListener;

    BottomSheetAdapter(List<BottomSheetItem> items, BottomSheetMode mode, BottomSheetItemClickListener itemClickListener) {
        this.items = items;
        this.mode = mode;
        this.itemClickListener = itemClickListener;
    }

    public void setItemClickListener(BottomSheetItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        BottomSheetItem item = items.get(position);

        if (item instanceof BottomSheetMenuItem) {
            return TYPE_ITEM;
        } else if (item instanceof BottomSheetHeader) {
            return TYPE_HEADER;
        }

        return super.getItemViewType(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mode == BottomSheetMode.GRID) {
            View layout = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.bottom_sheet_grid_item, parent, false);
            return new ItemViewHolder(layout);
        }

        if (mode == BottomSheetMode.LIST) {

            if (viewType == TYPE_HEADER) {
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.bottom_sheet_header, parent, false));
            }

            if (viewType == TYPE_ITEM) {
                return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.bottom_sheet_list_item, parent, false));
            }
        }

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bottom_sheet_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BottomSheetItem item = items.get(position);

        if (mode == BottomSheetMode.LIST) {
            if (holder.getItemViewType() == TYPE_ITEM) {
                ((ItemViewHolder) holder).setData((BottomSheetMenuItem) item);
            } else if (holder.getItemViewType() == TYPE_HEADER) {
                ((HeaderViewHolder) holder).setData((BottomSheetHeader) item);
            }
        } else {
            ((ItemViewHolder) holder).setData((BottomSheetMenuItem) item);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class HeaderViewHolder extends ViewHolder {

        private TextView title;

        HeaderViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.bottom_sheet_header_title);
        }

        public void setData(BottomSheetHeader headerItem) {
            title.setText(headerItem.getTitle());
        }
    }

    private class ItemViewHolder extends ViewHolder implements View.OnClickListener {

        private TextView title;
        private ImageView icon;

        ItemViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.bottom_sheet_item_title);
            icon = (ImageView) itemView.findViewById(R.id.bottom_sheet_item_icon);
            itemView.setOnClickListener(this);
        }

        public void setData(BottomSheetMenuItem menuItem) {
            title.setText(menuItem.getTitle());
            icon.setImageDrawable(menuItem.getIcon());
        }

        @Override
        public void onClick(View v) {
            BottomSheetMenuItem item = (BottomSheetMenuItem) items.get(getLayoutPosition());

            if (itemClickListener != null) {
                itemClickListener.onItemClick(item);
            }
        }
    }
}
