package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;

import com.testproject.sign.cameratest.camera.ui.EditImageView;
import com.testproject.sign.cameratest.util.UIThreadHelper;


/**
 * Created by prog on 03.08.16.
 */
public class BrightnessEffect extends Effect {

    public static final String LOG_TAG = BrightnessEffect.class.getSimpleName();

    public BrightnessEffect(double value) {
        super(value);
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.BRIGHTNESS;
    }

    @Override
    public Bitmap execute(Bitmap originalImage) {
        double progress = countValueProportionally(value, EffectsConstants.BRIGHTNESS_MIN_VALUE, EffectsConstants.BRIGHTNESS_MAX_VALUE);

        return adjustBrightness(progress, originalImage);
    }

    private Bitmap adjustBrightness(double contrast, Bitmap originalImage) {
        return ImageProcessor.adjustBrightness(originalImage, contrast);
    }

    @Override
    public void undo() {

    }
}
