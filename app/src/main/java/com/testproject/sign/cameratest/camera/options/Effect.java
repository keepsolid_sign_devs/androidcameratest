package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.testproject.sign.cameratest.camera.ui.EditImageView;


public abstract class Effect {
    protected double value;
    protected int type;

    public Effect(double value, int type) {
        this.value = value;
        this.type = type;
    }

    public Effect(int type) {
        this.type = type;
    }

    public Effect(double value) {
        this.value = value;

    }

    public Effect() {}

    public abstract EffectType getEffectType();

    public void setType(int type) {
        this.type = type;
    }

    public double getValue() {
        return value;
    }

    abstract Bitmap execute(Bitmap originalImage);

    abstract void undo();

    protected Bitmap setNewBitmap(Bitmap reference, Bitmap newObject) {
        if (newObject != null) {
            if (reference != null && !reference.isRecycled()) {
                reference.recycle();
            }
            reference = newObject;
        }
        return reference;
    }

    public static double countValueProportionally(double value, double minVal, double maxVal) {
        return (maxVal - minVal) / 100d * value + minVal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Effect effect = (Effect) o;

        return getEffectType() == effect.getEffectType();

    }

    @Override
    public int hashCode() {
        return getEffectType().hashCode();
    }

}