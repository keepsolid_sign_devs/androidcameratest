package com.testproject.sign.cameratest.camera.ui;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.camera.callbacks.OnCropEventListener;
import com.testproject.sign.cameratest.camera.callbacks.OnEditModeChangeListener;
import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;
import com.testproject.sign.cameratest.camera.options.EffectsConstants;


public class PhotoEditOptionsView extends RelativeLayout {

    public static final int DEFAULT_TUNE_VALUE = 50;
    private static final int MAX_ROTATION_VALUE = 90;

    private OnEditModeChangeListener onEditModeChangeListener;
    private OnCropEventListener onCropEventListener;
    private OnTuneEventListener onTuneEventListener;

    private RelativeLayout rotationModePanel;
    private LinearLayout tuneOptionsModePanel;
    private RelativeLayout tuneModePanel;

    private ImageView tuneModeImage;
    private TabLayout optionsTabs;
    private SeekBar tuneSeekBar;

    public PhotoEditOptionsView(Context context) {
        super(context);
        initView();
    }

    public PhotoEditOptionsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public PhotoEditOptionsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.photo_optins_view, this);
        optionsTabs = (TabLayout) findViewById(R.id.photo_options_tabs);
        optionsTabs.addTab(optionsTabs.newTab().setIcon(R.drawable.camera_crop_blue_icon), true);
        optionsTabs.addTab(optionsTabs.newTab().setIcon(R.drawable.camera_tune_white_icon));
        optionsTabs.setOnTabSelectedListener(optionTabSelectionListener);

        rotationModePanel = (RelativeLayout) findViewById(R.id.rotation_panel);
        tuneOptionsModePanel = (LinearLayout) findViewById(R.id.options_modes_panel);
        tuneModePanel = (RelativeLayout) findViewById(R.id.tune_mode_panel);

        FrameLayout autoCropBtn = (FrameLayout) findViewById(R.id.apply_rotation_crop_btn);
        FrameLayout rotationRightBtn = (FrameLayout) findViewById(R.id.options_rotate_right_btn);
        FrameLayout rotationLeftBtn = (FrameLayout) findViewById(R.id.options_rotate_left_btn);

        FrameLayout colorModeBtn = (FrameLayout) findViewById(R.id.color_mode_btn);
        FrameLayout lightModeBtn = (FrameLayout) findViewById(R.id.light_mode_btn);
        FrameLayout contrastModeBtn = (FrameLayout) findViewById(R.id.contrast_mode_btn);

        tuneSeekBar = (SeekBar) findViewById(R.id.tune_seek_bar);
        tuneModeImage = (ImageView) findViewById(R.id.tune_mode_img);
        FrameLayout tuneOkBtn = (FrameLayout) findViewById(R.id.camera_tune_ok_btn);

        autoCropBtn.setOnClickListener(onAutoRotateClick);
        rotationRightBtn.setOnClickListener(onRotateRightClick);
        rotationLeftBtn.setOnClickListener(onRotateLeftClick);

        colorModeBtn.setOnClickListener(onColorModeBtnClick);
        lightModeBtn.setOnClickListener(onBrightnessModeBtnClick);
        contrastModeBtn.setOnClickListener(onContrastModeBtnClick);

        tuneOkBtn.setOnClickListener(onTuneOkClick);
        tuneSeekBar.setOnSeekBarChangeListener(onTuneValueChangedListener);
    }

    public void setDefaultSeekBarValue() {
        tuneSeekBar.setProgress(DEFAULT_TUNE_VALUE);
    }

    // <editor-fold desc="tabs listeners">

    public void selectTuneModeTab() {
        try {
            optionsTabs.getTabAt(1).select();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

    }

    private TabLayout.OnTabSelectedListener optionTabSelectionListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            if (tab.getPosition() == 0) {
                tab.setIcon(R.drawable.camera_crop_blue_icon);
                rotationModePanel.setVisibility(VISIBLE);
                tuneOptionsModePanel.setVisibility(GONE);
                tuneModePanel.setVisibility(GONE);
                if (onEditModeChangeListener != null) {
                    onEditModeChangeListener.onCropModeChosen();
                }
                onTuneEventListener.onCancelTune();
            } else {
                tab.setIcon(R.drawable.camera_tune_blue_icon);
                rotationModePanel.setVisibility(GONE);
                tuneOptionsModePanel.setVisibility(VISIBLE);
                tuneModePanel.setVisibility(GONE);
                if (onEditModeChangeListener != null) {
                    onEditModeChangeListener.onTuneModeChosen();
                }
                onTuneEventListener.onCancelTune();
            }

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            if (tab.getPosition() == 0) {
                tab.setIcon(R.drawable.camera_crop_white_icon);
            } else {
                tab.setIcon(R.drawable.camera_tune_white_icon);
            }
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    // </editor-fold>

    // <editor-fold desc="crop listeners">

    private OnClickListener onColorModeBtnClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int defaultProgress = DEFAULT_TUNE_VALUE;
            if (onTuneEventListener != null) {
                defaultProgress = onTuneEventListener.onTuneModeChanged(OnTuneEventListener.TuneMode.COLOR);
            }
            tuneModeImage.setImageDrawable(getResources().getDrawable(R.drawable.camera_color_icon));
            tuneOptionsModePanel.setVisibility(GONE);
            tuneModePanel.setVisibility(VISIBLE);
            setTuneValue(defaultProgress);
        }
    };

    private OnClickListener onBrightnessModeBtnClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int defaultProgress = DEFAULT_TUNE_VALUE;
            if (onTuneEventListener != null) {
                defaultProgress = onTuneEventListener.onTuneModeChanged(OnTuneEventListener.TuneMode.BRIGHTNESS);
            }
            tuneModeImage.setImageDrawable(getResources().getDrawable(R.drawable.camera_light_icon));
            tuneOptionsModePanel.setVisibility(GONE);
            tuneModePanel.setVisibility(VISIBLE);
            setTuneValue(defaultProgress);
        }
    };

    private OnClickListener onContrastModeBtnClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int defaultProgress = DEFAULT_TUNE_VALUE;
            if (onTuneEventListener != null) {
                defaultProgress = onTuneEventListener.onTuneModeChanged(OnTuneEventListener.TuneMode.CONTRAST);
            }
            tuneModeImage.setImageDrawable(getResources().getDrawable(R.drawable.camera_contrast_icon));
            tuneOptionsModePanel.setVisibility(GONE);
            tuneModePanel.setVisibility(VISIBLE);
            setTuneValue(defaultProgress);
        }
    };

    private OnClickListener onTuneOkClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onTuneEventListener != null) {
                onTuneEventListener.onConfirmButtonPressed();
            }
            tuneOptionsModePanel.setVisibility(VISIBLE);
            tuneModePanel.setVisibility(GONE);
        }
    };

    private SeekBar.OnSeekBarChangeListener onTuneValueChangedListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if (onTuneEventListener != null) {
                onTuneEventListener.onTuneValueChanged((double) seekBar.getProgress(), false);
            }
        }
    };

    // </editor-fold>

    // <editor-fold desc="tune listeners">

    private OnClickListener onAutoRotateClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onCropEventListener != null) {
                onCropEventListener.onApplyBtnPressed();
            }
        }
    };

    private OnClickListener onRotateRightClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onCropEventListener != null) {
                onCropEventListener.onRotationButtonPressed(EffectsConstants.ROTATE_RIGHT);
            }
        }
    };

    private OnClickListener onRotateLeftClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onCropEventListener != null) {
                onCropEventListener.onRotationButtonPressed(EffectsConstants.ROTATE_LEFT);
            }
        }
    };


    public int getSeekBarProgress() {
        if (tuneSeekBar == null) {
            return 0;
        }
        return tuneSeekBar.getProgress();
    }

    // </editor-fold>

    // <editor-fold desc="getters & setters">

    public void setOnCropEventListener(OnCropEventListener onCropEventListener) {
        this.onCropEventListener = onCropEventListener;
    }

    public void setOnTuneEventListener(OnTuneEventListener onTuneEventListener) {
        this.onTuneEventListener = onTuneEventListener;
    }

    public void setOnEditModeChangeListener(OnEditModeChangeListener onEditModeChangeListener) {
        this.onEditModeChangeListener = onEditModeChangeListener;
    }

    public void setTuneValue(int value){
        Log.e("lool", "setTuneValue: " + value);
        tuneSeekBar.setProgress(value);
    }

    // </editor-fold>
}