package com.testproject.sign.cameratest.camera.ui;


import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.testproject.sign.cameratest.R;

public class DeletePageItem extends RelativeLayout {

    private static final int ANIM_DURATION = 350;

    private TextView title;
    private ImageView icon;
    private View deleteArea;

    private int hoveredColor;
    private int normalColor;

    private int height;

    public DeletePageItem(Context context) {
        super(context);
        init();
    }

    public DeletePageItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DeletePageItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.move_pages_footer, this);
        height = getResources().getDimensionPixelSize(R.dimen.delete_item_height);
        title = (TextView) findViewById(R.id.page_delete_text_view);
        icon = (ImageView) findViewById(R.id.page_delete_icon);
        deleteArea = findViewById(R.id.delete_page_area);
        hoveredColor = getResources().getColor(R.color.delete_page_color);
        normalColor = getResources().getColor(R.color.draw_color_black);
        setTranslationY(height);
        setPivotY(0);
    }

    public void setHovered(boolean hovered) {
        title.setTextColor(hovered? hoveredColor : normalColor);
        icon.setColorFilter(new PorterDuffColorFilter(hovered? hoveredColor : normalColor, PorterDuff.Mode.SRC_IN));
    }

    public View getDeleteArea() {
        return deleteArea;
    }

    public void show(){
        this.animate().translationY(0).setDuration(ANIM_DURATION);
    }

    public void hide(){
        this.animate().translationY(height).setDuration(ANIM_DURATION);
    }

}
