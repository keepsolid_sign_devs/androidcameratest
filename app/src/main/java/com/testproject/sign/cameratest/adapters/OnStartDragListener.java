package com.testproject.sign.cameratest.adapters;

import android.support.v7.widget.RecyclerView;

public interface OnStartDragListener {
    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    void onStartDrag(RecyclerView.ViewHolder viewHolder);

    void onFinishDrag(RecyclerView.ViewHolder viewHolder);
}