package com.testproject.sign.cameratest.camera.callbacks;

public interface OnEditModeChangeListener {

    void onTuneModeChosen();

    void onCropModeChosen();
}
