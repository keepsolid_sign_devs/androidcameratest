package com.testproject.sign.cameratest.camera;

/**
 * Created by yoba on 17.04.17.
 */

public enum CameraMode {
    DOCUMENT,
    SIGNATURE,
    INITIALS
}
