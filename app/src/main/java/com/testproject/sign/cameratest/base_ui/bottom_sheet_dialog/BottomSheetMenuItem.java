package com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog;

import android.graphics.drawable.Drawable;
import android.view.MenuItem;

public class BottomSheetMenuItem implements BottomSheetItem {

    private int mId;
    private String mTitle;

    private Drawable mIcon;

    BottomSheetMenuItem(int id, String title, Drawable icon) {
        this.mId = id;
        this.mTitle = title;
        this.mIcon = icon;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    public int getId() {
        return mId;
    }

    public Drawable getIcon() {
        return mIcon;
    }
}
