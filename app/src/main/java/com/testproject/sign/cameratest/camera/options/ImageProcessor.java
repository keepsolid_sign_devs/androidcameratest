package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.List;

import static com.testproject.sign.cameratest.camera.options.EffectsConstants.BRIGHTNESS_DEF_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.BRIGHTNESS_MIN_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_DEF_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_MAX_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.GAMMA_DEF_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.GAMMA_MIN_VALUE;


public class ImageProcessor {

    private static final String LOG_TAG = ImageProcessor.class.getSimpleName();

    public static final int INTERVAL = 25;
    public static final int MAX_SIDE_LENGTH = 500;

    public static ArrayList<Point> findObject(Bitmap input, boolean isFirstTry) {
//        OpenCVLoader.initDebug();

        Mat inputMat = new Mat();
        Utils.bitmapToMat(input, inputMat);

        double originalWidth = inputMat.width();
        double originalHeight = inputMat.height();

        double resultWidth;
        double resultHeight;

        double ratio = originalWidth / originalHeight;
        if (ratio < 1) {
            resultHeight = MAX_SIDE_LENGTH;
            resultWidth = resultHeight * ratio;
        } else {
            ratio = originalHeight / originalWidth;
            resultWidth = MAX_SIDE_LENGTH;
            resultHeight = resultWidth * ratio;
        }

        Imgproc.resize(inputMat, inputMat, new Size(resultWidth, resultHeight));

        if (isFirstTry) {
            Imgproc.cvtColor(inputMat, inputMat, Imgproc.COLOR_BGR2GRAY);
        } else {
            inputMat = executeAlternativeAlgorithmForSimilarBackground(inputMat);
        }
        showBitmap(inputMat);

        Imgproc.GaussianBlur(inputMat, inputMat, new Size(3, 3), 0);
        showBitmap(inputMat);
        autoCanny(inputMat);

        Mat morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
        Imgproc.morphologyEx(inputMat, inputMat, Imgproc.MORPH_CLOSE, morphKernel);

        ArrayList<MatOfPoint> contoursX = new ArrayList<>();
        Mat mask = Mat.zeros(inputMat.size(), CvType.CV_8UC1);
        Imgproc.findContours(inputMat, contoursX, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE); //RETR_LIST //RETR_CCOMP
        Imgproc.drawContours(mask, contoursX, -1, new Scalar(255), 5);
        showBitmap(mask);
        Log.v(LOG_TAG, "size " + Integer.toString(contoursX.size()));

        Imgproc.cvtColor(inputMat, inputMat, Imgproc.COLOR_BayerBG2RGB);

        ArrayList<Point> corners = new ArrayList<>();
        if (findLargestContour(contoursX, corners, originalWidth, originalHeight, resultWidth, resultHeight)) {
            transformCornersToRelative(corners, (int) resultWidth, (int) resultHeight);
        } else if (isFirstTry) {
            corners = findObject(input, false);
        } else {
            addDefaultCorners(corners, input.getWidth(), input.getHeight());
        }
        return corners;
    }

    private static boolean findLargestContour(ArrayList<MatOfPoint> contoursX, ArrayList<Point> corners,
                                              double originalWidth, double originalHeight,
                                              double resultWidth, double resultHeight) {
        double maxArea = -1;
        MatOfPoint2f approxCurve = new MatOfPoint2f();
        MatOfPoint temp_contour; //the largest is at the index 0 for starting point

        for (int idx = 0; idx < contoursX.size(); idx++) {
            temp_contour = contoursX.get(idx);
            double contourArea = Imgproc.contourArea(temp_contour);
            //compare this contour to the previous largest contour found
            if (contourArea > maxArea) {
                //check if this contour is a square
                MatOfPoint2f new_mat = new MatOfPoint2f(temp_contour.toArray());
                int contourSize = (int) Imgproc.arcLength(new_mat, true);
                MatOfPoint2f approxCurve_temp = new MatOfPoint2f();
                Imgproc.approxPolyDP(new_mat, approxCurve_temp, contourSize * 0.05, true);
                if (approxCurve_temp.total() == 4) {
                    maxArea = contourArea;
                    approxCurve = approxCurve_temp;
                }
            }
        }

        double[] temp_double = approxCurve.get(0, 0);
        double widthRatio = originalWidth / resultWidth;
        double heightRatio = originalHeight / resultHeight;

        Log.v(LOG_TAG, "findObject: " + maxArea);
        if (temp_double != null && maxArea >= resultWidth * resultHeight * 0.20) {
            Point p1 = new Point(temp_double[0] * widthRatio, temp_double[1] * heightRatio);
            temp_double = approxCurve.get(1, 0);
            Point p2 = new Point(temp_double[0] * widthRatio, temp_double[1] * heightRatio);
            temp_double = approxCurve.get(2, 0);
            Point p3 = new Point(temp_double[0] * widthRatio, temp_double[1] * heightRatio);
            temp_double = approxCurve.get(3, 0);
            Point p4 = new Point(temp_double[0] * widthRatio, temp_double[1] * heightRatio);

            corners.add(p1);
            corners.add(p2);
            corners.add(p3);
            corners.add(p4);

            for (Point corner : corners) {
                Log.v(LOG_TAG, "findObject: " + corner);
            }
            return true;
        }
        else
            return false;
    }

    public static void addDefaultCorners(ArrayList<Point> corners, int width, int height) {
        corners.add(new Point(INTERVAL, INTERVAL));
        corners.add(new Point(width - INTERVAL, INTERVAL));
        corners.add(new Point(INTERVAL, height - INTERVAL));
        corners.add(new Point(width - INTERVAL, height - INTERVAL));

        transformCornersToRelative(corners, width, height);
    }

    public static void addDefaultCornersWithProportions(ArrayList<Point> corners, int width, int height,
                                                        double cropAspectRatio) {
        int cropWidth = width - INTERVAL * 2;
        int cropHeight = (int) Math.round(cropWidth * cropAspectRatio);

        corners.add(new Point(INTERVAL, (height - cropHeight) / 2));
        corners.add(new Point(INTERVAL + cropWidth, (height - cropHeight) / 2));
        corners.add(new Point(INTERVAL, (height + cropHeight) / 2));
        corners.add(new Point(INTERVAL + cropWidth, (height + cropHeight) / 2));

        transformCornersToRelative(corners, width, height);
    }

    private static void transformCornersToRelative(ArrayList<Point> corners, int width, int height) {
        for (Point p : corners) {
            p.x /= width;
            p.y /= height;
        }
    }

    private static void transformCornersToAbsolute(ArrayList<Point> corners, int width, int height) {
        for (Point p : corners) {
            p.x *= width;
            p.y *= height;
        }
    }

    private static Mat executeAlternativeAlgorithmForSimilarBackground(Mat inputMat) {
        Imgproc.cvtColor(inputMat, inputMat, Imgproc.COLOR_BGR2HSV);
        List<Mat> channels = new ArrayList<>();
        Core.split(inputMat, channels);
        inputMat = channels.get(0);

//        Imgproc.equalizeHist(inputMat, inputMat);
//        showBitmap(inputMat);

//        Imgproc.adaptiveThreshold(inputMat, inputMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 9, 6);
//        showBitmap(inputMat);

        return inputMat;
    }

    public static Bitmap showBitmap(Mat inputMat) {
        Bitmap result = Bitmap.createBitmap(inputMat.width(), inputMat.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(inputMat, result);
        Log.v(LOG_TAG, "showBitmap - height " + result.getHeight() + ", width " + result.getWidth());

        return result;
    }

    public static void autoCanny(Mat inputMat) {
        double sigma = 0.4d; //0.44
        double lower = 0;
        double higher = 255;
        double v = Core.mean(inputMat).val[0];

        lower = Math.max(lower, (1 - sigma) * v);
        higher = Math.min(higher, (1 + sigma) * v);

        Log.v(LOG_TAG, "autoCanny: lower = " + lower + ", higher = " + higher);
        Imgproc.Canny(inputMat, inputMat, lower, higher);
    }

    private static double lineLength(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    }

    private static double getHeightRatio(ArrayList<Point> corners) {
        return getAverageHeight(corners) / getAverageWidth(corners);
    }

    private static double getWidthRatio(ArrayList<Point> corners) {
        return getAverageWidth(corners) / getAverageHeight(corners);
    }

    private static double getAverageHeight (ArrayList<Point> corners) {
        return (lineLength(corners.get(0).x,
                corners.get(0).y,
                corners.get(3).x,
                corners.get(3).y) +
                lineLength(corners.get(1).x,
                        corners.get(1).y,
                        corners.get(2).x,
                        corners.get(2).y)) / 2;
    }

    private static double getAverageWidth(ArrayList<Point> corners) {
        return (lineLength(corners.get(0).x,
                corners.get(0).y,
                corners.get(1).x,
                corners.get(1).y) +
                lineLength(corners.get(2).x,
                        corners.get(2).y,
                        corners.get(3).x,
                        corners.get(3).y)) / 2;
    }

    public static Bitmap warp(Bitmap image, ArrayList<Point> corners) {
        transformCornersToAbsolute(corners, image.getWidth(), image.getHeight());
        sortCorners(corners);
        Mat startM = Converters.vector_Point2f_to_Mat(corners);

        int resultWidth;
        int resultHeight;

        if (getWidthRatio(corners) < 1) {
            resultHeight = image.getHeight();
            resultWidth = (int) (resultHeight * getWidthRatio(corners));
        } else {
            resultWidth = image.getWidth();
            resultHeight = (int) (resultWidth * getHeightRatio(corners));
        }

        Mat inputMat = new Mat();
        Utils.bitmapToMat(image, inputMat);
        Mat outputMat = new Mat(resultWidth, resultHeight, CvType.CV_8UC4);

        List<Point> dest = new ArrayList<>();
        dest.add(new Point(0, 0));
        dest.add(new Point(resultWidth, 0));
        dest.add(new Point(resultWidth, resultHeight));
        dest.add(new Point(0, resultHeight));
        Mat endM = Converters.vector_Point2f_to_Mat(dest);

        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(startM, endM);

        Imgproc.warpPerspective(inputMat,
                outputMat,
                perspectiveTransform,
                new Size(resultWidth, resultHeight),
                Imgproc.INTER_CUBIC);
        image.recycle();
        Bitmap output = Bitmap.createBitmap(outputMat.width(), outputMat.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(outputMat, output);
        return output;
    }

    public static Bitmap adjustBrightness(Bitmap bitmap, double brightness) {
        Mat src = new Mat();

        Utils.bitmapToMat(bitmap, src);
        src.convertTo(src, -1, CONTRAST_DEF_VALUE, brightness);
        Utils.matToBitmap(src, bitmap);

        src.release();
        return bitmap;
    }

    public static Bitmap adjustContrast(Bitmap bitmap, double contrast) {
        Mat src = new Mat();
        double brightness;

        if (contrast == CONTRAST_DEF_VALUE) {
            brightness = BRIGHTNESS_DEF_VALUE;

        } else if (contrast < CONTRAST_DEF_VALUE) {
            brightness = (BRIGHTNESS_MIN_VALUE) * (contrast - CONTRAST_DEF_VALUE);

        }  else {
            brightness = BRIGHTNESS_MIN_VALUE *
                    (contrast - CONTRAST_DEF_VALUE) / (CONTRAST_MAX_VALUE - CONTRAST_DEF_VALUE)
                    + CONTRAST_DEF_VALUE;
        }

        Log.v(LOG_TAG, "contrast = " + contrast + ", brightness = " + brightness);

        Utils.bitmapToMat(bitmap, src);

        adjustGamma(src, GAMMA_DEF_VALUE - ((GAMMA_DEF_VALUE - GAMMA_MIN_VALUE)
                * (contrast - CONTRAST_DEF_VALUE) / (CONTRAST_MAX_VALUE - CONTRAST_DEF_VALUE)));

        src.convertTo(src, -1, contrast, brightness);
        Utils.matToBitmap(src, bitmap);

        src.release();

        return bitmap;
    }

    public static void adjustGamma(Mat im, double gamma) {
        Log.v(LOG_TAG, "adjustGamma: " + gamma);
        Mat lut = new Mat(1, 256, CvType.CV_8UC1);
        lut.setTo(new Scalar(0));

        for (int i = 0; i < 256; i++) {
            lut.put(0, i, Math.pow((double) (1.0 * i / 255), 1 / gamma) * 255);
        }
        Core.LUT(im, lut, im);
    }

    public static Bitmap cvt(Bitmap bitmap) {
        Mat src = new Mat();
        Utils.bitmapToMat(bitmap, src);
        Imgproc.cvtColor(src, src, Imgproc.COLOR_BGRA2GRAY);
        Utils.matToBitmap(src, bitmap);
        return bitmap;
    }

    public static Bitmap binarization(Bitmap bitmap) {
        Mat mat = new Mat();
        Utils.bitmapToMat(bitmap, mat);
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGRA2GRAY);
        Imgproc.medianBlur(mat, mat, 5);
        Imgproc.adaptiveThreshold(mat, mat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 9, 6);
        Utils.matToBitmap(mat, bitmap);
        return bitmap;
    }

    private static Point getMassCenter(ArrayList<Point> corners) {
        Point center = new Point(0, 0);
        for (Point point : corners) {
            center.x += point.x;
            center.y += point.y;
        }
        center.x /= 4;
        center.y /= 4;

        return center;
    }

    private static ArrayList<Point> sortCorners(ArrayList<Point> corners) {

        ArrayList<Point> top, bot;
        top = new ArrayList<>();
        bot = new ArrayList<>();
        Point center = getMassCenter(corners);
        for (int i = 0; i < corners.size(); i++) {
            if (corners.get(i).y <= center.y)
                top.add(corners.get(i));
            else
                bot.add(corners.get(i));
        }

        if (top.size() == 2 && bot.size() == 2) {
            corners.clear();

            Point tl = top.get(0).x > top.get(1).x ? top.get(1) : top.get(0);
            Point tr = top.get(0).x > top.get(1).x ? top.get(0) : top.get(1);
            Point bl = bot.get(0).x > bot.get(1).x ? bot.get(1) : bot.get(0);
            Point br = bot.get(0).x > bot.get(1).x ? bot.get(0) : bot.get(1);

            corners.add(tl);
            corners.add(tr);
            corners.add(br);
            corners.add(bl);
        }
        return corners;
    }

    public static Bitmap rotateBitmap(Bitmap image, int angle) {
//        OpenCVLoader.initDebug();
        Mat imageMat = new Mat();
        Utils.bitmapToMat(image, imageMat);
        Mat rotated = new Mat();
        for (int i = 0; i < angle / 90; i++) {
            rotated.release();
            Core.transpose(imageMat, rotated);
            Core.flip(rotated, rotated, 1);
            imageMat = rotated.clone();
        }

        Bitmap result = Bitmap.createBitmap(imageMat.width(), imageMat.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(imageMat, result);
        image.recycle();
        return result;
    }

    public static Bitmap rotateHardWay(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public static ArrayList<Point> getConvexHull(ArrayList<Point> input) {
//        OpenCVLoader.initDebug();
        Point[] points = new Point[input.size()];
        input.toArray(points);
        MatOfPoint matOfPoint = new MatOfPoint(points);
        MatOfInt indexes = new MatOfInt();
        Imgproc.convexHull(matOfPoint, indexes);
        ArrayList<Point> result = new ArrayList<>();
        int[] idx = indexes.toArray();
        if (idx.length == 4) {
            for (int i = 0; i < input.size(); i++) {
                result.add(input.get(idx[i]));
            }
            input.clear();
            return result;
        } else {
            return input;
        }
    }

}