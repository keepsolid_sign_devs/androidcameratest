package com.testproject.sign.cameratest.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.testproject.sign.cameratest.R;


public class RoundedImageView extends ImageView {

    protected Paint backgroundPaint;
    private int backgroundColor;

    public RoundedImageView(Context context) {
        super(context);
        init();
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        getAttributeFromXml(context, attrs);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        getAttributeFromXml(context, attrs);
    }

    private void init() {
        backgroundPaint = new Paint();
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setAntiAlias(true);
    }

    private void getAttributeFromXml(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(
                attrs,
                R.styleable.RoundedImageView);

        try {
            backgroundColor = a.getColor(R.styleable.RoundedImageView_colorBkg, Color.TRANSPARENT);
        } finally {
            a.recycle();
        }
        backgroundPaint.setColor(backgroundColor);
        this.invalidate();
    }

    public void setColorBkgByResId(@ColorRes int color) {
        backgroundColor = color;
        backgroundPaint.setColor(getContext().getResources().getColor(backgroundColor));
        this.invalidate();
    }

    public void setColorBkg(int color) {
        backgroundColor = color;
        backgroundPaint.setColor(backgroundColor);
        this.invalidate();
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, getHeight() / 2, backgroundPaint);

        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }

        Bitmap b = ((BitmapDrawable) drawable).getBitmap();
        canvas.drawBitmap(b, (getWidth() - b.getWidth()) / 2, (getHeight() - b.getHeight()) / 2, null);
    }
}
