package com.testproject.sign.cameratest.listeners;

import android.content.Intent;

public interface ActivityListener {

    void onActivityResult(int requestCode, int resultCode, Intent data);

}