package com.testproject.sign.cameratest.listeners;

public interface EditDialogListener {

        void onConfirmed(String result);
    }