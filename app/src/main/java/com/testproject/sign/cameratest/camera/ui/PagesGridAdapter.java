package com.testproject.sign.cameratest.camera.ui;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.adapters.DeleteItemListener;
import com.testproject.sign.cameratest.adapters.ItemTouchHelperAdapter;
import com.testproject.sign.cameratest.adapters.ItemTouchHelperViewHolder;
import com.testproject.sign.cameratest.adapters.OnStartDragListener;
import com.testproject.sign.cameratest.base_ui.RecyclingImageView;
import com.testproject.sign.cameratest.camera.callbacks.CameraOperationListener;
import com.testproject.sign.cameratest.camera.util.ImageLoadingObserver;
import com.testproject.sign.cameratest.camera.util.ImageSize;
import com.testproject.sign.cameratest.camera.util.ImageState;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.PxDpMetrics;
import com.testproject.sign.cameratest.camera.util.RecyclingBitmapDrawable;

import java.util.ArrayList;
import java.util.List;

public class PagesGridAdapter extends RecyclerView.Adapter<PagesGridAdapter.ViewHolder> implements ItemTouchHelperAdapter, ImageLoadingObserver {

    private List<String> imgKeys = new ArrayList<>();
    private OnStartDragListener startDragListener;
    private DeleteItemListener deleteItemListener;
    private SparseArray<String> waitingForSaveImages = new SparseArray<>();
    private ViewHolder draggedView;
    private DeletePageItem deletePageView;

    private boolean wasMoved = false;
    private int movementFlags = 0; //to prevent deleting item just below delete panel without moving it

    public PagesGridAdapter(List<String> imgKeys) {
        this.imgKeys = imgKeys;
    }

    public PagesGridAdapter(List<String> imgKeys, OnStartDragListener startDragListener) {
        this.imgKeys = imgKeys;
        this.startDragListener = startDragListener;
    }

    public PagesGridAdapter(List<String> imgKeys, OnStartDragListener startDragListener,
                            DeleteItemListener deleteItemListener) {
        this.imgKeys = imgKeys;
        this.startDragListener = startDragListener;
        this.deleteItemListener = deleteItemListener;
    }

    public void setDeletePageView(DeletePageItem deletePageView) {
        this.deletePageView = deletePageView;
    }

    public void setStartDragListener(OnStartDragListener startDragListener) {
        this.startDragListener = startDragListener;
    }

    public void setDeleteItemListener(DeleteItemListener deleteItemListener) {
        this.deleteItemListener = deleteItemListener;
    }

    public void setData(List<String> imgKeys) {
        this.imgKeys = imgKeys;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ViewHolder holder = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.camera_page_grid_item, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String imgKey = imgKeys.get(position);
        holder.setData(imgKey, position);
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return imgKeys.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemReallyMove(int fromPosition, int toPosition) {
        checkForDelete();
        if (draggedView != null) {
            int positionStart = Math.min(fromPosition, toPosition);
            int positionEnd = Math.max(fromPosition, toPosition);
            updateData(fromPosition, toPosition);
            notifyItemRangeChanged(positionStart, positionEnd - positionStart + 1);
        }
    }

    private void updateData(int fromPosition, int toPosition) {
        if (fromPosition > toPosition) {
            for (int i = fromPosition; i > toPosition; i--) {
                KSImageCache.getInstance().swapImagesOrder(i, i - 1);
            }
        } else {
            for (int i = fromPosition; i < toPosition; i++) {
                KSImageCache.getInstance().swapImagesOrder(i, i + 1);
            }
        }
        imgKeys = new ArrayList<>(KSImageCache.getInstance().getCachedImagesKeySet());
    }

    @Override
    public void onItemDismiss(int position) {
        //do nothing yet
    }

    @Override
    public void onItemDragging(MotionEvent event) {
        movementFlags++;
        if (movementFlags > 10) {
            wasMoved = true;
            if (draggedView != null && deletePageView != null && deletePageView.getDeleteArea() != null) {
                deletePageView.setHovered(isOverlapping(deletePageView.getDeleteArea(), draggedView.itemView));
            }
        }
    }

    @Override
    public void onItemDropped(MotionEvent event) {
        checkForDelete();
    }

    private void checkForDelete() {
        if (deletePageView != null && deletePageView.getDeleteArea() != null && draggedView != null &&
                isOverlapping(deletePageView.getDeleteArea(), draggedView.itemView)
                && wasMoved) {
            int pos = Integer.parseInt(draggedView.itemView.getTag().toString());
            draggedView = null;
            deleteItem(pos);
        }
    }

    private static boolean isOverlapping(View firstView, View secondView) {
        int[] firstPosition = new int[2];
        int[] secondPosition = new int[2];

        firstView.getLocationOnScreen(firstPosition);
        secondView.getLocationOnScreen(secondPosition);

        int margin = PxDpMetrics.convertDpToPixel(15);

        Rect firstViewRect = new Rect(firstPosition[0], firstPosition[1],
                firstPosition[0] + firstView.getMeasuredWidth(), firstPosition[1] + firstView.getMeasuredHeight());
        Rect secondViewRect = new Rect(secondPosition[0] + margin, secondPosition[1],
                secondPosition[0] + secondView.getMeasuredWidth(), secondPosition[1] + secondView.getMeasuredHeight() - margin);

        return firstViewRect.intersect(secondViewRect);
    }

    private void deleteItem(int position) {
        if (deleteItemListener != null) {
            deleteItemListener.onItemDeleted(position, imgKeys.get(position));
        }
        imgKeys.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public void notifyLoadStarted(String imageTag) {

    }

    @Override
    public void notifyLoadFinished(String imageTag) {
        for (int i = 0; i < waitingForSaveImages.size(); i++) {
            int position = waitingForSaveImages.keyAt(i);
            String imageKey = waitingForSaveImages.get(position);
            if (imageKey.equals(imageTag)) {
                notifyItemChanged(position);
                waitingForSaveImages.remove(position);
                break;
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnLongClickListener {

        RecyclingImageView imageView;
        TextView pageTv;
        View selectionLayout;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = (RecyclingImageView) itemView.findViewById(R.id.page_grid_icon);
            pageTv = (TextView) itemView.findViewById(R.id.page_counter);
            selectionLayout = itemView.findViewById(R.id.page_selection_layout);
            selectionLayout.setVisibility(View.GONE);
            itemView.setOnLongClickListener(this);
        }

        void setData(String imageKey, int position) {
            if (KSImageCache.getInstance().getImageState(imageKey) == ImageState.IS_SAVING) {
                waitingForSaveImages.put(position, imageKey);
            }
            KSImageCache.getInstance().getImgAsync(imageKey, ImageSize.MIN, new CameraOperationListener<RecyclingBitmapDrawable>() {
                @Override
                public void onSuccess(RecyclingBitmapDrawable result) {
                    if (result != null)
                        imageView.setImageDrawable(result);
                }
            });
            pageTv.setText(String.valueOf(position + 1));
        }

        @Override
        public void onItemSelected() {
            selectionLayout.setVisibility(View.VISIBLE);
        }

        @Override
        public void onItemClear() {
            wasMoved = false;
            movementFlags = 0;
            selectionLayout.setVisibility(View.GONE);
            if (startDragListener != null) {
                startDragListener.onFinishDrag(draggedView);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (startDragListener != null) {
                PagesGridAdapter.this.draggedView = ViewHolder.this;
                startDragListener.onStartDrag(ViewHolder.this);
                return true;
            }
            return false;
        }
    }
}
