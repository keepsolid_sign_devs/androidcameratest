package com.testproject.sign.cameratest.camera.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.camera.callbacks.CameraOperationListener;
import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;
import com.testproject.sign.cameratest.camera.options.EditedImage;
import com.testproject.sign.cameratest.camera.options.EditedImagesManager;
import com.testproject.sign.cameratest.camera.options.EffectsConstants;
import com.testproject.sign.cameratest.camera.options.EffectsControl;
import com.testproject.sign.cameratest.camera.util.ImageSize;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.Log;
import com.testproject.sign.cameratest.camera.util.MemoryHelper;
import com.testproject.sign.cameratest.camera.util.RecyclingBitmapDrawable;
import com.testproject.sign.cameratest.util.ProgressDialogController;
import com.testproject.sign.cameratest.util.UIThreadHelper;

import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_DEF_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_MAX_VALUE;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.CONTRAST_MIN_VALUE;
import static com.testproject.sign.cameratest.camera.ui.FilterControlFragment.TUNE_MODE_KEY;

public class ImageFragment extends AbstractFragment implements OnTuneEventListener{

    private static final String IMG_NAME_KEY = "image_name";
    private static final String TAG = ImageFragment.class.getSimpleName();

    private TuneMode tuneMode;
    private double originalTuneValue;
    private EditedImage editedImage;

    private EditImageView docImage;
    private Bitmap originalImage;
    private Bitmap previewImage;

    public static ImageFragment newInstance(@NonNull String imageKey, TuneMode mode) {
        ImageFragment fragment = new ImageFragment();
        Bundle args = new Bundle();
        args.putString(IMG_NAME_KEY, imageKey);
        args.putSerializable(TUNE_MODE_KEY, mode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String imageId = getArguments().getString(IMG_NAME_KEY);
        editedImage = EditedImagesManager.getInstance().get(imageId);
        TuneMode tuneMode = (TuneMode) getArguments().getSerializable(TUNE_MODE_KEY);
        setTuneMode(tuneMode);
    }

    @Override
    public void onCreateView(LayoutInflater inflater) {
        setContentView(R.layout.gallery_photo_item);
        docImage = (EditImageView) findViewById(R.id.gallery_item_iv);
        getImage(new CameraOperationListener<RecyclingBitmapDrawable>() {
            @Override
            public void onSuccess(RecyclingBitmapDrawable result) {
                setImage(result);
            }
        });
    }

    private void getImage(CameraOperationListener<RecyclingBitmapDrawable> listener) {
//        if (tuneMode == TuneMode.NONE) {
//            KSImageCache.getInstance().getImgAsync(editedImage.getImageId(), ImageSize.MEDIUM, listener);
//        } else {
            KSImageCache.getInstance().getImgAsync(editedImage.getImageId(), ImageSize.MEDIUM, listener);
//        }
    }

    private void setImage(RecyclingBitmapDrawable newImage) {
//        if (tuneMode == TuneMode.NONE) {
//            originalImage = newImage.getBitmap();
//            previewImage = MemoryHelper.setNewBitmap(previewImage, newImage.getBitmap().copy(newImage.getBitmap().getConfig(), true));
//            docImage.setImageBitmap(previewImage);
//        } else {
            originalImage = editedImage.transformImage(newImage.getBitmap());
            previewImage = MemoryHelper.setNewBitmap(previewImage, originalImage.copy(originalImage.getConfig(), true));
            previewImage = editedImage.executeColorEffects(previewImage);
            docImage.setImageBitmap(previewImage);
//        }
    }

    @Override
    public void onLockActions() {

    }

    private void setTuneMode(TuneMode newMode) {
        tuneMode = newMode;

        if (tuneMode == TuneMode.NONE) {
            return;
        }
        originalTuneValue = getTuneValue();
    }

    public double getTuneValue() {
        return getTuneValueForMode(tuneMode);
    }

    public double getTuneValueForMode(TuneMode tuneMode) {
        switch(tuneMode) {
            case CONTRAST:
                return editedImage.getContrast();
            case BRIGHTNESS:
                return editedImage.getBrightness();
            case COLOR:
                return editedImage.getColor();
            default:
                return 0;
        }
    }

    public void setTuneValue(double value) {
        switch(tuneMode) {
            case CONTRAST:
                editedImage.setContrast(value);
                break;
            case BRIGHTNESS:
                editedImage.setBrightness(value);
                break;
            case COLOR:
                editedImage.setColor(value);
                break;
        }
    }

    @Override
    public int onTuneModeChanged(TuneMode mode) {
        tuneMode = mode;

        return (int) getTuneValue();
    }

    private int getDefaultTuneValue() {
        if (tuneMode == null) {
            return DEFAULT_TUNE_VALUE;
        }
        switch (tuneMode) {
            case CONTRAST:
                return (int) ((CONTRAST_DEF_VALUE - CONTRAST_MIN_VALUE) * 100 / (CONTRAST_MAX_VALUE - CONTRAST_MIN_VALUE));
            case COLOR:
                return EffectsConstants.COLOR_DEF_VALUE;
            case BRIGHTNESS:
                return (int) EffectsConstants.BRIGHTNESS_DEF_VALUE;
            default:
                return DEFAULT_TUNE_VALUE;
        }
    }

    @Override
    public void onTuneValueChanged(double value, boolean isConfirmed) {
        if (tuneMode == null || tuneMode == TuneMode.NONE) {
            return;
        }
        setTuneValue(value);

        Bitmap result = editedImage.executeColorEffects(getOriginalImageCopy());

        setNewBitmap(result);
    }

    @Override
    public void onConfirmButtonPressed() {
        if (tuneMode == null || getTuneValue() == getDefaultTuneValue()
                || getTuneValue() == originalTuneValue) {
            return;
        }
        onCloseActions();
    }

    @Override
    public void onCancelTune() {
        setTuneValue(originalTuneValue);
        onCloseActions();
    }

    public void onCloseActions() {
        recycleImages();
    }

    private void setNewBitmap(Bitmap newObject) {
        if (newObject != null && !newObject.isRecycled()) {
            if (previewImage != null && !previewImage.isRecycled()) {
                previewImage.recycle();
            }
            previewImage = newObject;
            docImage.setImageBitmap(previewImage);
        }
    }

    public String getImageId() {
        return editedImage.getImageId();
    }

    private Bitmap getOriginalImageCopy() {
        return originalImage.copy(originalImage.getConfig(), true);
    }

    public void rotateImage(int degree) {
        editedImage.setRotation(degree);
        ProgressDialogController.getInstance().showTransparentProgressBar(false);
        KSImageCache.getInstance().getImgAsync(editedImage.getImageId(), ImageSize.MEDIUM, new CameraOperationListener<RecyclingBitmapDrawable>() {
                    @Override
                    public void onSuccess(RecyclingBitmapDrawable result) {
                        final Bitmap resultBitmap = editedImage.executeAllEffects(
                                result.getBitmap().copy(result.getBitmap().getConfig(), true));
//                        final Bitmap rotatedImage = editedImage.rotateImage(resultBitmap);
                        UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                setNewBitmap(resultBitmap);
                                ProgressDialogController.getInstance().hideProgressDialog();
                            }
                        });
                    }
                });
    }

    private void recycleImages() {
        if (originalImage != null && !originalImage.isRecycled()) {
            originalImage.recycle();
        }
        if (previewImage != null && !previewImage.isRecycled()) {
            previewImage.recycle();
        }
    }

}
