package com.testproject.sign.cameratest.camera.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;
import com.testproject.sign.cameratest.camera.options.EffectsConstants;

public class FilterView extends RelativeLayout {

    private OnTuneEventListener onTuneEventListener;

    public static final int DEFAULT_TUNE_VALUE = 50;

    private ImageView tuneModeImage;
    private SeekBar tuneSeekBar;

    private OnTuneEventListener.TuneMode mode = OnTuneEventListener.TuneMode.NONE;

    public FilterView(Context context) {
        super(context);
        initView();
    }

    public FilterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public FilterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.filter_view, this);
        tuneSeekBar = (SeekBar) findViewById(R.id.tune_seek_bar);
        tuneModeImage = (ImageView) findViewById(R.id.tune_mode_img);
        FrameLayout tuneOkBtn = (FrameLayout) findViewById(R.id.camera_tune_ok_btn);
        tuneOkBtn.setOnClickListener(onTuneOkClick);
        tuneSeekBar.setOnSeekBarChangeListener(onTuneValueChangedListener);
    }

    public OnTuneEventListener.TuneMode getMode() {
        return mode;
    }

    public void setOnTuneEventListener(OnTuneEventListener onTuneEventListener) {
        this.onTuneEventListener = onTuneEventListener;
    }

    public void setMode(OnTuneEventListener.TuneMode mode) {
        this.mode = mode;
        int defaultProgress = DEFAULT_TUNE_VALUE;
        switch (mode) {
            case COLOR:
                if (onTuneEventListener != null) {
                    defaultProgress = onTuneEventListener.onTuneModeChanged(OnTuneEventListener.TuneMode.COLOR);
                }
                tuneModeImage.setImageDrawable(getResources().getDrawable(R.drawable.icon_filter_color_tune));
                break;
            case BRIGHTNESS:
                if (onTuneEventListener != null) {
                    defaultProgress = onTuneEventListener.onTuneModeChanged(OnTuneEventListener.TuneMode.BRIGHTNESS);
                }
                tuneModeImage.setImageDrawable(getResources().getDrawable(R.drawable.icon_filter_exposition));
                break;
            case CONTRAST:
                if (onTuneEventListener != null) {
                    defaultProgress = onTuneEventListener.onTuneModeChanged(OnTuneEventListener.TuneMode.CONTRAST);
                }
                tuneModeImage.setImageDrawable(getResources().getDrawable(R.drawable.icon_filter_contrast));
                break;
        }

        setTuneValue(defaultProgress);
    }

    public void setTuneValue(int value) {
        tuneSeekBar.setProgress(value);
    }

    private void transformTuneValueForMode() {
        int originalValue = tuneSeekBar.getProgress();

        switch (mode) {
            case COLOR:
                if (originalValue < 25) {
                    setTuneValue(0);
                } else if (originalValue >= 25 && originalValue < 75) {
                    setTuneValue(50);
                } else if (originalValue >= 75) {
                    setTuneValue(100);
                }
                break;
        }
    }

    private OnClickListener onTuneOkClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onTuneEventListener != null) {
                onTuneEventListener.onConfirmButtonPressed();
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener onTuneValueChangedListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if (onTuneEventListener != null) {
                transformTuneValueForMode();
                onTuneEventListener.onTuneValueChanged((double) seekBar.getProgress(), false);
            }
        }
    };

}
