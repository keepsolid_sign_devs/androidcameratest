package com.testproject.sign.cameratest.camera.options;

/**
 * Created by prog on 03.08.16.
 */
public interface EffectsConstants {

    float NORMAL_ZOOM = 1f;
    float MAX_ZOOM = 2f;
    int MAX_ROTATION_DEGREES = 45;

    double BRIGHTNESS_MAX_VALUE = 100d;
    double BRIGHTNESS_MIN_VALUE = -100d;
    double BRIGHTNESS_DEF_VALUE = 0d;

    double CONTRAST_MAX_VALUE = 2.8d;
    double CONTRAST_MIN_VALUE = 0.3d;
    double CONTRAST_DEF_VALUE = 1.0d;

    double GAMMA_DEF_VALUE = 1.0d;
    double GAMMA_MIN_VALUE = 0.5d;

    int COLOR_DEF_VALUE = 0;
    int COLOR_CVT = 1;
    int COLOR_BINARIZATION = 2;

    int ROTATION_IMAGEVIEW = 0;
    int ROTATION_BITMAP = 1;

    int ROTATE_LEFT = -90;
    int ROTATE_RIGHT = 90;

}
