package com.testproject.sign.cameratest.camera.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

public class PxDpMetrics {
    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @return An int value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(float dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @return An int value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixelAlternative(float dp) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics()));
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return An int value to represent dp equivalent to px value
     */
    public static int convertPixelsToDp(float px, Context context) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int convertPointsToPx(float points) {
        return (int) ((points * 96f) / 72 + 0.5f);
    }
}
