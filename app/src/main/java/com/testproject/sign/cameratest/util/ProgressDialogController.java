package com.testproject.sign.cameratest.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Build;
import android.util.Log;

import com.testproject.sign.cameratest.R;


public class ProgressDialogController {
    private static volatile ProgressDialogController ourInstance;
    private Activity activity;
    private ProgressDialog progressDialog;
    private ProgressDialog criticalProgressDialog;

    public static ProgressDialogController getInstance() {
        if (ourInstance == null) {
            synchronized (ProgressDialogController.class) {
                ourInstance = new ProgressDialogController();
            }
        }
        return ourInstance;
    }

    private ProgressDialogController() {
    }

    public void init(Activity activity) {
        this.activity = activity;
    }

    public void showProgressDialog(final String title, final boolean cancelable) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.hide();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    progressDialog = new ProgressDialog(activity, R.style.HankoDialog);
                } else {
                    progressDialog = new ProgressDialog(activity, R.style.HankoHoloDialog);
                }

                progressDialog.setMessage(title);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(cancelable);
                ProgressDialogController.this.showDialog();
            }
        });

    }

    private void showDialog() {
        try {
            progressDialog.show();
        } catch (Exception e) {

        }
    }

    public void showProgressDialog(final String title, final boolean cancelable, final Dialog.OnDismissListener listener) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.hide();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    progressDialog = new ProgressDialog(activity, R.style.HankoDialog);
                } else {
                    progressDialog = new ProgressDialog(activity, R.style.HankoHoloDialog);
                }
                progressDialog.setMessage(title);
                progressDialog.setOnDismissListener(listener);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(cancelable);
                ProgressDialogController.this.showDialog();
            }
        });

    }

    public void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } finally {
                    progressDialog = null;
                }
            }
        });
    }

    public void showCriticalProgressDialog(final String title) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (criticalProgressDialog != null)
                    criticalProgressDialog.hide();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    criticalProgressDialog = new ProgressDialog(activity, R.style.HankoDialog);
                } else {
                    criticalProgressDialog = new ProgressDialog(activity, R.style.HankoHoloDialog);
                }

                criticalProgressDialog.setMessage(title);
                criticalProgressDialog.setIndeterminate(true);
                criticalProgressDialog.setCancelable(false);
                criticalProgressDialog.show();
            }
        });
    }

    public void hideCriticalProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (criticalProgressDialog != null) {
                    criticalProgressDialog.hide();
                }
            }
        });
    }

    private void runOnUiThread(Runnable r) {
        if (activity != null && !activity.isFinishing()) {
            activity.runOnUiThread(r);
        }
    }

    public void dismissProgressDialogs() {
        if (progressDialog != null && progressDialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        progressDialog.dismiss();
                    } catch (final Exception e) {
                        ///shit
                    } finally {
                        progressDialog = null;
                    }
                }
            });
        }

        if (criticalProgressDialog != null && criticalProgressDialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        criticalProgressDialog.dismiss();
                    } catch (final Exception e) {
                        ///shit
                    } finally {
                        criticalProgressDialog = null;
                    }
                }
            });
        }
    }

    public void showTransparentProgressBar(final boolean cancelable) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    try {
                        progressDialog.dismiss();
                    } catch (final Exception e) {
                        Log.v("ProgressDialogController", e.getMessage());
                    } finally {
                        progressDialog = null;
                    }
                }

                progressDialog = new ProgressDialog(activity, R.style.ProgressDialog);
                progressDialog.setCancelable(cancelable);
                ProgressDialogController.this.showDialog();
            }
        });
    }
}
