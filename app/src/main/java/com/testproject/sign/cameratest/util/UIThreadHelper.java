package com.testproject.sign.cameratest.util;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

/**
 * Created by prog on 15.08.16.
 */
public class UIThreadHelper {

    public static void runOnUIThread(Activity activity, Runnable task) {
        if (activity != null && !activity.isFinishing()) {
            activity.runOnUiThread(task);
        }
    }

    public static void runOnUIThread(Context context, Runnable task) {
        if (context != null) {
            Handler handler = new Handler(context.getMainLooper());
            handler.post(task);
        }
    }

    public static void runOnUIThreadWithDelay(Context context, Runnable task, long delayMillis) {
        if (context != null) {
            Handler handler = new Handler(context.getMainLooper());
            handler.postDelayed(task, delayMillis);
        }
    }
}
