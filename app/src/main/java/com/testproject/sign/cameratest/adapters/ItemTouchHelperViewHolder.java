package com.testproject.sign.cameratest.adapters;

public interface ItemTouchHelperViewHolder {
    /**
      * Implementations should update the item view to indicate it's active state.
     */
    void onItemSelected();


    /**
     * state should be cleared.
     */
    void onItemClear();
}