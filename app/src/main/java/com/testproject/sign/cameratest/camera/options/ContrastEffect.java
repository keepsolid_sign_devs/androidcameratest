package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import com.testproject.sign.cameratest.camera.ui.EditImageView;


/**
 * Created by prog on 03.08.16.
 */
public class ContrastEffect extends Effect {

    public static final String LOG_TAG = ContrastEffect.class.getSimpleName();

    public ContrastEffect(double value) {
        super(value);
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.CONTRAST;
    }

    @Override
    public Bitmap execute(Bitmap originalImage) {
        double progress = countValueProportionally(value, EffectsConstants.CONTRAST_MIN_VALUE, EffectsConstants.CONTRAST_MAX_VALUE);

        return adjustContrast(progress, originalImage);
    }

    private Bitmap adjustContrast(double contrast, Bitmap originalImage) {
        return ImageProcessor.adjustContrast(originalImage, contrast);
    }

    @Override
    public void undo() {

    }
}
