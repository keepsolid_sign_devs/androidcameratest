package com.testproject.sign.cameratest;

import android.Manifest;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.testproject.sign.cameratest.base_ui.AbstractActivity;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.util.ViewUtils;
import com.testproject.sign.cameratest.util.PermissionManager;
import com.testproject.sign.cameratest.util.PermissionsObserver;
import com.testproject.sign.cameratest.util.SystemMessageHelper;

public class MainActivity extends AbstractActivity implements PermissionsObserver{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PermissionManager.getInstance().init(this);
        CoreSignFragmentManager.getInstance().init(this, findViewById(R.id.rootContainer));
        PermissionManager.getInstance().registerPermissionObserver(this);
        ViewUtils.init(this);
        if (!PermissionManager.getInstance().isObtained(Manifest.permission.CAMERA)) {
            PermissionManager.getInstance().requestPermission(Manifest.permission.CAMERA, PermissionManager.MY_CAMERA_PERMISSION_CODE);
        } else {
            CoreSignFragmentManager.getInstance().showCameraNewPhotoFragment(CameraMode.DOCUMENT);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PermissionManager.getInstance().unregisterPermissionObserver(this);
    }

    @Override
    public void onPermissionGranted(String[] permissions, int permissionRequestCode) {
        if (permissionRequestCode == PermissionManager.MY_CAMERA_PERMISSION_CODE) {
            CoreSignFragmentManager.getInstance().showCameraNewPhotoFragment(CameraMode.DOCUMENT);
        }
    }

    @Override
    public void onPermissionDenied(String[] permissions, int permissionRequestCode) {
        CoreSignFragmentManager.getInstance().showCameraNewPhotoFragment(CameraMode.DOCUMENT);
    }
}
