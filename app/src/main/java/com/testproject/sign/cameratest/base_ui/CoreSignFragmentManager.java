package com.testproject.sign.cameratest.base_ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;
import com.testproject.sign.cameratest.camera.ui.CameraGalleryFragment;
import com.testproject.sign.cameratest.camera.ui.CameraNewPhotoFragment;
import com.testproject.sign.cameratest.camera.ui.CameraOptionsFragment;
import com.testproject.sign.cameratest.camera.ui.CropImageFragment;
import com.testproject.sign.cameratest.camera.ui.FilterControlFragment;
import com.testproject.sign.cameratest.camera.ui.GalleryMainFragment;
import com.testproject.sign.cameratest.camera.ui.MovePagesFragment;

import static com.testproject.sign.cameratest.camera.ui.GalleryMainFragment.CAMERA_MODE_KEY;

public class CoreSignFragmentManager extends AppFragmentManager {

    private static CoreSignFragmentManager ourInstance;

    private CameraNewPhotoFragment cameraNewPhotoFragment;
    private CameraOptionsFragment cameraOptionsFragment;

    public enum OpeningDocumentsFragmentTypes {
        ROOT,
        ARCHIVE,
        TEMPLATE,
        NOTIFICATIONS
    }

    public static synchronized CoreSignFragmentManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new CoreSignFragmentManager();
        }
        return ourInstance;
    }

    private CoreSignFragmentManager() {
    }

    @Override
    public void init(AbstractActivity activity, View container) {
        super.init(activity, container);
        initAppFragments();
    }

    private void initAppFragments() {

        cameraNewPhotoFragment = new CameraNewPhotoFragment();
        cameraOptionsFragment = new CameraOptionsFragment();
    }

    public void closeAll() {
        getActivity().finish();
    }

    public void showCameraNewPhotoFragment(CameraMode cameraMode) {
        CameraNewPhotoFragment fragment = CameraNewPhotoFragment.newInstance();
        Bundle args = new Bundle();
        args.putSerializable(CAMERA_MODE_KEY, cameraMode);
        fragment.setArguments(args);
        showFragment(fragment);
    }

    public void showCameraGalleryFragment(CameraMode cameraMode) {
        showCameraGalleryFragment(cameraMode, 0);
    }

    public void showCameraGalleryFragment(CameraMode cameraMode, int page) {
        CameraGalleryFragment fragment = CameraGalleryFragment.newInstance();
        Bundle args = new Bundle();
        args.putSerializable(CAMERA_MODE_KEY, cameraMode);
        fragment.setArguments(args);
        showFragment(GalleryMainFragment.newInstance(cameraMode, page));
    }

    public void showCameraOptionsFragment(String imageTag, CameraMode cameraMode) {
        AbstractFragment fragment = CameraOptionsFragment.newInstance();
        Bundle args = new Bundle();
        args.putString(CameraOptionsFragment.DISPLAY_IMAGE_NAME_KEY, imageTag);
        args.putSerializable(CAMERA_MODE_KEY, cameraMode);
        fragment.setArguments(args);
        showFragment(fragment);
    }

    public void showMovePagesFragments(){
        showFragment(MovePagesFragment.newInstance());
    }

    public void showFilterControlFragment(@NonNull CameraMode cameraMode, @NonNull OnTuneEventListener.TuneMode tuneMode, int page) {
        showFragment(FilterControlFragment.newInstance(cameraMode, tuneMode, page));
    }

    public void showCropImageFragment(@NonNull CameraMode cameraMode, int page) {
        showFragment(CropImageFragment.newInstance(cameraMode, page));
    }

    public void showMessage(String message) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showMessage(message);
    }

    public void showSuccessMessage(String message) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showSuccessMessage(message);
    }

    public void showWarningMessage(String message) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showWarningMessage(message);
    }

    public void showErrorMessage(String message) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showErrorMessage(message);
    }

    public void showMessage(String message, String actionText, View.OnClickListener onClickListener) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showMessage(message, actionText, onClickListener);
    }

    public void showSuccessMessage(String message, String actionText, View.OnClickListener onClickListener) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showSuccessMessage(message, actionText, onClickListener);
    }

    public void showWarningMessage(String message, String actionText, View.OnClickListener onClickListener) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showWarningMessage(message, actionText, onClickListener);
    }

    public void showErrorMessage(String message, String actionText, View.OnClickListener onClickListener) {
        if (getCurrentFragment() == null) {
            return;
        }
        getCurrentFragment().showErrorMessage(message, actionText, onClickListener);
    }

}
