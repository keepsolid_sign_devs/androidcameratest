package com.testproject.sign.cameratest.adapters;

public interface DeleteItemListener {

    void onItemDeleted(int position, String tag);
}
