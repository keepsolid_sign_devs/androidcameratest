package com.testproject.sign.cameratest.camera.util;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;

import static android.content.Context.ACTIVITY_SERVICE;
import static android.content.pm.ApplicationInfo.FLAG_LARGE_HEAP;

/**
 * Created by prog on 25.10.16.
 */

public class MemoryHelper {

    static <T> T getService(Context context, String service) {
        return (T) context.getSystemService(service);
    }

    public static int calculateMemoryCacheSize(Context context) {
        ActivityManager am = getService(context, ACTIVITY_SERVICE);
        boolean largeHeap = (context.getApplicationInfo().flags & FLAG_LARGE_HEAP) != 0;
        int memoryClass = am.getMemoryClass();
        if (largeHeap) {
            memoryClass = am.getLargeMemoryClass();
        }
        return (int) (1024L * 1024L * memoryClass / 10);
    }

    public static Bitmap setNewBitmap(Bitmap reference, Bitmap newObject) {
        if (reference == newObject) {
            return reference;
        }
        if (newObject != null) {
            if (reference != null && !reference.isRecycled()) {
                reference.recycle();
            }
            reference = newObject;
        }
        return reference;
    }
}
