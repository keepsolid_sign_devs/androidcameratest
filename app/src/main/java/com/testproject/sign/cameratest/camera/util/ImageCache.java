package com.testproject.sign.cameratest.camera.util;

import android.content.Context;
import android.graphics.Bitmap;

import com.testproject.sign.cameratest.camera.callbacks.CameraOperationListener;

import java.util.Set;


public interface ImageCache {

    /**
     * Initialize cache with Context (Application context is preferred)
     *
     * @param context context
     */
    void init(Context context);

    /**
     * Get image from cache synchronously.
     *
     * @param key       image tag
     * @param imageSize you can get image of MAX size (from disk) -> big size, but heavy operation,
     *                  or you can get image of MEDIUM size(runtime cache) -> small, but you'll get it quickly
     * @return image
     */
    Bitmap get(String key, ImageSize imageSize);

    /**
     * Get image from cache asynchronously. Recommended to use for getting images of MAX size.
     *
     * @param key       image tag
     * @param imageSize you can get image of MAX size (from disk) -> big size, but heavy operation,
     *                  or you can get image of MEDIUM size(runtime cache) -> small, but you'll get it quickly
     * @param listener  result listener
     */
    void getImgAsync(String key, ImageSize imageSize, CameraOperationListener<Bitmap> listener);

    /**
     * Store an image in the cache for the specified {@code key}.
     */
    void set(String key, Bitmap bitmap);

    /**
     * Store an image in the cache for the specified {@code key} or {@code null} asynchronously.
     * Use {@link CameraOperationListener} to avoid UI lags.
     */

    void setImgAsync(String key, Bitmap bitmap, CameraOperationListener<Boolean> listener);

    /**
     * Removes image with given key if it exists
     *
     * @param key Key of the image that should be removed
     */
    void removeImage(String key);

    /**
     * Get parameter from 0 to 100 that displays quality of serialized image that we save on disk
     *
     * @return
     */
    int getSerializationQuality();

    /**
     * Set parameter from 0 to 100 that displays quality of serialized image that we save on disk
     *
     * @return
     */
    void setSerializationQuality(int serializationQuality);

    /**
     * Clears the cache.
     */
    void clear();

    /**
     * Register an observer to listen to image loading events
     * @param observer Listener
     */
    void registerObserver(ImageLoadingObserver observer);

    /**
     * Remove observer from observers list
     * @param observer Listener to remove
     */
    void unregisterObserver(ImageLoadingObserver observer);

    /**
     * Remove all listeners to avoid memory leaks
     */
    void unregisterAll();

    /**
     * Get set of keys of all images successfully saved to cache. Note, this integer does NOT include images that are is loading progress
     * @return set of keys of all images saved to cache
     */
    Set<String> getCachedImagesKeySet();

    /**
     * Get amount of images saved to cache. Note, this integer also includes images that are is loading progress
     * @return amount of images saved to cache
     */
    int getSavedImagesCount();

    /**
     * Get set of keys of all images that are is loading progress
     * @return set of keys of all images that are is loading progress
     */
    Set<String> getProgressImagesKeySet();

    /**
     * Get amount of images saved to cache. Note, this integer also includes images that are is loading progress
     * @return amount of images saved to cache
     */
    int getProgressImagesCount();

    /**
     * If there are some images that are in loading progress, they will be loaded
     * @param listener Callback to loading progress of all images that haven't been saved yet
     */
    void finishLoadingImagesInProgress(ImageLoadingObserver listener);

}
