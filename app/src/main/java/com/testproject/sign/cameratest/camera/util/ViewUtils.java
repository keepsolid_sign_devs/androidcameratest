package com.testproject.sign.cameratest.camera.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.util.DisplayModule;


/**
 * Crafted by tuule on 14.06.16 with love ♥.
 */
public class ViewUtils {

    public static Context context;

    private static final int MIN_COLUMNS_NUM = 2;
    private static final int MAX_COLUMNS_NUM = 4;

    public static void init(Context context) {
        ViewUtils.context = context;
    }

    public static int getStatusBarHeight() {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void hideSoftInput(View view) {
        if (view != null) {
            view.clearFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftInput(View view) {
        view.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static float getDensity() {
        return context.getResources().getDisplayMetrics().density;
    }

    public static Drawable getDrawable(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(id, null);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    public static int getWindowHeight() {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int getWindowWidth() {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static void setElevation(View view, float elevation) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.setElevation(elevation);
        }
    }

    public static int countGridViewColumns() {
        double width = DisplayModule.getDisplayWidth(context);
        double minColumnWidth = context.getResources().getDimension(R.dimen.document_grid_view_min_column_width);
        int countedColumnNum = (int) (width / minColumnWidth);
        return Math.min(Math.max(countedColumnNum, MIN_COLUMNS_NUM), MAX_COLUMNS_NUM);
    }

    public static boolean isOverlapping(View firstView, View secondView) {
        int[] firstPosition = new int[2];
        int[] secondPosition = new int[2];

        firstView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        secondView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        firstView.getLocationOnScreen(firstPosition);
        secondView.getLocationOnScreen(secondPosition);

        Rect firstViewRect = new Rect(firstPosition[0], firstPosition[1],
                firstPosition[0] + firstView.getMeasuredWidth(), firstPosition[1] + firstView.getMeasuredHeight());
        Rect secondViewRect = new Rect(secondPosition[0], secondPosition[1],
                secondPosition[0] + secondView.getMeasuredWidth(), secondPosition[1] + secondView.getMeasuredHeight());

        return firstViewRect.intersect(secondViewRect);
    }

    public static void setupUI(View view, final Activity activity) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    v.requestFocus();
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, activity);
            }
        }
    }

    public static void setScaledViewHeight(View view, int bitmapWidth, int bitmapHeight) {
        if (view != null) {
            int viewWidth = view.getWidth();
            final double viewWidthToBitmapWidthRatio = (double) viewWidth / bitmapWidth;

            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = (int) (bitmapHeight * viewWidthToBitmapWidthRatio);
            view.setLayoutParams(params);
        }
    }

    public static void setWidth(View view, int width) {
        if (view != null) {

            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.width = width;
            view.setLayoutParams(params);
        }
    }

    public static void setHeight(View view, int height) {
        if (view != null) {

            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = height;
            view.setLayoutParams(params);
        }
    }

    public static void setMargins(View view, int left, int top, int right, int bottom) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.setMargins(left, top, right, bottom);
        view.setLayoutParams(params);
    }

    public static void setMarginBottom(View view, int bottom) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottom);
        view.setLayoutParams(params);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    public static void scrollToStartOnFocusLost(final EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focused) {
                if (!focused) {
                    editText.setSelection(0);
                }
            }
        });
    }

    public static void setEnabled(View view, boolean isEnabled) {
        view.setEnabled(isEnabled);
        view.setClickable(isEnabled);
        view.setFocusable(isEnabled);
    }

}
