package com.testproject.sign.cameratest.util;

import android.content.Context;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.testproject.sign.cameratest.R;


/**
 * Created by prog on 17.05.17.
 */

public class SystemMessageHelper {

    private static Snackbar buildMessage(String message, View parent, Context context,
                                         final String actionText, final View.OnClickListener onClickListener) {
        Snackbar snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(context.getResources().getColor(R.color.white));
        if (actionText != null && !actionText.isEmpty() && onClickListener != null) {
            snackbar.setAction(actionText, onClickListener);
        }
        return snackbar;
    }

    private static Snackbar buildMessage(String message, View parent, Context context,
                                         String actionText, View.OnClickListener onClickListener,
                                         @ColorRes int backgroundColorRes) {
        Snackbar snackbar = buildMessage(message, parent, context, actionText, onClickListener);
        View snackbarLayout = snackbar.getView();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            snackbarLayout.setBackgroundColor(context.getColor(backgroundColorRes));
        } else {
            snackbarLayout.setBackgroundColor(context.getResources().getColor(backgroundColorRes));
        }

        return snackbar;
    }

    private static Snackbar buildSuccessMessage(String message, View parent, Context context,
                                                String actionText, View.OnClickListener onClickListener) {
        return buildMessage(message, parent, context,
                actionText, onClickListener,
                R.color.system_message_info_background);

    }

    private static Snackbar buildWarningMessage(String message, View parent, Context context,
                                                String actionText, View.OnClickListener onClickListener) {
        return buildMessage(message, parent, context,
                actionText, onClickListener,
                R.color.system_message_alert_background);

    }

    private static Snackbar buildErrorMessage(String message, View parent, Context context,
                                              String actionText, View.OnClickListener onClickListener) {
        return buildMessage(message, parent, context,
                actionText, onClickListener,
                R.color.system_message_error_background);
    }

    public static void showSuccessMessage(String message, View parent, Context context) {
        Snackbar snackbar = buildSuccessMessage(message, parent, context, null, null);
        snackbar.show();
    }

    public static void showWarningMessage(String message, View parent, Context context) {
        Snackbar snackbar = buildWarningMessage(message, parent, context, null, null);
        snackbar.show();
    }

    public static void showErrorMessage(String message, View parent, Context context) {
        Snackbar snackbar = buildErrorMessage(message, parent, context, null, null);
        snackbar.show();
    }

    public static void showMessage(String message, View parent, Context context) {
        Snackbar snackbar = buildMessage(message, parent, context, null, null);
        snackbar.show();
    }

    public static void showSuccessMessage(String message, View parent, Context context,
                                          String actionText, View.OnClickListener onClickListener) {
        Snackbar snackbar = buildSuccessMessage(message, parent, context, actionText, onClickListener);
        snackbar.show();
    }

    public static void showWarningMessage(String message, View parent, Context context,
                                          String actionText, View.OnClickListener onClickListener) {
        Snackbar snackbar = buildWarningMessage(message, parent, context, actionText, onClickListener);
        snackbar.show();
    }

    public static void showErrorMessage(String message, View parent, Context context,
                                        String actionText, View.OnClickListener onClickListener) {
        Snackbar snackbar = buildErrorMessage(message, parent, context, actionText, onClickListener);
        snackbar.show();
    }

    public static void showMessage(String message, View parent, Context context,
                                   String actionText, View.OnClickListener onClickListener) {
        Snackbar snackbar = buildMessage(message, parent, context, actionText, onClickListener);
        snackbar.show();
    }

}
