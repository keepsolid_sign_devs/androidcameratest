package com.testproject.sign.cameratest.camera.camera_api;

import android.graphics.Rect;

public interface CameraApi {

    void shoot();

    boolean toggleFlashlight();

    boolean isFlashOn();

    void onResume();

    void onPause();

    void refocus(Rect focusRect, Rect clickRect);

    boolean isShooting();

}
