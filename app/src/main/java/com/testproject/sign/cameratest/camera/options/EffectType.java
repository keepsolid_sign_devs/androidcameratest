package com.testproject.sign.cameratest.camera.options;

/**
 * Created by user on 11/3/17.
 */

public enum EffectType {
    NONE,
    CROP,
    ROTATION,
    COLOR,
    BRIGHTNESS,
    CONTRAST;

    /**
     * 0 if equals, negative if smaller, positive if bigger
     * @param effectType
     * @return
     */
    public int compareTypes(EffectType effectType) {
        if (effectType == this || (effectType == CROP && this == ROTATION)
                || (effectType == ROTATION && this == CROP)) {
            return 0;
        }
        return this.ordinal() - effectType.ordinal();
    }
}
