package com.testproject.sign.cameratest.camera.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.callbacks.CameraOperationListener;
import com.testproject.sign.cameratest.camera.options.EditedImage;
import com.testproject.sign.cameratest.camera.options.EditedImagesManager;
import com.testproject.sign.cameratest.camera.options.EffectsControl;
import com.testproject.sign.cameratest.camera.util.ImageSize;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.MemoryHelper;
import com.testproject.sign.cameratest.camera.util.RecyclingBitmapDrawable;
import com.testproject.sign.cameratest.util.UIThreadHelper;

import org.opencv.core.Point;

import java.util.ArrayList;

import static com.testproject.sign.cameratest.camera.ui.GalleryMainFragment.CAMERA_MODE_KEY;
import static com.testproject.sign.cameratest.camera.ui.GalleryMainFragment.PAGE_KEY;

/**
 * Created by user on 10/31/17.
 */

public class CropImageFragment extends AbstractFragment {

    public static final String TAG = CropImageFragment.class.getSimpleName();

    private CameraMode cameraMode = CameraMode.DOCUMENT;
    private int currentPage;
    private EditImageView docImage;
    private EditedImage editedImage;
    private Bitmap originalImage;
    private ArrayList<Point> origCropCorners;

    public static CropImageFragment newInstance(@NonNull CameraMode cameraMode, int page) {
        CropImageFragment fragment = new CropImageFragment();
        Bundle args = new Bundle();
        args.putSerializable(CAMERA_MODE_KEY, cameraMode);
        args.putInt(PAGE_KEY, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateView(LayoutInflater inflater) {
        setContentView(R.layout.crop_image_fragment);

        if (getArguments() != null) {
            Bundle args = getArguments();
            cameraMode = (CameraMode) args.getSerializable(CAMERA_MODE_KEY);
            currentPage = getArguments().getInt(PAGE_KEY);
        }

        prepareToolbar(getString(R.string.S_CROP_IMAGE), R.id.toolbar, R.drawable.back_icon);

        docImage = (EditImageView) findViewById(R.id.item_iv);
        TextView resetCornersBtn = (TextView) findViewById(R.id.reset_corners);
        resetCornersBtn.setText(getString(R.string.S_RESET_CROP_BORDERS));
        resetCornersBtn.setOnClickListener(resetCornersClickListener);

        String imageId = KSImageCache.getInstance().getKeyForPosition(currentPage);
        editedImage = EditedImagesManager.getInstance().get(imageId);
        origCropCorners = editedImage.getCorners();

        KSImageCache.getInstance().getImgAsync(imageId, ImageSize.MEDIUM, new CameraOperationListener<RecyclingBitmapDrawable>() {
            @Override
            public void onSuccess(final RecyclingBitmapDrawable result) {
                UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        setImage(result);
                    }
                });
            }
        });
        setHasOptionsMenu(true);
    }

    public void setImage(RecyclingBitmapDrawable image) {
        docImage.setShowCropCircles(true);

        originalImage = MemoryHelper.setNewBitmap(originalImage, image.getBitmap().copy(image.getBitmap().getConfig(), true));
        docImage.setImageBitmap(originalImage);

        setInitialCropCorners();
    }

    private void setInitialCropCorners() {
        if (cameraMode == CameraMode.INITIALS || cameraMode == CameraMode.SIGNATURE) {
            docImage.setEnableCropProportions(true);
        }
        if (origCropCorners.isEmpty()) {
            if (cameraMode == CameraMode.DOCUMENT) {
                findCropCorners();
            } else {
                resetCropBorders();
            }
        } else {
            setCropCorners(origCropCorners);
        }
    }

    @Override
    public void onLockActions() {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.filter_control_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_apply:
                cropImage(docImage.getCorners());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void cropImage(ArrayList<Point> corners) {
        editedImage.setCorners(corners);
//        Bitmap result = editedImage.transformImage(originalImage);
//        KSImageCache.getInstance().replaceImageOfMediumSize(editedImage.getImageId(), result);
        showGalleryFragment();
    }

    View.OnClickListener resetCornersClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            resetCropBorders();
        }
    };

    private void resetCropBorders() {
        docImage.post(new Runnable() {
            @Override
            public void run() {
                if (docImage != null) {
                    docImage.resetImageCorners();
                }
            }
        });
    }

    private void findCropCorners() {
        docImage.post(new Runnable() {
            @Override
            public void run() {
                if (docImage != null) {
                    docImage.findImageCorners();
                }
            }
        });
    }

    private void setCropCorners(final ArrayList<Point> corners) {
        docImage.post(new Runnable() {
            @Override
            public void run() {
                if (docImage != null) {
                    docImage.setImageCorners(corners);
                }
            }
        });
    }

    @Override
    public void goBack() {
        onCloseActions();
        showGalleryFragment();
    }

    private void onCloseActions() {
        recycleImages();
    }

    private void recycleImages() {
        if (originalImage != null && !originalImage.isRecycled()) {
            originalImage.recycle();
        }
    }

    private void showGalleryFragment() {
        CoreSignFragmentManager.getInstance().showCameraGalleryFragment(cameraMode, currentPage);
    }

}
