package com.testproject.sign.cameratest.camera.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog.BottomSheetBuilder;
import com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog.BottomSheetItemClickListener;
import com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog.BottomSheetMenuDialog;
import com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog.BottomSheetMenuItem;
import com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog.BottomSheetMode;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;
import com.testproject.sign.cameratest.camera.options.EditedImage;
import com.testproject.sign.cameratest.camera.options.EditedImagesManager;
import com.testproject.sign.cameratest.camera.options.ImageProcessor;
import com.testproject.sign.cameratest.camera.util.BitmapFromPath;
import com.testproject.sign.cameratest.camera.util.ImageLoadingObserver;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.listeners.ActivityListener;
import com.testproject.sign.cameratest.util.ProgressDialogController;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GalleryMainFragment extends AbstractFragment implements ImageLoadingObserver {

    private static final int GALLERY_ITEM_ID = 1;
    private static final int CAMERA_ITEM_ID = 2;

    private static final int COLOR_TUNE_ITEM_ID = 3;
    private static final int EXPOSITION_ITEM_ID = 4;
    private static final int CONTRAST_ITEM_ID = 5;

    public static final int GALLERY_REQUEST_CODE = 12420;

    public static final String CAMERA_MODE_KEY = "_camera_mode";
    public static final String PAGE_KEY = "page_to_select";
    private CameraMode cameraMode = CameraMode.DOCUMENT;
    private int currentPage;

    private GalleryPagerAdapter adapter;
    private GalleryViewPager galleryPager;
    private final ArrayList<String> images = new ArrayList<>();

    private volatile Queue<Uri> imagesQueue = new LinkedList<>();
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private BottomSheetMenuDialog addNewPageDialog;
    private BottomSheetMenuDialog filterOptionsDialog;

    private KSImageCache cacheManager;

    public static GalleryMainFragment newInstance(@NonNull CameraMode cameraMode, int page) {
        GalleryMainFragment fragment = new GalleryMainFragment();
        Bundle args = new Bundle();
        args.putSerializable(CAMERA_MODE_KEY, cameraMode);
        args.putInt(PAGE_KEY, page);
        fragment.setArguments(args);
        return fragment;
    }

    public GalleryMainFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cameraMode = (CameraMode) getArguments().getSerializable(CAMERA_MODE_KEY);
        currentPage = getArguments().getInt(PAGE_KEY);
        cacheManager = KSImageCache.getInstance();
    }

    @Override
    public void onCreateView(LayoutInflater inflater) {
        setContentView(R.layout.gallery_main_fragment);
        prepareToolbar("Gallery", R.id.gallery_toolbar, R.drawable.back_icon);
        updateToolbarTitle();
        setupViewPager();
        setupBottomMenu();
        buildDialogs();
        getCurrentActivity().addActivityListener(activityListener);
        cacheManager.registerObserver(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cacheManager.unregisterObserver(this);
    }

    @Override
    public void goBack() {
//        KSImageCache.getInstance().unregisterAll();
        KSImageCache.getInstance().clearCache();
        EditedImagesManager.getInstance().removeAll();
        onCloseActions();
        CoreSignFragmentManager.getInstance().showCameraNewPhotoFragment(cameraMode);
    }

    private void setupBottomMenu() {
        ViewGroup bottomOptionsMenu = (ViewGroup) findViewById(R.id.camera_bottom_menu);
        if (cameraMode == CameraMode.DOCUMENT) {
            bottomOptionsMenu.findViewById(R.id.action_retake_item).setVisibility(View.GONE);
            bottomOptionsMenu.findViewById(R.id.action_add_item).setOnClickListener(bottomItemClickListener);
            bottomOptionsMenu.findViewById(R.id.action_rotate_item).setOnClickListener(bottomItemClickListener);
            bottomOptionsMenu.findViewById(R.id.action_delete_item).setOnClickListener(bottomItemClickListener);
            bottomOptionsMenu.findViewById(R.id.action_filter_item).setOnClickListener(bottomItemClickListener);
            bottomOptionsMenu.findViewById(R.id.action_crop_item).setOnClickListener(bottomItemClickListener);
            bottomOptionsMenu.findViewById(R.id.action_move_item).setOnClickListener(bottomItemClickListener);
        } else {
            bottomOptionsMenu.findViewById(R.id.action_add_item).setVisibility(View.GONE);
            bottomOptionsMenu.findViewById(R.id.action_delete_item).setVisibility(View.GONE);
            bottomOptionsMenu.findViewById(R.id.action_move_item).setVisibility(View.GONE);
            bottomOptionsMenu.findViewById(R.id.action_rotate_item).setVisibility(View.GONE);
            bottomOptionsMenu.findViewById(R.id.action_retake_item).setOnClickListener(bottomItemClickListener);
            bottomOptionsMenu.findViewById(R.id.action_filter_item).setOnClickListener(bottomItemClickListener);
            bottomOptionsMenu.findViewById(R.id.action_crop_item).setOnClickListener(bottomItemClickListener);
        }
    }

    private View.OnClickListener bottomItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.action_add_item:
                    onAddNewPage();
                    break;
                case R.id.action_retake_item:
                    onRetakePhoto();
                    break;
                case R.id.action_filter_item:
                    onFilter();
                    break;
                case R.id.action_crop_item:
                    onCrop();
                    break;
                case R.id.action_move_item:
                    onMovePages();
                    break;
                case R.id.action_rotate_item:
                    onRotate();
                    break;
                case R.id.action_delete_item:
                    onDeleteCurrentPage();
                    break;
            }

        }
    };

    private void setupViewPager() {
        Set<String> cachedImages = cacheManager.getCachedImagesKeySet();
        images.addAll(cachedImages);
        adapter = new GalleryPagerAdapter(getFragmentManager(), images);
        galleryPager = (GalleryViewPager) findViewById(R.id.gallery_view_pager);
        galleryPager.setClipToPadding(false);
        galleryPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.gallery_page_margin_end));
        galleryPager.setAdapter(adapter);
        galleryPager.setCurrentItem(currentPage);
        galleryPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                updateToolbarTitle();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        handleSwipeEnabled();
    }

    private void refreshUi() {
        updateToolbarTitle();
        if (galleryPager != null) {
            handleSwipeEnabled();
        }
        adapter.notifyDataSetChanged();
    }

    private void updateToolbarTitle() {
        getToolbar().setTitle(String.format("%d of %d", currentPage + 1, cacheManager.getSavedImagesCount()));
    }

    private void handleSwipeEnabled() {
        galleryPager.setSwipeEnabled(images.size() > 1);
    }

    private void buildDialogs() {
        addNewPageDialog = new BottomSheetBuilder(getContext()).
                setMode(BottomSheetMode.LIST).
                addTitleItem("Choose filter").
                addItem(GALLERY_ITEM_ID, "Gallery", R.drawable.icon_add_from_gallery).
                addItem(CAMERA_ITEM_ID, "Camera", R.drawable.icon_add_from_camera).
                setItemClickListener(addNewPageDialogListener).
                createDialog();

        BottomSheetBuilder builder = new BottomSheetBuilder(getContext()).
                setMode(BottomSheetMode.LIST).
                addTitleItem("Choose filter").
                addItem(EXPOSITION_ITEM_ID, "Brightness", R.drawable.icon_filter_exposition).
                addItem(CONTRAST_ITEM_ID, "Contrast", R.drawable.icon_filter_contrast).
                setItemClickListener(filterOptionsDialogListener);
        if (cameraMode == CameraMode.DOCUMENT) {
            builder.addItem(COLOR_TUNE_ITEM_ID, "Color tune", R.drawable.icon_filter_color_tune);
        }
        filterOptionsDialog = builder.createDialog();
    }

    private BottomSheetItemClickListener addNewPageDialogListener = new BottomSheetItemClickListener() {
        @Override
        public void onItemClick(BottomSheetMenuItem item) {
            addNewPageDialog.dismiss();
            switch (item.getId()) {
                case GALLERY_ITEM_ID:
                    addPagesFromGallery();
                    break;
                case CAMERA_ITEM_ID:
                    addPagesFromCamera();
                    break;
            }
        }
    };

    private BottomSheetItemClickListener filterOptionsDialogListener = new BottomSheetItemClickListener() {
        @Override
        public void onItemClick(BottomSheetMenuItem item) {
            filterOptionsDialog.dismiss();
            OnTuneEventListener.TuneMode mode = OnTuneEventListener.TuneMode.NONE;
            switch (item.getId()) {
                case COLOR_TUNE_ITEM_ID:
                    mode = OnTuneEventListener.TuneMode.COLOR;
                    break;
                case EXPOSITION_ITEM_ID:
                    mode = OnTuneEventListener.TuneMode.BRIGHTNESS;
                    break;
                case CONTRAST_ITEM_ID:
                    mode = OnTuneEventListener.TuneMode.CONTRAST;
                    break;
            }
            onCloseActions();
            CoreSignFragmentManager.getInstance().showFilterControlFragment(cameraMode, mode, galleryPager.getCurrentItem());
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.gallery_toolbar_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLockActions() {
        if (addNewPageDialog != null && addNewPageDialog.isShowing()) {
            addNewPageDialog.dismiss();
        }
        if (filterOptionsDialog != null && filterOptionsDialog.isShowing()) {
            filterOptionsDialog.dismiss();
        }
    }

    private ImageFragment getCurrentFragment() {
        return (ImageFragment) adapter.getRegisteredFragment(galleryPager.getCurrentItem());
    }

    private void save() {
        Toast.makeText(getContext(), "Save", Toast.LENGTH_SHORT).show();
    }

    private void onRetakePhoto() {
        String imgId = getCurrentFragment().getImageId();
        EditedImagesManager.getInstance().remove(imgId);
        cacheManager.removeImage(imgId);
        goBack();
    }

    private void onAddNewPage() {
        if (addNewPageDialog != null)
            addNewPageDialog.show();
    }

    private void addPagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getCurrentActivity().startActivityForResult(Intent.createChooser(intent, "Select Images"), GALLERY_REQUEST_CODE);
    }

    private ActivityListener activityListener = new ActivityListener() {
        @Override
        public void onActivityResult(int requestCode, int resultCode, final Intent data) {
            if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_REQUEST_CODE && data != null) {
                ProgressDialogController.getInstance().showTransparentProgressBar(false);
                if (data.getClipData() != null) {
                    for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        imagesQueue.add(imageUri);
                    }
                } else {
                    imagesQueue.add(data.getData());
                }
                startNewImageLoading();
            }
        }
    };

    private void startNewImageLoading() {
        if (imagesQueue.size() > 0) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    addBitmapFromUri(imagesQueue.poll());
                }
            });
        }
    }

    @Override
    public void notifyLoadStarted(String imageTag) {

    }

    @Override
    public void notifyLoadFinished(String imageTag) {
        if (imagesQueue.isEmpty()) {
            ProgressDialogController.getInstance().hideProgressDialog();
            refreshUi();
        } else {
            if (images.contains(imageTag)) {
                startNewImageLoading();
            }
        }
    }

    private void addBitmapFromUri(Uri imageUri) {
        final Bitmap selectedImage = BitmapFromPath.getFromUri(getContext(), imageUri);
        if (selectedImage != null) {
            String originBitmapName = cacheManager.getNextName();
//            cacheManager.setSerializationQuality(60);
            cacheManager.setAutoSerializationQuality(selectedImage.getWidth(), selectedImage.getHeight());
            cacheManager.setImage(selectedImage, originBitmapName);
            addImageToManager(originBitmapName, selectedImage.getWidth(), selectedImage.getHeight());
            images.add(originBitmapName);
        }
    }

    private void addImageToManager(String imageId, int imageWidth, int imageHeight) {
        EditedImage image = new EditedImage(imageId);
        if (cameraMode == CameraMode.INITIALS || cameraMode == CameraMode.SIGNATURE) {
            image.setColor(100);
            ImageProcessor.addDefaultCornersWithProportions(image.getCorners(), imageWidth,
                    imageHeight, EditImageView.CROP_ASPECT_RATIO);
        }
        EditedImagesManager.getInstance().add(image);
    }

    private void addPagesFromCamera() {
        onCloseActions();
        CoreSignFragmentManager.getInstance().showCameraNewPhotoFragment(cameraMode);
    }

    private void onFilter() {
        if (filterOptionsDialog != null)
            filterOptionsDialog.show();
    }

    private void onCrop() {
        onCloseActions();
        CoreSignFragmentManager.getInstance().showCropImageFragment(cameraMode, currentPage);
    }

    private void onRotate() {
        getCurrentFragment().rotateImage(-90);
    }

    private void onDeleteCurrentPage() {
        String imgKey = cacheManager.removeImageByPosition(currentPage);
        EditedImagesManager.getInstance().remove(imgKey);
        if (images.size() == 1) {
            goBack();
        } else {
            images.remove(currentPage);
            refreshUi();
        }
    }

    private void onCloseActions() {
        for (int i = 0; i < adapter.getCount(); i++) {
            ImageFragment currentFragment = (ImageFragment) adapter.getRegisteredFragment(i);
            if (currentFragment != null) {
                currentFragment.onCloseActions();
            }
        }
    }

    private void onMovePages() {
        onCloseActions();
        CoreSignFragmentManager.getInstance().showMovePagesFragments();
    }
}
