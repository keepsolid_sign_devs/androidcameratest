package com.testproject.sign.cameratest.camera.util;

public interface ImageLoadingObserver {

    void notifyLoadStarted(String imageTag);

    void notifyLoadFinished(String imageTag);
}