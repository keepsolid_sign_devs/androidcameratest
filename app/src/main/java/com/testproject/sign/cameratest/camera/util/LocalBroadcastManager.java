package com.testproject.sign.cameratest.camera.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class LocalBroadcastManager {

    private static LocalBroadcastManager instance;
    private Context context;

    private LocalBroadcastManager(Context context) {
        this.context = context;
    }

    public static LocalBroadcastManager getInstance(Context context) {
        if (instance == null) {
            return new LocalBroadcastManager(context);
        }
        return instance;
    }

    public void registerReceiver(BroadcastReceiver mMessageReceiver, IntentFilter intentFilter) {
    	android.support.v4.content.LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver, intentFilter);

    }

    public void unregisterReceiver(BroadcastReceiver mMessageReceiver) {
    	android.support.v4.content.LocalBroadcastManager.getInstance(context).unregisterReceiver(mMessageReceiver);
    }

    public void sendBroadcast(Intent intent) {
    	android.support.v4.content.LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public void sendBroadcastSync(Intent schemeIntent) {
    	android.support.v4.content.LocalBroadcastManager.getInstance(context).sendBroadcastSync(schemeIntent);
    }
}
