package com.testproject.sign.cameratest.camera.util;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

/**
 * Created by user on 10/30/17.
 */

public class BitmapFromPathOpenCV {

    public static Bitmap getFromPath(String path) {
        Mat mat = Imgcodecs.imread(path, Imgcodecs.CV_LOAD_IMAGE_COLOR);
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;

        Bitmap result = Bitmap.createBitmap(mat.width(), mat.height(), conf);
        Utils.matToBitmap(mat, result);

        mat.release();

        return result;
    }

}
