package com.testproject.sign.cameratest.camera.util;


import android.content.Context;
import android.content.Intent;


public class SXBroadcastSender {
    public Context context;

    private static SXBroadcastSender mInstance = null;


    private SXBroadcastSender() {
    }

    public void init(Context context) {
        this.context = context.getApplicationContext();
    }

    public static synchronized SXBroadcastSender getInstance() {
        if (mInstance == null) {
            mInstance = new SXBroadcastSender();
        }
        return mInstance;
    }

    public void sendBroadcast(Intent intent) {
    	LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
        context.sendBroadcast(intent);
    }


}
