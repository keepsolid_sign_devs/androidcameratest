package com.testproject.sign.cameratest.base_ui;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.util.SystemMessageHelper;
import com.testproject.sign.cameratest.util.UIThreadHelper;

public abstract class AbstractFragment extends Fragment {

    private static final String LOG_TAG = AbstractFragment.class.getSimpleName();
    private View contentView;
    private LayoutInflater layoutInflater;
    private Toolbar toolbar;
    private boolean isUIBlockAllowed = true;
    private boolean staticContent = false;
    private boolean inNeedToLockDrawer = true;

    // <editor-fold desc="fragment states">

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.layoutInflater = inflater;
        onCreateView(inflater);
        setHasOptionsMenu(true);
        return contentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible()) {
            onShowFragmentActions();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            onShowFragmentActions();
        }
    }

    private void onShowFragmentActions() {
        onFragmentOnScreen();
    }

    public void finish() {

    }

    public void setUIBlockAllowed(boolean isAllowed) {
        isUIBlockAllowed = isAllowed;
    }


    public abstract void onCreateView(LayoutInflater inflater);

    public abstract void onLockActions();

    public boolean isNeedToLockDrawer() {
        return inNeedToLockDrawer;
    }

    public void setInNeedToLockDrawer(boolean inNeedToLockDrawer) {
        this.inNeedToLockDrawer = inNeedToLockDrawer;
    }

    protected void onFragmentOnScreen() {

    }

    /**
     * Method for initializing toolbar of current fragment
     *
     * @param title    Toolbar's text
     * @param resource Toolbar's view id
     */
    public void prepareToolbar(String title, @IdRes int resource) {
        toolbar = (Toolbar) findViewById(resource);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(null);
        toolbar.setNavigationOnClickListener(null);
    }

    /**
     * Method for initializing toolbar of current fragment
     *
     * @param title       Toolbar's text
     * @param resource    Toolbar's view id
     * @param backIconRes determines the back arrow resource
     */
    public void prepareToolbar(String title, @IdRes int resource, int backIconRes) {
        toolbar = (Toolbar) findViewById(resource);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(backIconRes);
        toolbar.setNavigationOnClickListener(onBackPressClick);
    }

    public void prepareToolbar(String title, @IdRes int resource, @DrawableRes int backIconRes,
                               @ColorRes int textColorRes) {
        prepareToolbar(title, resource, backIconRes);
        toolbar.setTitleTextColor(getResources().getColor(textColorRes));
    }

    public void prepareToolbar(String title, @IdRes int resource, @DrawableRes int backIconRes,
                               @ColorRes int textColorRes, @ColorRes int backgroundColorRes) {
        prepareToolbar(title, resource, backIconRes, textColorRes);
        toolbar.setBackgroundColor(getResources().getColor(backgroundColorRes));
    }

    /**
     * Method for initializing toolbar of current fragment
     *
     * @param title       Toolbar's text
     * @param resource    Toolbar's view id
     * @param backIconRes determines the back arrow resource
     * @param listener    listener for Navigation Icon
     */
    public void prepareToolbar(String title, @IdRes int resource, int backIconRes, View.OnClickListener listener) {
        toolbar = (Toolbar) findViewById(resource);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(backIconRes);
        toolbar.setNavigationOnClickListener(listener);
    }

    /**
     * Method for updating current toolbar
     *
     * @param title       Toolbar's new title
     * @param backIconRes determines the back icon drawable resource
     */
    public void updateToolbar(String title, int backIconRes) {
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(backIconRes);
        toolbar.setNavigationOnClickListener(onBackPressClick);
    }

    /**
     * Method for updating current toolbar
     *
     * @param title       String resource for title
     * @param backIconRes determines the back icon drawable resource
     */
    public void updateToolbar(@StringRes int title, int backIconRes) {
        updateToolbar(getString(title));
    }

    /**
     * Method for updating current toolbar
     *
     * @param title new title
     */
    public void updateToolbar(String title) {
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }

    /**
     * Method for updating current toolbar
     *
     * @param title new title
     */
    public void updateToolbar(@StringRes int title) {
        updateToolbar(getString(title));
    }

    public void hideToolbar() {
        if (toolbar != null) {
            toolbar.setVisibility(View.GONE);
        }
    }


    public void showToolbar() {
        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * BackPress click listener
     */
    public View.OnClickListener onBackPressClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getActivity() != null && !getActivity().isFinishing()) {
                goBack();
            }
        }
    };

    public void goBack() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            goBack();
        }
        return super.onOptionsItemSelected(item);
    }

    public AppFragmentManager.ANIMATION_TYPE getAnimationType() {
        return AppFragmentManager.ANIMATION_TYPE.FADE;
    }

    // </editor-fold>

    // <editor-fold desc="utils">

    public void setContentView(View view) {
        contentView = view;
    }

    public View findViewById(int id) {
        return contentView.findViewById(id);
    }

    public void post(Runnable runnable) {
        contentView.post(runnable);
    }

    public void postDelayed(Runnable runnable, int delay) {
        contentView.postDelayed(runnable, delay);
    }

    public void runOnUiThread(Runnable runnable) {
        UIThreadHelper.runOnUIThread(getCurrentActivity(), runnable);
    }

    public void runOnUiThread(Runnable runnable, AbstractActivity activity) {
        UIThreadHelper.runOnUIThread(activity, runnable);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return false;
    }

    public void onConfigurationChanged() {
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    // </editor-fold>

    // <editor-fold desc="getters & setters">

    public View getContentView() {
        return contentView;
    }

    public void setContentView(int id) {
        contentView = getLayoutInflater().inflate(id, null);
    }

    public AbstractActivity getCurrentActivity() {
        AbstractActivity activity = CoreSignFragmentManager.getInstance().getActivity();
        if (activity == null) {
            activity = (AbstractActivity) getActivity();
        }
        return activity;
    }

    public void setStatusBarColor(@ColorRes int statusBarColorRes) {
        if (!isAdded()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getCurrentActivity().getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(statusBarColorRes));
        }
    }

    public int getStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getCurrentActivity().getWindow();
            return window.getStatusBarColor();
        } else {
            return 0;
        }
    }

    public boolean isStaticContent() {
        return staticContent;
    }

    public void setStaticContent(boolean staticContent) {
        this.staticContent = staticContent;
    }

    public void showMessage(final String message) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showMessage(message, getView(), getContext());
                }
            });
        }
    }

    public void showSuccessMessage(final String message) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showSuccessMessage(message, getView(), getContext());
                }
            });
        }
    }

    public void showWarningMessage(final String message) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showWarningMessage(message, getView(), getContext());
                }
            });
        }
    }

    public void showErrorMessage(final String message) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showErrorMessage(message, getView(), getContext());
                }
            });
        }
    }

    public void showMessage(final String message, final String actionText, final View.OnClickListener onClickListener) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showMessage(message, getView(), getContext(), actionText, onClickListener);
                }
            });
        }
    }

    public void showSuccessMessage(final String message, final String actionText, final View.OnClickListener onClickListener) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showSuccessMessage(message, getView(), getContext(), actionText, onClickListener);
                }
            });
        }
    }

    public void showWarningMessage(final String message, final String actionText, final View.OnClickListener onClickListener) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showWarningMessage(message, getView(), getContext(), actionText, onClickListener);
                }
            });
        }
    }

    public void showErrorMessage(final String message, final String actionText, final View.OnClickListener onClickListener) {
        if (getView() != null) {
            UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                @Override
                public void run() {
                    SystemMessageHelper.showErrorMessage(message, getView(), getContext(), actionText, onClickListener);
                }
            });
        }
    }

// </editor-fold>

}