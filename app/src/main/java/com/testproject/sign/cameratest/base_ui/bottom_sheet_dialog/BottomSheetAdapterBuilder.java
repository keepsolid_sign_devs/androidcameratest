package com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.testproject.sign.cameratest.R;

import java.util.ArrayList;
import java.util.List;

class BottomSheetAdapterBuilder {

    private List<BottomSheetItem> mItems;
    private BottomSheetMode mMode = BottomSheetMode.LIST;
    private Context mContext;

    BottomSheetAdapterBuilder(Context context) {
        mContext = context;
        mItems = new ArrayList<>();
    }

    public void setMode(BottomSheetMode mMode) {
        this.mMode = mMode;
    }

    void addTitleItem(String title) {
        mItems.add(new BottomSheetHeader(title));
    }

    void addItem(int id, String title, Drawable icon) {
        mItems.add(new BottomSheetMenuItem(id, title, icon));
    }

    @SuppressLint("InflateParams")
    View createView(BottomSheetItemClickListener itemClickListener) {
//
//        if (mFromMenu) {
//            mItems = createAdapterItems();
//        }

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View sheet = layoutInflater.inflate(R.layout.base_bottomsheet, null);

        final RecyclerView recyclerView = (RecyclerView) sheet.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        final BottomSheetAdapter adapter = new BottomSheetAdapter(mItems, mMode,
                itemClickListener);

        if (mMode == BottomSheetMode.LIST) {
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        } else {
            final int columns = mContext.getResources().getInteger(R.integer.bottomsheet_grid_columns);
            GridLayoutManager layoutManager = new GridLayoutManager(mContext, columns);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);
        return sheet;
    }

    public List<BottomSheetItem> getItems() {
        return mItems;
    }

}
