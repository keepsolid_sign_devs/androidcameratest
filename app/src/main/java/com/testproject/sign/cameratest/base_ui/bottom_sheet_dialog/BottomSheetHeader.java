package com.testproject.sign.cameratest.base_ui.bottom_sheet_dialog;

class BottomSheetHeader implements BottomSheetItem {

    private String mTitle;

    BottomSheetHeader(String mTitle) {
        this.mTitle = mTitle;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }
}
