package com.testproject.sign.cameratest.camera.util;

import android.content.Context;
import android.database.Observable;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.util.Pair;

import com.testproject.sign.cameratest.camera.callbacks.CameraOperationListener;
import com.testproject.sign.cameratest.camera.options.EditedImagesManager;

import java.io.ByteArrayOutputStream;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.Stack;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executors;

public class KSImageCache {

    private static final String LOG_TAG = KSImageCache.class.getSimpleName();

    public static final int UNKNOWN_POSITION = -1;

    private static final String CACHE_NAME = "docs_photo_cache";
    public static final String DOC_IMG_NAME_PREFIX = "doc_img_";

    private static final String MAX_IMG_SUFFIX = "_max";
    private static final String MEDIUM_IMG_SUFFIX = "_medium";
    private static final String MINI_IMG_SUFFIX = "_mini";

    private static final double PREFERRED_IMG_HEIGHT = 1800;
    private static final double PREFERRED_IMG_WIDTH = 1000;

    private static final int MAX_DISK_MEMORY_SIZE = 1024 * 1024 * 100; //100mb

    private int serializationQuality = 60;

    private KSImageAsyncClient asyncClient;

    private Context context;
    private DiskBitmapCache diskCache;
    private LruCache<String, RecyclingBitmapDrawable> runtimeCache;

    private volatile Set<String> progressSet;
    private volatile SortedMap<Integer, String> cachedImages;
    private volatile Stack<Pair<String, Integer>> bucket;

    private volatile int imagesCounter = 1;

    private static volatile KSImageCache instance;

    private ImageCacheObservable observable;

    public static KSImageCache getInstance() {
        if (instance == null) {
            synchronized (KSImageCache.class) {
                instance = new KSImageCache();
            }
        }
        return instance;
    }

    // <editor-fold desc="init">

    public void init(Context context) {
        this.context = context;
        asyncClient = new KSImageAsyncClient(Executors.newCachedThreadPool());
        observable = new ImageCacheObservable();

        progressSet = new ConcurrentSkipListSet<>();
        cachedImages = new TreeMap<>();
        bucket = new Stack<>();

        //init disk cache
        diskCache = DiskBitmapCache.getInstance();
        diskCache.init(MAX_DISK_MEMORY_SIZE, context.getFilesDir().getAbsolutePath());

        //init LRU runtime cache
        int cacheSize = MemoryHelper.calculateMemoryCacheSize(context);
        runtimeCache = new KSLruCache(cacheSize);
    }

    public void refresh() {
        asyncClient = new KSImageAsyncClient(Executors.newCachedThreadPool());
    }

    // </editor-fold>

    // <editor-fold desc="get image">

    public String getNextName() {
        return DOC_IMG_NAME_PREFIX.concat(String.valueOf(imagesCounter));
    }

    /**
     * Get image from cache synchronously.
     *
     * @param key  image tag
     * @param size you can get image of MAX size (from disk) -> big size, but heavy operation,
     *             or you can get image of MEDIUM size(runtime cache) -> small, but you'll get it quickly
     * @return image
     */
    public RecyclingBitmapDrawable getImage(String key, ImageSize size) {
        switch (size) {
            case MAX:
                return getImageOfMaxSize(key);
            case MEDIUM:
                return getImageOfMediumSize(key);
            case MIN:
                return getImageOfMinSize(key);
        }
        return null;
    }

    public int getPositionForKey(String key) {
        for (Map.Entry<Integer, String> entry : cachedImages.entrySet()) {
            if (entry.getValue().equals(key)) {
                return entry.getKey();
            }
        }
        return UNKNOWN_POSITION;
    }

    public String getKeyForPosition(int position) {
        return cachedImages.get(position);
    }

    /**
     * Get image from cache asynchronously. Recommended to use for getting images of MAX size.
     *
     * @param key      image tag
     * @param size     you can get image of MAX size (from disk) -> big size, but heavy operation,
     *                 or you can get image of MEDIUM size(runtime cache) -> small, but you'll get it quickly
     * @param listener result listener
     */
    public void getImgAsync(final String key, final ImageSize size, CameraOperationListener<RecyclingBitmapDrawable> listener) {
        asyncClient.submitTask(new Callable<RecyclingBitmapDrawable>() {
            @Override
            public RecyclingBitmapDrawable call() throws Exception {
                return getImage(key, size);
            }
        }, listener);
    }

    public String getImageFilePath(String key, ImageSize size) {
        String postfix = "";
        switch (size) {
            case MAX:
                postfix = MAX_IMG_SUFFIX;
                break;
            case MEDIUM:
                postfix = MEDIUM_IMG_SUFFIX;
                break;
            case MIN:
                postfix = MINI_IMG_SUFFIX;
                break;
        }
        String path = diskCache.getImgPath(key + postfix);
        return path;
    }

    @Nullable
    private RecyclingBitmapDrawable getImageOfMaxSize(String key) {
        //to avoid unnecessary heavy operation
        ImageState state = getImageState(key);
        if (state == ImageState.IS_SAVING || state == ImageState.ABSENT) {
            Log.e(LOG_TAG, "Sorry image is saving " + key);
            return null;
        }

        String imageTag = key.concat(MAX_IMG_SUFFIX);
        Bitmap bitmap = diskCache.get(imageTag);
        if (bitmap != null) {
            return new RecyclingBitmapDrawable(context.getResources(), bitmap, key);
        } else {
            return null;
        }
    }

    @Nullable
    private RecyclingBitmapDrawable getImageOfMediumSize(String key) {
        //to avoid unnecessary heavy operation
        if (getImageState(key) == ImageState.ABSENT) return null;

        String imageTag = key.concat(MEDIUM_IMG_SUFFIX);
        RecyclingBitmapDrawable recyclingBitmapDrawable = runtimeCache.get(imageTag);
        if (recyclingBitmapDrawable != null && recyclingBitmapDrawable.hasValidBitmap()) {
            recyclingBitmapDrawable.setTag(key); //just in case, we do not want to lose it's tag
            return recyclingBitmapDrawable;
        } else { //original image's copy was removed from runtime cache, create it again
            RecyclingBitmapDrawable image = getImageOfMaxSize(key);
            if (image != null && image.hasValidBitmap()) {
                RecyclingBitmapDrawable mediumCopy = createMediumCopy(image.getBitmap(), key);
                setImageOfMediumSize(mediumCopy);
                return mediumCopy;
            }
        }
        return null;
    }

    @Nullable
    private RecyclingBitmapDrawable getImageOfMinSize(String key) {
        if (getImageState(key) == ImageState.ABSENT)
            return null; //error! this image has never been cached

        String imageTag = key.concat(MINI_IMG_SUFFIX);
        RecyclingBitmapDrawable recyclingBitmapDrawable = runtimeCache.get(imageTag);
        if (recyclingBitmapDrawable != null && recyclingBitmapDrawable.hasValidBitmap()) {
            recyclingBitmapDrawable.setTag(key); //just in case, we do not want to lose it's tag
            return recyclingBitmapDrawable;
        } else { //original image's copy was removed from runtime cache, create it again
            RecyclingBitmapDrawable image = getImageOfMaxSize(key);
            if (image != null && image.hasValidBitmap()) {
                RecyclingBitmapDrawable miniCopy = createMiniCopy(image.getBitmap(), key);
                setImageOfMiniSize(miniCopy);
                return miniCopy;
            }
        }
        return null;
    }

    // </editor-fold>

    // <editor-fold desc="set image">

    /**
     * Get parameter from 0 to 100 that displays quality of serialized image that we save on disk
     *
     * @return
     */
    public int getSerializationQuality() {
        return serializationQuality;
    }

    /**
     * Set parameter from 0 to 100 that displays quality of serialized image that we save on disk
     *
     * @return
     */
    public void setSerializationQuality(int serializationQuality) {
        this.serializationQuality = serializationQuality;
    }

    public void setAutoSerializationQuality(int bitmapWidth, int bitmapHeight) {
        //TODO test this formula
        //calculate diagonals difference
        double diff = (Math.sqrt(Math.pow(bitmapHeight, 2) + Math.pow(bitmapWidth, 2)) /
                Math.sqrt(Math.pow(PREFERRED_IMG_HEIGHT, 2) + Math.pow(PREFERRED_IMG_WIDTH, 2))) - 1;
        if (diff < 0) {
            diff = 0; //if our image is smaller, than preferred - do not serialize at all
        } else {
            diff = (double) Math.round(diff * 10) * 0.03; //3% of quality reduction for every 10% of oversizing
        }
        int calculatedQuality = (int) (100 * (1 - Math.min(diff, 0.5)));
        Log.v(LOG_TAG, "new counted serialization quality is " + calculatedQuality);
        this.serializationQuality = calculatedQuality;
    }

    /**
     * Save image to cache
     *
     * @param bitmap image bitmap
     * @param key    image tag
     * @return image tag
     */
    public String setImage(final Bitmap bitmap, final String key) {
        return setImageAtPosition(bitmap, key, cachedImages.size());
    }

    public String setImageAtPosition(final Bitmap bitmap, final String key, int position) {
//        if (cachedImages.containsKey(position) && !cachedImages.get(position).equals(key)) {
//            throw new IllegalArgumentException("This image is already cached at another position ");
//        }
        if (cachedImages.containsValue(key)) {
            position = getPositionForKey(key);
            removeImageByPosition(position);
            EditedImagesManager.getInstance().remove(key);
        }
        cachedImages.put(position, key);
        increment();
        setImageOfMediumSize(createMediumCopy(bitmap, key));
        setImageOfMiniSize(createMiniCopy(bitmap, key));
        setImageOfMaxSize(bitmap, key);
        return key;
    }

    public void swapImagesOrder(int firstPosition, int secondPosition) {
        if (!cachedImages.containsKey(firstPosition)) {
            return;
        }
        if (!cachedImages.containsKey(secondPosition)) {
            return;
        }

        String tempKey = cachedImages.get(firstPosition);
        cachedImages.put(firstPosition, cachedImages.get(secondPosition));
        cachedImages.put(secondPosition, tempKey);
    }

    private void setImageOfMaxSize(final Bitmap bitmap, final String key) {
        observable.notifyLoadStarted(key);
        asyncClient.submitTask(
                new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        diskCache.put(key.concat(MAX_IMG_SUFFIX), bitmap, serializationQuality);
                        bitmap.recycle();
                        return key;
                    }
                },
                new CameraOperationListener<String>() {
                    @Override
                    public void onSuccess(String result) {
                        if (!cachedImages.containsValue(key) && !getImagesInBucket().contains(key)) {
                            removeImageOfMaxSize(key);
                        } else {
                            observable.notifyLoadFinished(result);
                        }
                    }
                });
    }

    private void setImageOfMediumSize(final RecyclingBitmapDrawable bitmapDrawable) {
        final String key = bitmapDrawable.getTag();
        runtimeCache.put(key.concat(MEDIUM_IMG_SUFFIX), bitmapDrawable);
        bitmapDrawable.setIsCached(true);
    }

    private void setImageOfMiniSize(RecyclingBitmapDrawable bitmapDrawable) {
        String key = bitmapDrawable.getTag();
        runtimeCache.put(key.concat(MINI_IMG_SUFFIX), bitmapDrawable);
        bitmapDrawable.setIsCached(true);
    }

    private RecyclingBitmapDrawable createMediumCopy(Bitmap bitmap, String key) {
        return createCopy(bitmap, key, 0.6f);
    }

    private RecyclingBitmapDrawable createMiniCopy(Bitmap bitmap, String key) {
        return createCopy(bitmap, key, 0.15f);
    }

    private RecyclingBitmapDrawable createCopy(Bitmap bitmap, String key, float compressionCoeff) {
        float coefficient = compressionCoeff * serializationQuality / 100;
        int copyWidth = (int) (bitmap.getWidth() * coefficient);
        int copyHeight = (int) (bitmap.getHeight() * coefficient);

        Log.e(LOG_TAG, "createCopy: " + copyWidth + "x" + copyHeight);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, copyWidth, copyHeight, false);
        RecyclingBitmapDrawable bitmapDrawable = new RecyclingBitmapDrawable(context.getResources(), resizedBitmap);
        bitmapDrawable.setTag(key);

        return bitmapDrawable;
    }

    // </editor-fold>

    // <editor-fold desc="callbacks">

    public void registerObserver(ImageLoadingObserver observer) {
        observable.registerObserver(observer);
    }

    public void unregisterObserver(ImageLoadingObserver observer) {
        observable.unregisterObserver(observer);
    }

    public void unregisterAll() {
        observable.unregisterAll();
    }

    // </editor-fold>

    // <editor-fold desc="other methods">

    public Set<String> getCachedImagesKeySet() {
        return new LinkedHashSet<>(cachedImages.values());
    }

    public int getSavedImagesCount() {
        return cachedImages.size();
    }

    public Set<String> getProgressImagesKeySet() {
        return progressSet;
    }

    public int getProgressImagesCount() {
        return progressSet.size();
    }

    public void removeImage(String key) {
        boolean exists = false;
        for (Map.Entry<Integer, String> entry : cachedImages.entrySet()) {
            if (entry.getValue().equals(key)) {
                exists = true;
                cachedImages.remove(entry.getKey());
                break;
            }
        }
        if (exists) {
            progressSet.remove(key);
            decrement();
            removeImageOfMaxSize(key);
            removeImageOfMediumSize(key);
            removeImageOfMiniSize(key);
        }
    }

    public String removeImageByPosition(int position) {
        String key = cachedImages.get(position);
        if (key != null) {
            cachedImages.remove(position);
            progressSet.remove(key);
            decrement();
            removeImageOfMaxSize(key);
            removeImageOfMediumSize(key);
            removeImageOfMiniSize(key);
            return key;
        }
        return null;
    }

    public void moveImageToBucket(int position) {
        Log.v(LOG_TAG, "moveImageToBucket " + position);
        String key = cachedImages.get(position);
        if (key != null) {
            cachedImages.remove(position);
            progressSet.remove(key);
            decrement();
            removeImageOfMediumSize(key);
        }
        bucket.push(new Pair<>(key, position));
    }

    public void moveImageToBucket(String key) {
        boolean exists = false;
        int position = 0;
        for (Map.Entry<Integer, String> entry : cachedImages.entrySet()) {
            if (entry.getValue().equals(key)) {
                exists = true;
                position = entry.getKey();
                cachedImages.remove(entry.getKey());
                progressSet.remove(key);
                break;
            }
        }
        if (exists) {
            decrement();
            removeImageOfMediumSize(key);
        }
        bucket.push(new Pair<>(key, position));
    }

    public Pair<String, Integer> restoreImageFromBucket(boolean restorePosition) {
        if (bucket.empty()) {
            return null;
        }
        Pair<String, Integer> restoredImage = bucket.pop();
        int position = restorePosition ? Math.min(restoredImage.second, cachedImages.size()) : cachedImages.size();
        cachedImages.put(position, restoredImage.first);
        return restoredImage;
    }

    private void dumpCache() {
        Log.v(LOG_TAG, "--- Cache --- \n" + cachedImages.toString() +
                "\n +++ Bucket +++ \n" + getImagesInBucket().toString());
    }

    public void deleteLastImageInBucket() {
        if (bucket.empty())
            return;

        Pair<String, Integer> imageFromBucket = bucket.pop();
        String key = imageFromBucket.first;
        if (progressSet.contains(key)) {
            progressSet.remove(key);
        }
        removeImageOfMaxSize(key);
        removeImageOfMiniSize(key);
    }

    public void clearBucket() {
        while (!bucket.empty()) {
            deleteLastImageInBucket();
        }
    }

    public Set<String> getImagesInBucket() {
        Set<String> result = new HashSet<>();
        for (Pair<String, Integer> entry : bucket) {
            result.add(entry.first);
        }
        return result;
    }

    public int getBucketSize() {
        return bucket.size();
    }

    private void removeImageOfMiniSize(String key) {
        runtimeCache.remove(key.concat(MINI_IMG_SUFFIX));
    }

    private void removeImageOfMediumSize(String key) {
        runtimeCache.remove(key.concat(MEDIUM_IMG_SUFFIX));
    }

    private void removeImageOfMaxSize(String key) {
        diskCache.delete(key.concat(MAX_IMG_SUFFIX));
    }

    public void clearCache() {
        asyncClient.cancellAll();
        diskCache.dropCache();
        runtimeCache.evictAll();
        cachedImages.clear();
        progressSet.clear();
    }

    public ImageState getImageState(String key) {
        boolean contains = false;
        if (cachedImages.containsValue(key)) {
            contains = true;
        }

        if (contains) {
            if (progressSet.contains(key)) {
                return ImageState.IS_SAVING;
            } else {
                return ImageState.SAVED;
            }
        }

        return ImageState.ABSENT;
    }

    // </editor-fold>

    // <editor-fold desc="inner classes">

    private class ImageCacheObservable extends Observable<ImageLoadingObserver> {

        private void notifyLoadStarted(String imageTag) {
            synchronized (mObservers) {
                for (ImageLoadingObserver observer : mObservers) {
                    observer.notifyLoadStarted(imageTag);
                }
                progressSet.add(imageTag);
            }
        }

        private void notifyLoadFinished(String imageTag) {
            synchronized (mObservers) {
                progressSet.remove(imageTag);
                for (ImageLoadingObserver observer : mObservers) {
                    observer.notifyLoadFinished(imageTag);
                }
            }
        }
    }

    private class KSLruCache extends LruCache<String, RecyclingBitmapDrawable> {

        private KSLruCache(int maxSize) {
            super(maxSize);
        }

        @Override
        protected int sizeOf(String key, RecyclingBitmapDrawable value) {
            return new BitmapSize().sizeOf(value.getBitmap());
        }

        @Override
        protected void entryRemoved(boolean evicted, String key, RecyclingBitmapDrawable oldValue, RecyclingBitmapDrawable newValue) {
            super.entryRemoved(evicted, key, oldValue, newValue);
            oldValue.setIsCached(false);
        }
    }

    private class BitmapSerializer implements CacheSerializer<Bitmap> {

        @Override
        public Bitmap fromString(String data) {
            byte[] bytes = Base64.decode(data, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }

        @Override
        public String toString(Bitmap object) {
            int newWidth = object.getWidth() * serializationQuality / 100;
            int newHeight = object.getHeight() * serializationQuality / 100;
            Bitmap bitmapToSave = Bitmap.createScaledBitmap(object, newWidth, newHeight, false);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapToSave.compress(Bitmap.CompressFormat.JPEG, serializationQuality, stream);
            return Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
        }
    }

    private class BitmapSize implements SizeOf<Bitmap> {

        @Override
        public int sizeOf(Bitmap object) {
            return object.getByteCount();
        }
    }

    private void increment() {
        imagesCounter++;
    }

    private void decrement() {
        imagesCounter--;
    }

    // </editor-fold>

}
