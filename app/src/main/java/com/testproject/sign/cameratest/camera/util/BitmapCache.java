package com.testproject.sign.cameratest.camera.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.util.LruCache;

public class BitmapCache {

    private static final String LOG_TAG = BitmapCache.class.getSimpleName();
    private static volatile BitmapCache instance;
    private static Context context;

    public static BitmapCache getInstance() {
        if (instance == null) {
            synchronized (BitmapCache.class) {
                instance = new BitmapCache();
            }
        }
        return instance;
    }

    public static void init(Context context) {
        BitmapCache.context = context;
    }

    private LruCache<String, RecyclingBitmapDrawable> mMemoryCache;

    private BitmapCache() {
        final int cacheSize = MemoryHelper.calculateMemoryCacheSize(context);
        mMemoryCache = new LruCache<String, RecyclingBitmapDrawable>(cacheSize) {
            @Override
            protected int sizeOf(String key, RecyclingBitmapDrawable bitmapDrawable) {
                return bitmapDrawable.getBitmap().getByteCount();
            }

            @Override
            protected void entryRemoved(boolean evicted, String key, RecyclingBitmapDrawable oldValue, RecyclingBitmapDrawable newValue) {
                super.entryRemoved(evicted, key, oldValue, newValue);
                oldValue.setIsCached(false);
            }
        };
    }

    public void addBitmapToMemoryCache(String key, RecyclingBitmapDrawable bitmap) {
        Log.i(LOG_TAG, "addBitmapToMemoryCache call - key is " + key);
        if (getBitmapFromMemCache(key) == null) {
            bitmap.setIsCached(true);
            mMemoryCache.put(key, bitmap);
            Log.i(LOG_TAG, "addBitmapToMemoryCache - bitmap was put, key is " + key);
        }
    }

    public void removeFromMemCache(String key) {
        Log.i(LOG_TAG, "removeFromMemCache call - key is " + key);
        if (getBitmapFromMemCache(key) != null) {
            mMemoryCache.remove(key);
            Log.i(LOG_TAG, "removeFromMemCache call - bitmap was removed, key is " + key);
        }
    }

    public RecyclingBitmapDrawable getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public void clearCache() {
        mMemoryCache.evictAll();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void trimToSize() {
        mMemoryCache.trimToSize(mMemoryCache.size() / 2);
    }

    public int getSize() {
        return mMemoryCache.size();
    }

}
