package com.testproject.sign.cameratest.camera.ui;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.adapters.CustomItemTouchHelper;
import com.testproject.sign.cameratest.adapters.DeleteItemListener;
import com.testproject.sign.cameratest.adapters.OnStartDragListener;
import com.testproject.sign.cameratest.adapters.SpacesItemDecoration;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.base_ui.GridItemTouchHelperCallback;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.ViewUtils;

import java.util.ArrayList;

public class MovePagesFragment extends AbstractFragment implements OnStartDragListener, DeleteItemListener {

    private PagesGridAdapter adapter;
    private CustomItemTouchHelper itemTouchHelper;
    private DeletePageItem deleteView;
    private RecyclerView recyclerView;

    private boolean deleteCancelled = false;

    public static MovePagesFragment newInstance() {
        return new MovePagesFragment();
    }

    public MovePagesFragment() {
    }

    @Override
    public void onCreateView(LayoutInflater inflater) {
        setContentView(R.layout.move_pages_fragment);
        initView();
    }

    private void initView() {
        prepareToolbar("Move pages", R.id.toolbar, R.drawable.back_icon);
        deleteView = (DeletePageItem) findViewById(R.id.delete_page_item);
        adapter = new PagesGridAdapter(new ArrayList<>(KSImageCache.getInstance().getCachedImagesKeySet()), this, this);
        adapter.setDeletePageView(deleteView);
        KSImageCache.getInstance().registerObserver(adapter);
        recyclerView = (RecyclerView) findViewById(R.id.pages_grid_recycler);
        CustomItemTouchHelper.Callback callback = new GridItemTouchHelperCallback(adapter);
        itemTouchHelper = new CustomItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), ViewUtils.countGridViewColumns()));
        recyclerView.setItemViewCacheSize(-2);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addItemDecoration(new SpacesItemDecoration(getContext(),
                getResources().getDimensionPixelOffset(R.dimen.grid_page_items_vertical_spacing),
                getResources().getDimensionPixelOffset(R.dimen.grid_page_items_horizontal_spacing),
                adapter.getItemCount()));
        recyclerView.setAdapter(adapter);
        hideDeleteView();
    }

    @Override
    public void goBack() {
        KSImageCache.getInstance().clearBucket();
        CoreSignFragmentManager.getInstance().showCameraGalleryFragment(CameraMode.DOCUMENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        KSImageCache.getInstance().unregisterObserver(adapter);
        KSImageCache.getInstance().clearBucket();
    }

    @Override
    public void onLockActions() {

    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
        showDeleteView();
    }

    @Override
    public void onFinishDrag(RecyclerView.ViewHolder viewHolder) {
        hideDeleteView();
    }

    @Override
    public void onItemDeleted(int position, String tag) {
        deleteCancelled = false;
        hideDeleteView();
        KSImageCache.getInstance().moveImageToBucket(tag);
        showDeleteSnackBar();
    }

    private void showDeleteSnackBar() {
        if (getView() == null) return;
        Snackbar snackbar = Snackbar.make(getView(), "Page was successfully deleted!", Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(getResources().getColor(R.color.white));
        snackbar.setAction(getString(R.string.S_CANCEL), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                revertDeletingImage();
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.delete_page_color));
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                finishDeletingImage();
            }
        });
        snackbar.show();
    }

    private void finishDeletingImage() {
        if (!deleteCancelled) {
            KSImageCache.getInstance().deleteLastImageInBucket();
            deleteCancelled = false;
        }
    }

    private void revertDeletingImage() {
        deleteCancelled = true;
        final Pair<String, Integer> restoredImage = KSImageCache.getInstance().restoreImageFromBucket(true);
        adapter.setData(new ArrayList<>(KSImageCache.getInstance().getCachedImagesKeySet()));
        deleteCancelled = false;
        if (restoredImage != null) {
            recyclerView.scrollToPosition(restoredImage.second);
        }
    }

    private void showDeleteView() {
        //TODO change for animation
        deleteView.setHovered(false);
        deleteView.show();
    }

    private void hideDeleteView() {//TODO change for animation
        deleteView.hide();
    }
}
