package com.testproject.sign.cameratest.camera.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.cameraview.CameraView;
import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.options.EditedImage;
import com.testproject.sign.cameratest.camera.options.EditedImagesManager;
import com.testproject.sign.cameratest.camera.options.ImageProcessor;
import com.testproject.sign.cameratest.camera.util.BitmapFromPath;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.listeners.ActivityListener;
import com.testproject.sign.cameratest.util.ProgressDialogController;
import com.testproject.sign.cameratest.util.UIThreadHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.testproject.sign.cameratest.camera.ui.GalleryMainFragment.CAMERA_MODE_KEY;

public class CameraNewPhotoFragment extends AbstractFragment {

    private static final String LOG_TAG = CameraNewPhotoFragment.class.getSimpleName();
    public static final int GALLERY_REQUEST_CODE = 12420;

    private enum FlashMode {
        FLASH_ON,
        FLASH_OFF
    }

    private enum GridMode {
        GRID_ON,
        GRID_OFF
    }

    private CameraView cameraView;
    private ImageView galleryBtn;
    private ImageView flashIcon;
    private ImageView gridIcon;
    private RelativeLayout counterLayout;
    private TextView tvCounter;

    private CameraMode cameraMode = CameraMode.DOCUMENT;
    private FlashMode flashMode = FlashMode.FLASH_OFF;
    private GridMode gridMode = GridMode.GRID_OFF;

    private KSImageCache cacheManager;

    private GridModeLayout gridModeLayout;

    private ExecutorService executorService;

    public static CameraNewPhotoFragment newInstance() {
        return new CameraNewPhotoFragment();
    }

    @Nullable
    @Override
    public void onCreateView(LayoutInflater inflater) {
        setContentView(R.layout.camera_new_photo_fragment);

        if (getArguments() != null) {
            Bundle args = getArguments();
            cameraMode = (CameraMode) args.getSerializable(CAMERA_MODE_KEY);
        }
        executorService = Executors.newSingleThreadExecutor();
        cacheManager = KSImageCache.getInstance();
        cacheManager.refresh();

        initViews();
        setToolbar();
        updateRightButtonLayout();
        initCamera();
        getCurrentActivity().addActivityListener(activityListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getCurrentActivity().removeActivityListener(activityListener);
    }

    @Override
    public void onLockActions() {

    }

    @Override
    public void goBack() {
        KSImageCache.getInstance().unregisterAll();
        KSImageCache.getInstance().clearCache();
        EditedImagesManager.getInstance().removeAll();
//        CoreSignFragmentManager.getInstance().showRootFragment();
    }

    private void initCamera() {
        if (cameraView != null) {
            cameraView.addCallback(cameraListener);
//            cameraView.setAspectRatio(AspectRatio.of(3, 4));

            if (Build.VERSION.SDK_INT < 21) {
                cameraView.setAutoFocus(false);
            }
        }

        handleFlashMode(flashMode);
        handleGridMode(gridMode);
    }

    private void initViews() {
        cameraView = (CameraView) findViewById(R.id.camera_view);
        galleryBtn = (ImageView) findViewById(R.id.camera_gallery_btn);
        flashIcon = (ImageView) findViewById(R.id.icon_flash);
        gridIcon = (ImageView) findViewById(R.id.icon_grid);
        counterLayout = (RelativeLayout) findViewById(R.id.counterLayout);
        tvCounter = (TextView) findViewById(R.id.tv_count);

        ImageView takePhotoBtn = (ImageView) findViewById(R.id.camera_shot_btn);

        takePhotoBtn.setOnClickListener(takePhotoClickListener);
        galleryBtn.setOnClickListener(galleryBtnClickListener);
        counterLayout.setOnClickListener(counterLayoutClickListener);
        findViewById(R.id.btn_flash).setOnClickListener(flashBtnClickListener);
        findViewById(R.id.btn_grid).setOnClickListener(gridBtnClickListener);
        findViewById(R.id.camera_close_btn).setOnClickListener(onBackClickListener);

        gridModeLayout = new GridModeLayout(getContext());
        ViewGroup container = (ViewGroup) findViewById(R.id.camera_new_photo_view_container);
        container.addView(gridModeLayout);
    }

    private void setToolbar() {
        Toolbar cameraToolbar = (Toolbar) findViewById(R.id.camera_toolbar);
        cameraToolbar.setNavigationIcon(R.drawable.back_icon);
        cameraToolbar.setNavigationOnClickListener(onBackClickListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (cameraView != null) {
            cameraView.start();
        }
    }

    @Override
    public void onPause() {
        if (cameraView != null) {
            cameraView.stop();
        }
        super.onPause();
    }

    private CameraView.Callback cameraListener = new CameraView.Callback() {
        @Override
        public void onCameraOpened(CameraView cameraView) {
            Log.v(LOG_TAG, "onCameraOpened");
        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            Log.v(LOG_TAG, "onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, byte[] data) {
            processImage(data);
        }
    };

    private void processImage(final byte[] image) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                final Bitmap currentBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                String originBitmapName = KSImageCache.DOC_IMG_NAME_PREFIX.concat(KSImageCache.getInstance().getNextName());
//                cacheManager.setSerializationQuality(60);
                cacheManager.setAutoSerializationQuality(currentBitmap.getWidth(), currentBitmap.getHeight());
                cacheManager.setImage(currentBitmap, originBitmapName);
                addImageToManager(originBitmapName, currentBitmap.getWidth(), currentBitmap.getHeight());

                UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        if (cameraMode == CameraMode.SIGNATURE || cameraMode == CameraMode.INITIALS) {
                            showNextStep();
                        } else if (cameraMode == CameraMode.DOCUMENT) {
                            updateRightButtonLayout();
                        }
                    }
                });
            }
        });
    }

    private void addImageToManager(String imageId, int imageWidth, int imageHeight) {
        EditedImage image = new EditedImage(imageId);
        if (cameraMode == CameraMode.INITIALS || cameraMode == CameraMode.SIGNATURE) {
            image.setColor(100);
            ImageProcessor.addDefaultCornersWithProportions(image.getCorners(), imageWidth,
                    imageHeight, EditImageView.CROP_ASPECT_RATIO);
        }
        EditedImagesManager.getInstance().add(image);
    }

    private void changeGridMode() {
        gridMode = (gridMode == GridMode.GRID_OFF) ? GridMode.GRID_ON : GridMode.GRID_OFF;
        gridIcon.setImageResource(gridMode == GridMode.GRID_ON ? R.drawable.icon_grid_on : R.drawable.icon_grid_off);

        handleGridMode(gridMode);
    }

    private void changeFlashMode() {
        flashMode = (flashMode == FlashMode.FLASH_ON) ? FlashMode.FLASH_OFF : FlashMode.FLASH_ON;
        flashIcon.setImageResource(flashMode == FlashMode.FLASH_OFF ? R.drawable.icon_flash_turn_off : R.drawable.icon_flash_on);

        handleFlashMode(flashMode);
    }

    private void handleGridMode(GridMode gridMode) {
        switch (gridMode) {
            case GRID_OFF:
                if (gridModeLayout != null) {
                    gridModeLayout.setVisibility(View.GONE);
                }
                break;
            case GRID_ON:
                if (gridModeLayout != null) {
                    gridModeLayout.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void handleFlashMode(FlashMode flashMode) {
        switch (flashMode) {
            case FLASH_OFF:
                if (cameraView != null) {
                    cameraView.setFlash(CameraView.FLASH_OFF);
                }
                break;
            case FLASH_ON:
                if (cameraView != null) {
                    cameraView.setFlash(CameraView.FLASH_ON);
                }
                break;
        }
    }

    private View.OnClickListener takePhotoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (cameraView != null) {
                cameraView.takePicture();
            }
        }
    };

    private void updateRightButtonLayout() {
        if (galleryBtn != null && counterLayout != null) {
            int imageCounter = KSImageCache.getInstance().getSavedImagesCount();

            galleryBtn.setVisibility(imageCounter == 0 && cameraMode == CameraMode.DOCUMENT ? View.VISIBLE : View.GONE);
            counterLayout.setVisibility(imageCounter > 0 && cameraMode == CameraMode.DOCUMENT ? View.VISIBLE : View.GONE);
            tvCounter.setText(String.valueOf(imageCounter));
        }
    }

    private View.OnClickListener galleryBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            chooseImagesFromGallery();
        }
    };

    private View.OnClickListener counterLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showNextStep();
        }
    };

    private View.OnClickListener flashBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            changeFlashMode();
        }
    };

    private View.OnClickListener gridBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            changeGridMode();
        }
    };

    private void chooseImagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getCurrentActivity().startActivityForResult(Intent.createChooser(intent, "Select Images"), GALLERY_REQUEST_CODE);
    }

    private void showNextStep() {
        CoreSignFragmentManager.getInstance().showCameraGalleryFragment(cameraMode);
    }

    private View.OnClickListener onBackClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v(LOG_TAG, "Back! Home!");
            if (isAdded() && getActivity() != null && !getActivity().isFinishing()) {
                getActivity().onBackPressed();
            }
        }
    };

    private ActivityListener activityListener = new ActivityListener() {
        @Override
        public void onActivityResult(int requestCode, int resultCode, final Intent data) {
            if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_REQUEST_CODE && data != null) {
                executorService.submit(new Runnable() {
                    @Override
                    public void run() {
                        ProgressDialogController.getInstance().showTransparentProgressBar(false);
                        if (data.getClipData() != null) {
                            for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                                Uri imageUri = data.getClipData().getItemAt(i).getUri();
                                addBitmapFromUri(imageUri);
                            }
                        } else {
                            addBitmapFromUri(data.getData());
                        }

                        UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                updateRightButtonLayout();
                                ProgressDialogController.getInstance().hideProgressDialog();
                                showNextStep();
                            }
                        });
                    }
                });
            }
        }
    };

    private void addBitmapFromUri(Uri imageUri) {
        final Bitmap selectedImage = BitmapFromPath.getFromUri(getContext(), imageUri);
        if (selectedImage != null) {
            String originBitmapName = KSImageCache.DOC_IMG_NAME_PREFIX.concat(KSImageCache.getInstance().getNextName());
//            cacheManager.setSerializationQuality(60);
            cacheManager.setAutoSerializationQuality(selectedImage.getWidth(), selectedImage.getHeight());
            cacheManager.setImage(selectedImage, originBitmapName);
            addImageToManager(originBitmapName, selectedImage.getWidth(), selectedImage.getHeight());
        }
    }

    public class GridModeLayout extends View {
        Paint p;

        public GridModeLayout(Context context) {
            super(context);
            initPaint();
        }

        public GridModeLayout(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);
            initPaint();
        }

        public GridModeLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            initPaint();
        }

        private void initPaint() {
            p = new Paint();
            p.setColor(Color.WHITE);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            int w = canvas.getWidth();
            int h = canvas.getHeight();
            canvas.drawLine(w / 3, 0, w / 3, h, p);
            canvas.drawLine(w - w / 3, 0, w - w / 3, h, p);
            canvas.drawLine(0, h / 3, w, h / 3, p);
            canvas.drawLine(0, h - h / 3, w, h - h / 3, p);
        }
    }
}
