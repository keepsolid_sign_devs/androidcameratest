package com.testproject.sign.cameratest.camera.util;

public enum ImageState {
    SAVED,
    IS_SAVING,
    ABSENT
}
