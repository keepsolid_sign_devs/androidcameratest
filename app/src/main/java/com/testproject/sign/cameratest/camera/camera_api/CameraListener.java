package com.testproject.sign.cameratest.camera.camera_api;

public interface CameraListener {

    void onBitmapReady(byte[] image);
}
