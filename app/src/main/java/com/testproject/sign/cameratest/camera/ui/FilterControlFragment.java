package com.testproject.sign.cameratest.camera.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractFragment;
import com.testproject.sign.cameratest.base_ui.CoreSignFragmentManager;
import com.testproject.sign.cameratest.camera.CameraMode;
import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;
import com.testproject.sign.cameratest.camera.options.EffectType;
import com.testproject.sign.cameratest.camera.util.KSImageCache;
import com.testproject.sign.cameratest.camera.util.Log;
import com.testproject.sign.cameratest.util.ProgressDialogController;
import com.testproject.sign.cameratest.util.UIThreadHelper;

import java.util.ArrayList;
import java.util.Set;

import static com.testproject.sign.cameratest.camera.ui.GalleryMainFragment.CAMERA_MODE_KEY;
import static com.testproject.sign.cameratest.camera.ui.GalleryMainFragment.PAGE_KEY;

public class FilterControlFragment extends AbstractFragment {

    private static final String TAG = FilterControlFragment.class.getSimpleName();
    public static final String TUNE_MODE_KEY = "tune_mode";

    private int currentPage;
    private OnTuneEventListener.TuneMode mode = OnTuneEventListener.TuneMode.NONE;
    private CameraMode cameraMode = CameraMode.DOCUMENT;

    private GalleryPagerAdapter adapter;
    private GalleryViewPager galleryPager;
    private FilterView filterView;
    private final ArrayList<String> images = new ArrayList<>();

    public static FilterControlFragment newInstance(@NonNull CameraMode cameraMode, @NonNull OnTuneEventListener.TuneMode tuneMode, int page) {
        FilterControlFragment fragment = new FilterControlFragment();
        Bundle args = new Bundle();
        args.putSerializable(TUNE_MODE_KEY, tuneMode);
        args.putSerializable(CAMERA_MODE_KEY, cameraMode);
        args.putInt(PAGE_KEY, page);
        fragment.setArguments(args);
        return fragment;
    }

    public FilterControlFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mode = (OnTuneEventListener.TuneMode) getArguments().getSerializable(TUNE_MODE_KEY);
        cameraMode = (CameraMode) getArguments().getSerializable(CAMERA_MODE_KEY);
        currentPage = getArguments().getInt(PAGE_KEY);
    }

    @Override
    public void onCreateView(LayoutInflater inflater) {
        setContentView(R.layout.filter_control_fragment);
        String toolbarTitle = "";
        switch (mode) {
            case COLOR:
                toolbarTitle = "Color tune";
                break;
            case BRIGHTNESS:
                toolbarTitle = "Brightness";
                break;
            case CONTRAST:
                toolbarTitle = "Contrast";
                break;
        }
        prepareToolbar(toolbarTitle, R.id.toolbar, R.drawable.back_icon);

        filterView = (FilterView) findViewById(R.id.filter_view);
        filterView.setMode(mode);
        setupViewPager();

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.filter_control_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_apply:
                confirmEffectsForAllImages();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager() {
        Set<String> cachedImages = KSImageCache.getInstance().getCachedImagesKeySet();
        images.addAll(cachedImages);
        adapter = new GalleryPagerAdapter(getFragmentManager(), images, mode);
        galleryPager = (GalleryViewPager) findViewById(R.id.view_pager);
        galleryPager.setClipToPadding(false);
        galleryPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.gallery_page_margin_end));
        galleryPager.setAdapter(adapter);
        galleryPager.setCurrentItem(currentPage);
        galleryPager.addOnPageChangeListener(pageChangeListener);

        galleryPager.post(new Runnable() {
            @Override
            public void run() {
                pageChangeListener.onPageSelected(currentPage);
            }
        });
        handleSwipeEnabled();
    }

    private void handleSwipeEnabled() {
        galleryPager.setSwipeEnabled(images.size() > 1);
    }

    private ImageFragment getCurrentFragment() {
        return (ImageFragment) adapter.getRegisteredFragment(galleryPager.getCurrentItem());
    }

    private void setupCurrentFragment() {
        ImageFragment currentFragment = getCurrentFragment();
        if (currentFragment != null) {
            filterView.setOnTuneEventListener(currentFragment);
            filterView.setTuneValue((int) currentFragment.getTuneValue());
        }
    }

    @Override
    public void goBack() {
        clearEffectsForAllImages();
        showGalleryFragment();
    }

    private void showGalleryFragment() {
        CoreSignFragmentManager.getInstance().showCameraGalleryFragment(cameraMode, currentPage);
    }

    private void confirmEffectsForAllImages() {
        ProgressDialogController.getInstance().showTransparentProgressBar(false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < adapter.getCount(); i++) {
                    ImageFragment currentFragment = (ImageFragment) adapter.getRegisteredFragment(i);
                    if (currentFragment != null) {
                        currentFragment.onConfirmButtonPressed();
                    }
                }
                ProgressDialogController.getInstance().hideProgressDialog();
                UIThreadHelper.runOnUIThread(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        showGalleryFragment();
                    }
                });
            }
        }).start();
    }

    private void clearEffectsForAllImages() {
        for (int i = 0; i < adapter.getCount(); i++) {
            ImageFragment currentFragment = (ImageFragment) adapter.getRegisteredFragment(i);
            currentFragment.onCancelTune();
        }
    }

    @Override
    public void onLockActions() {

    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            Log.v(TAG, "onPageSelected " + position);
            currentPage = position;
            setupCurrentFragment();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
