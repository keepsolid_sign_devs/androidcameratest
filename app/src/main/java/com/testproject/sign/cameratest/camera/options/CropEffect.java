package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;


import com.testproject.sign.cameratest.camera.ui.EditImageView;

import org.opencv.core.Point;

import java.util.ArrayList;


/**
 * Created by prog on 03.08.16.
 */
public class CropEffect extends Effect {

    private ArrayList<Point> corners = new ArrayList<>(4);

    public CropEffect(ArrayList<Point> corners) {
        for (Point item : corners) {
            this.corners.add(item.clone());
        }
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.CROP;
    }

    @Override
    public Bitmap execute(Bitmap originalImage) {
        return cropImage(originalImage);
    }

    private Bitmap cropImage(Bitmap bitmap) {
        ArrayList<Point> tmpCorners = new ArrayList<>(4);
        for (Point item : corners) {
            tmpCorners.add(item.clone());
        }
        return ImageProcessor.warp(bitmap, tmpCorners);
    }

    @Override
    public void undo() {

    }
}
