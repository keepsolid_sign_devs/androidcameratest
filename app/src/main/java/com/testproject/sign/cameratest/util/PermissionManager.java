package com.testproject.sign.cameratest.util;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Observable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.base_ui.AbstractActivity;


public class PermissionManager {

    public static final int MY_CONTACTS_PERMISSION_CODE = 666;
    public static final int MY_STORAGE_PICK_FILE_PERMISSION_CODE = 777;
    public static final int MY_STORAGE_PICK_DIRECTORY_PERMISSION_CODE = 888;
    public static final int MY_STORAGE_SAVE_LOCALLY_PERMISSION_CODE = 341;
    public static final int MY_GET_ACCOUNT_PERMISSION_CODE = 999;
    public static final int MY_CAMERA_PERMISSION_CODE = 998;

    private static volatile PermissionManager instance;

    private PermissionObservable permissionObservable;
    private AbstractActivity activity;

    private PermissionManager() {
    }

    public static PermissionManager getInstance() {
        if (instance == null) {
            synchronized (PermissionManager.class) {
                instance = new PermissionManager();
            }
        }
        return instance;
    }

    public void init(AbstractActivity activity) {
        this.activity = activity;
        permissionObservable = new PermissionObservable();
    }

    public void requestPermission(String permission, int permissionCode) {
        if (activity == null || activity.isFinishing())
            return;

        if (!isObtained(permission)) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{permission},
                    permissionCode);
        }
    }

    public void requestPermission(final String permission, final int permissionCode, String explanation, DialogInterface.OnCancelListener cancelListener) {
        if (activity == null || activity.isFinishing())
            return;

        if (!isObtained(permission)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showMessageOKCancel(explanation,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermission(permission, permissionCode);
                            }
                        }, cancelListener);
            } else {
                requestPermission(permission, permissionCode);
            }
        }
    }

    public void requestPermissions(String[] permissions, int permissionCode) {
        ActivityCompat.requestPermissions(activity,
                permissions,
                permissionCode);
    }

    public void registerPermissionObserver(PermissionsObserver observer) {
        if (permissionObservable != null) {
            permissionObservable.registerObserver(observer);
        }
    }


    public void unregisterPermissionObserver(PermissionsObserver observer) {
        if (permissionObservable != null) {
            try {
                permissionObservable.unregisterObserver(observer);
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void unregisterAll() {
        if (permissionObservable != null) {
            permissionObservable.unregisterAll();
        }
    }

    public void onPermissionsObtained(String[] permissions, int[] obtained, int permissionCode) {
        String[] obtainedPermissions = new String[0];
        String[] deniedPermissions = new String[0];
        for (int i = 0; i < permissions.length; i++) {
            if (obtained[i] == PackageManager.PERMISSION_GRANTED) {
                String[] obtainedPermissionsCopy = obtainedPermissions.clone();
                obtainedPermissions = new String[i + 1];
                System.arraycopy(obtainedPermissionsCopy, 0, obtainedPermissions, 0, obtainedPermissionsCopy.length);
                obtainedPermissions[i] = permissions[i];
            } else {
                String[] deniedPermissionsCopy = deniedPermissions.clone();
                deniedPermissions = new String[i + 1];
                System.arraycopy(deniedPermissionsCopy, 0, deniedPermissions, 0, deniedPermissionsCopy.length);
                deniedPermissions[i] = permissions[i];
            }
        }
        if (obtainedPermissions.length > 0) {
            permissionObservable.notifyPermissionGranted(obtainedPermissions, permissionCode);
        }

        if (deniedPermissions.length > 0) {
            permissionObservable.notifyPermissionDenied(deniedPermissions, permissionCode);
        }
    }

    public boolean isObtained(String permission) {
        return ActivityCompat.checkSelfPermission(activity,
                permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, final DialogInterface.OnCancelListener cancelListener) {
        new AlertDialog.Builder(activity, R.style.AlertDialogCustom)
                .setMessage(message)
                .setPositiveButton(activity.getString(R.string.S_OK), okListener)
                .setNegativeButton(activity.getString(R.string.S_CANCEL), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        cancelListener.onCancel(dialogInterface);
                    }
                })
                .setOnCancelListener(cancelListener)
                .create()
                .show();
    }

    private class PermissionObservable extends Observable<PermissionsObserver> {

        private void notifyPermissionGranted(String[] permissions, int permissionCode) {
            synchronized (mObservers) {
                for (PermissionsObserver observer : mObservers) {
                    observer.onPermissionGranted(permissions, permissionCode);
                }
            }
        }

        private void notifyPermissionDenied(String[] permissions, int permissionCode) {
            synchronized (mObservers) {
                for (PermissionsObserver observer : mObservers) {
                    observer.onPermissionDenied(permissions, permissionCode);
                }
            }
        }
    }
}
