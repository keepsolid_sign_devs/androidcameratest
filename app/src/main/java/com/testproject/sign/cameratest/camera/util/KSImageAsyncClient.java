package com.testproject.sign.cameratest.camera.util;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import com.testproject.sign.cameratest.camera.callbacks.CameraOperationListener;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class KSImageAsyncClient {
    private static final Handler UI_HANDLER = new Handler(Looper.getMainLooper());
    private static final Thread UI_THREAD = Looper.getMainLooper().getThread();

    private ExecutorService mExecutorService;

    public KSImageAsyncClient(@NonNull ExecutorService mExecutorService) {
        this.mExecutorService = mExecutorService;
    }

    public <T> Future<T> submitTask(@NonNull final Callable<T> callable, @Nullable final CameraOperationListener<T> callback) {
        return mExecutorService.submit(new Callable<T>() {
            @Override
            public T call() throws Exception {
                try {
                    T result = callable.call();
                    onSuccess(result, callback);
                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        });
    }

    public void cancellAll() {
        mExecutorService.shutdownNow();
    }

    private <T> void onSuccess(final T result, @Nullable final CameraOperationListener<T> callback) {
        if (callback != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.onSuccess(result);
                }
            });
        }
    }

    private void runOnUiThread(@NonNull Runnable runnable) {
        if (Thread.currentThread() != UI_THREAD) {
            UI_HANDLER.post(runnable);
        } else {
            runnable.run();
        }
    }
}
