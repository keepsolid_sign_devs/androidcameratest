package com.testproject.sign.cameratest.util;

public interface PermissionsObserver {

    void onPermissionGranted(String[] permissions, int permissionRequestCode);

    void onPermissionDenied(String[] permissions, int permissionRequestCode);

}
