package com.testproject.sign.cameratest.camera.camera_api;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("deprecation")
public class PreLollipopCameraApi implements CameraApi, SurfaceHolder.Callback {

    private static final String LOG_TAG = PreLollipopCameraApi.class.getSimpleName();

    private final Activity context;
    private SurfaceHolder previewHolder = null;
    private SurfaceView frame = null;

    private CameraListener cameraListener;
    private Camera camera = null;

    private int displayOrientation;
    private boolean isDetached = true;
    private boolean isShooting = false;
    private boolean isFlashOn = false;

    private float focusX, focusY;

    public PreLollipopCameraApi(Activity context, SurfaceView frame, CameraListener cameraListener,
                                final View focusIndicatorView) {
        this.frame = frame;
        this.cameraListener = cameraListener;
        this.context = context;
        init();
    }

    // <editor-fold desc="prepare">

    private void init() {
        previewHolder = this.frame.getHolder();
        previewHolder.addCallback(this);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void preparePreview() {
        camera = Camera.open();
        setCameraParameters();
        try {
            camera.setPreviewDisplay(previewHolder);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void stopCameraPreview() {
        try {
            camera.stopPreview();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    // </editor-fold>

    // <editor-fold desc="size & orientation">

    public void determineDisplayOrientation() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraId = 0;
        Camera.getCameraInfo(cameraId, cameraInfo);
        int rotation = context.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;

            case Surface.ROTATION_90:
                degrees = 90;
                break;

            case Surface.ROTATION_180:
                degrees = 180;
                break;

            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }


        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            displayOrientation = (cameraInfo.orientation + degrees) % 360;
            displayOrientation = (360 - displayOrientation) % 360;
        } else {
            displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
        }

        camera.setDisplayOrientation(displayOrientation);
    }

    void setPreviewSize() {
        Point outSize = new Point(0, 0);
        context.getWindowManager().getDefaultDisplay().getSize(outSize);
        Camera.Size bestSize = null;
        Camera.Parameters parameters = camera.getParameters();
        List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
        List<Camera.Size> photoSizes = parameters.getSupportedPictureSizes();
        int bestDiff = 0;
        int diff;
        for (Camera.Size size : previewSizes) {
            diff = Math.abs(outSize.x - size.height) + Math.abs(outSize.y - size.width);
            if (bestSize == null || diff < bestDiff) {
                bestSize = size;
                bestDiff = diff;
            }
        }
        if (bestSize != null) {
            parameters.setPreviewSize(bestSize.width, bestSize.height);
        }

        if (photoSizes.get(0).width > photoSizes.get(photoSizes.size() - 1).width) {
            parameters.setPictureSize(photoSizes.get(0).width, photoSizes.get(0).height);
        } else {
            parameters.setPictureSize(photoSizes.get(photoSizes.size() - 1).width,
                    photoSizes.get(photoSizes.size() - 1).height);
        }

        camera.setParameters(parameters);
        double coeff = (double) bestSize.width / bestSize.height;
        frame.getLayoutParams().height = (int) (frame.getWidth() * coeff);
        frame.requestLayout();
    }

    // </editor-fold>

    // <editor-fold desc="camera">

    private void setCameraParameters() {
        setPreviewSize();
        Camera.Parameters params = camera.getParameters();

        if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
        if (params.isAutoWhiteBalanceLockSupported()) {
            params.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
        }

        determineDisplayOrientation();
        camera.setParameters(params);
    }

    public void doTouchFocus(final Rect focusRect, final Rect clickRect) {
        if (camera != null) {
            List<Camera.Area> focusList = new ArrayList<>();
            Camera.Area focusArea = new Camera.Area(focusRect, 1000);
            focusList.add(focusArea);
            Camera.Parameters param = camera.getParameters();
            if (param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                try {
                    param.setFocusAreas(focusList);
                    param.setMeteringAreas(focusList);
                    camera.setParameters(param);
                    camera.autoFocus(new Camera.AutoFocusCallback() {
                        @Override
                        public void onAutoFocus(boolean success, Camera camera1) {
//                            transition.stopAnimation();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(LOG_TAG, "Unable to autofocus");
                }
            }
        }
    }

    private void capture() {
        Camera.Parameters param = camera.getParameters();
        boolean hasAutoFocus = param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO);
        if (!hasAutoFocus) {
            camera.takePicture(null, null, new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    if (data != null) {
                        cameraListener.onBitmapReady(data);
                    }
                }
            });
        } else {
            camera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                    camera.takePicture(null, null, new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(byte[] data, Camera camera) {
                            if (data != null) {
                                cameraListener.onBitmapReady(data);
                            }
                        }
                    });
                }
            });
        }
    }

    // </editor-fold>

    // <editor-fold desc="public api">

    @Override
    public void shoot() {
        if (camera != null) {
            capture();
        }
    }

    @Override
    public boolean toggleFlashlight() {
        Camera.Parameters p = camera.getParameters();
        if (p.getSupportedFlashModes() != null) {
            if (p.getSupportedFlashModes().contains(Camera.Parameters.FLASH_MODE_TORCH)) {
                if (p.getFlashMode().equals(Camera.Parameters.FLASH_MODE_TORCH)) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    Log.i(LOG_TAG, "FlashLight is off!");
                } else {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                    Log.i(LOG_TAG, "FlashLight is on!");
                    isFlashOn = true;
                }
                camera.setParameters(p);
            } else {
                Log.e(LOG_TAG, "Flashlight is not supported!");
                Toast.makeText(context, "Sorry, you have no flashlight support", Toast.LENGTH_SHORT).show();
            }
        }
        return isFlashOn;
    }

    @Override
    public boolean isFlashOn() {
        return isFlashOn;
    }


    @Override
    public void onResume() {
        Log.d(LOG_TAG, "Using preLollipop api.");
        if (isDetached) {
            previewHolder.addCallback(this);
            isDetached = false;
        } else {
            preparePreview();
            camera.startPreview();
        }
    }

    @Override
    public void onPause() {
        Log.v(LOG_TAG, "onPause called");
        stopCameraPreview();
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }

    @Override
    public void refocus(Rect focusRect, Rect clickRect) {
        doTouchFocus(focusRect, clickRect);
    }

    @Override
    public boolean isShooting() {
        return isShooting;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        isDetached = false;
        preparePreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(LOG_TAG, "surfaceDestroyed");
        isDetached = true;
        previewHolder.removeCallback(this);
    }

    // </editor-fold>
}
