package com.testproject.sign.cameratest.base_ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.testproject.sign.cameratest.listeners.ActivityListener;
import com.testproject.sign.cameratest.util.PermissionManager;
import com.testproject.sign.cameratest.util.ProgressDialogController;

import java.util.ArrayList;
import java.util.List;

public class AbstractActivity extends AppCompatActivity {

    private List<ActivityListener> activityListeners = new ArrayList<>();

    private long mLastClickTime;

    private boolean needToCleanUp = true;
    // <editor-fold desc="activity life cycle">


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        needToCleanUp = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogManager.getInstance().init(this);
    }

    @Override
    protected void onStart() {
        ProgressDialogController.getInstance().init(this);
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogManager.getInstance().onDestroyActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (ActivityListener activityListener : activityListeners) {
            activityListener.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.getInstance().onPermissionsObtained(permissions, grantResults, requestCode);
    }

    // </editor-fold>

    // <editor-fold desc="getters & setters">

    public void addActivityListener(ActivityListener activityListener) {
        if (!activityListeners.contains(activityListener)) {
            activityListeners.add(activityListener);
        }
    }

    public void removeActivityListener(ActivityListener activityListener) {
        activityListeners.remove(activityListener);
    }

    public void setNeedToCleanUp(boolean needToCleanUp) {
        this.needToCleanUp = needToCleanUp;
    }

    public boolean isNeedToCleanUp() {
        return this.needToCleanUp;
    }

    public long getmLastClickTime() {
        return mLastClickTime;
    }

    public void setmLastClickTime(long mLastClickTime) {
        this.mLastClickTime = mLastClickTime;
    }

    // </editor-fold>
}