package com.testproject.sign.cameratest.base_ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.testproject.sign.cameratest.R;

import java.util.Stack;

public class AppFragmentManager {

    private static final String LOG_TAG = AppFragmentManager.class.getSimpleName();
    private AbstractActivity activity;
    protected FragmentManager fragmentManager;
    private View container;

    public enum ANIMATION_TYPE {DEFAULT, SLIDE_UP, NONE, FADE, FREEZE}

    public enum TRANSACTION_TYPE {FORWARD, BACKWARD}

    private Stack<AbstractFragment> stack;
    private AbstractFragment previousFragmentNotInStack;
    private AbstractFragment importantFragmentOpenedInLockMode;


    public Stack<AbstractFragment> getStack() {
        return stack;
    }

    public void init(AbstractActivity activity, View container) {
        this.activity = activity;
        this.fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager.getFragments() != null && !fragmentManager.getFragments().isEmpty()) {
            Log.e(LOG_TAG, "init: fragmentManager.getFragments isn't empty!");
            for (Fragment fragm : fragmentManager.getFragments()) {
                if (fragm != null) {
                    Log.e(LOG_TAG, "init: fragment name is " + fragm.getClass().getSimpleName());
//                for (Fragment fragment : fragmentManager.getFragments()) {
                    fragmentManager.beginTransaction().remove(fragm).commit();
//                }
                }
            }
        }
        this.container = container;

        stack = new Stack<>();

        previousFragmentNotInStack = null;
        importantFragmentOpenedInLockMode = null;

    }

    // <editor-fold desc="public">

    void showFragment(final AbstractFragment fragment) {
        showFragment(fragment, true, ANIMATION_TYPE.DEFAULT);
    }

    void showFragment(final AbstractFragment fragment, final boolean addToBackStack, final ANIMATION_TYPE type) {
        Log.v(LOG_TAG, "showFragment " + fragment.getClass().getName());
        performShowFragmentActions(fragment, addToBackStack, type);
    }

    private void performShowFragmentActions(AbstractFragment fragment) {
        showFragment(fragment, true, fragment.getAnimationType());
        getCurrentFragment();
    }

    private void performShowFragmentActions(final AbstractFragment fragment, final boolean addToBackStack, final ANIMATION_TYPE type) {
        if (getCurrentFragment() == fragment || DialogManager.getInstance().isDialogVisible() || activity.isFinishing()) {
            Log.e(LOG_TAG, "can not show fragment!");
            return;
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View container = getFragContainer(fragment);
                container.bringToFront();
                changeFragment(fragment, addToBackStack, type, TRANSACTION_TYPE.FORWARD);
            }
        });
    }

    private void changeFragment(AbstractFragment fragment, boolean addToBackStack, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {

        ANIMATION_TYPE toShowType = type;
        ANIMATION_TYPE toHideType = type;

        AbstractFragment tmpCurrentFragment = getCurrentFragment();

        if (tmpCurrentFragment != null) {
            toShowType = getToShowAnimType(getCurrentFragment(), fragment);
            toHideType = getToHideAnimType(getCurrentFragment(), fragment);
        }

        if (tmpCurrentFragment == null) {
            add(fragment, type, direction);
        } else {
            if (fragment.isStaticContent()) {
                if (tmpCurrentFragment.isStaticContent()) {
                    hide(tmpCurrentFragment, toHideType, direction);
                } else {
                    remove(tmpCurrentFragment, toHideType, direction);
                }
                showOrAdd(fragment, toShowType, direction);
            } else {
                if (tmpCurrentFragment.isStaticContent()) {
                    hide(tmpCurrentFragment, toHideType, direction);
                } else {
                    remove(tmpCurrentFragment, toHideType, direction);
                }
                add(fragment, toShowType, direction);
            }
        }
        if (addToBackStack) {
            stack.push(fragment);
            previousFragmentNotInStack = null;
        } else {
            previousFragmentNotInStack = fragment;
        }

//        handleDrawer(fragment);
    }

    private void changeFragment(AbstractFragment fragment, AbstractFragment previous, boolean addToBackStack, TRANSACTION_TYPE direction) {

        ANIMATION_TYPE toShowType = getToShowAnimType(previous, fragment);
        ANIMATION_TYPE toHideType = getToHideAnimType(previous, fragment);

        if (getCurrentFragment() == null) {
            add(fragment, toShowType, direction);
        } else {
            if (fragment.isStaticContent()) {
                if (previous.isStaticContent()) {
                    hide(previous, toHideType, direction);
                } else {
                    remove(previous, toHideType, direction);
                }
                showOrAdd(fragment, toShowType, direction);
            } else {
                if (previous.isStaticContent()) {
                    hide(previous, toHideType, direction);
                } else {
                    remove(previous, toHideType, direction);
                }
                add(fragment, toShowType, direction);
            }
        }

        previousFragmentNotInStack = null;

        if (addToBackStack) {
            stack.push(fragment);
        }/* else {
            previousFragmentNotInStack = fragment;
        }*/

//        handleDrawer(fragment);
    }

    private ANIMATION_TYPE getToShowAnimType(AbstractFragment previous, AbstractFragment fragment) {

        if (previous.getAnimationType() != ANIMATION_TYPE.DEFAULT &&
                fragment.getAnimationType() != ANIMATION_TYPE.DEFAULT) {

            return fragment.getAnimationType();
        }

        if (previous.getAnimationType() != ANIMATION_TYPE.DEFAULT) {
            return ANIMATION_TYPE.NONE;
        } else {
            return fragment.getAnimationType();
        }
    }

    private ANIMATION_TYPE getToHideAnimType(AbstractFragment previous, AbstractFragment fragment) {

        if (previous.getAnimationType() != ANIMATION_TYPE.DEFAULT &&
                fragment.getAnimationType() != ANIMATION_TYPE.DEFAULT) {

            return previous.getAnimationType();
        }

        if (fragment.getAnimationType() != ANIMATION_TYPE.DEFAULT) {
            return ANIMATION_TYPE.FREEZE;
        } else {
            return previous.getAnimationType();
        }
    }

//    protected void preloadFragment(AbstractFragmentOld fragment) {
//        add(fragment, fragment.getAnimationType());
//        hide(fragment, fragment.getAnimationType());
//    }

    public void goBack() {
        if (DialogManager.getInstance().isDialogVisible() || activity == null || activity.isFinishing()) {
            Log.e(LOG_TAG, "can't goBack!");
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (stack.size() <= 1 && previousFragmentNotInStack == null) {
                    activity.finish();
                } else {
                    AbstractFragment previous = previousFragmentNotInStack == null ? stack.pop() : previousFragmentNotInStack;
                    AbstractFragment next = stack.pop();
                    stack.push(next);
                    //to avoid show already showing fragment
                    if (previous == next) {
                        goBack();
                        return;
                    }
                    changeFragment(next, previous, false, TRANSACTION_TYPE.BACKWARD);
                    Log.v(LOG_TAG, "stck size = " + stack.size());
                }
            }
        });
    }

    // </editor-fold>

    // <editor-fold desc="listeners">

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.v(LOG_TAG, "onKeyUp " + keyCode);
        if (stack.size() != 0) {
            AbstractFragment peek = getCurrentFragment();
            if (peek != null) {
                boolean consumed = peek.onKeyUp(keyCode, event);
                if (consumed) {
                    Log.e(LOG_TAG, "key UP event consumed");
                    return true;
                }
            }
        }
        return false;
    }

    public void onConfigurationChanged() {
        AbstractFragment currentFragment = getCurrentFragment();
        if (currentFragment != null) {
            currentFragment.onConfigurationChanged();
        }
    }

    // </editor-fold>

    // <editor-fold desc="getters & setters">

    public AbstractFragment getCurrentFragment() {
        if (previousFragmentNotInStack != null) {
            Log.v(LOG_TAG, "current fragment (not in backstack)" + previousFragmentNotInStack.getClass().getSimpleName());
            return previousFragmentNotInStack;
        }
        if (stack == null || stack.size() == 0) {
            return null;
        }
        Log.v(LOG_TAG, "current fragment " + stack.peek());
        return stack.peek();
    }

    public AbstractActivity getCurrentActivity() {
        if (previousFragmentNotInStack != null) {
            Log.v(LOG_TAG, "current fragment (not in backstack)" + previousFragmentNotInStack.getClass().getSimpleName());
            return previousFragmentNotInStack.getCurrentActivity();
        }
        if (stack == null || stack.size() == 0) {
            return null;
        }
        Log.v(LOG_TAG, "current fragment " + stack.peek());
        return stack.peek().getCurrentActivity();
    }

    public View getFragContainer(final Fragment fragment) {
        return container;
    }

    public AbstractActivity getActivity() {
        return activity;
    }

    // </editor-fold>

    // <editor-fold desc="fragment transactions">
    public void show(Fragment fragment, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {
        Log.v(LOG_TAG, "show " + fragment.getClass().getSimpleName());
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        showAnimation(transaction, type, direction);
        transaction.show(fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public void hide(Fragment fragment, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {
        Log.v(LOG_TAG, "hide " + fragment.getClass().getSimpleName());
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        showAnimation(transaction, type, direction);
        transaction.hide(fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public void replace(Fragment fragment, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {
        Log.v(LOG_TAG, "replace " + fragment.getClass().getSimpleName());
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        showAnimation(transaction, type, direction);
        transaction.replace(container.getId(), fragment, fragment.getClass().getName());
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

    }

    public void add(Fragment fragment, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {
        Log.v(LOG_TAG, "add " + fragment.getClass().getSimpleName());
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        showAnimation(transaction, type, direction);
        transaction.add(container.getId(), fragment, fragment.getClass().getName());
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public void remove(Fragment fragment, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {
        Log.v(LOG_TAG, "remove" + fragment.getClass().getSimpleName());
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        showAnimation(transaction, type, direction);
        transaction.remove(fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public void showOrAdd(AbstractFragment fragment, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {
        Log.v(LOG_TAG, "showOrAdd");
        if (fragmentManager.findFragmentByTag(fragment.getClass().getName()) != null) {
            show(fragment, type, direction);
        } else {
            add(fragment, type, direction);
        }
    }

    private void showAnimation(FragmentTransaction transaction, ANIMATION_TYPE type, TRANSACTION_TYPE direction) {

        switch (type) {
            case SLIDE_UP:
                if (stack.size() != 0) {
                    transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_top, R.anim.enter_from_left, R.anim.exit_to_top);
                }
                break;

            case NONE:
                transaction.setCustomAnimations(0, 0, 0, 0);
                break;

            case FADE:
                transaction.setCustomAnimations(R.anim.fade_out, R.anim.fade_in, R.anim.fade_out, R.anim.fade_in);
                break;

            case FREEZE:
                transaction.setCustomAnimations(R.anim.freeze_translation, R.anim.freeze_translation, R.anim.freeze_translation, R.anim.freeze_translation);
                break;

            default:
                if (stack.size() != 0) {
                    if (direction == TRANSACTION_TYPE.FORWARD) {
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
                    } else {
                        transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_left, R.anim.exit_to_right);
                    }

                }
                break;
        }
    }

//    private void handleDrawer(AbstractFragment currentFragment) {
//        if (activity.getClass().equals(AlternativeMainActivity.class)) {
//            if (currentFragment.getClass().equals(CoreSignFragmentManager.class)) {
//                ((AlternativeMainActivity) activity).showTabs();
//                ((AlternativeMainActivity) activity).unlockNavigationDrawer();
//            } else {
//                ((AlternativeMainActivity) activity).hideTabs();
//                ((AlternativeMainActivity) activity).lockNavigationDrawer();
//            }
//        }
//    }

    public void finish() {
        stack.clear();
        activity = null;
        container = null;
        fragmentManager = null;
    }

    // </editor-fold>

}
