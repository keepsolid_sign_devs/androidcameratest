package com.testproject.sign.cameratest.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.testproject.sign.cameratest.util.DisplayModule;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {

    private int vertSpacing;
    private int horSpacing;
    private int itemCount;
    private Context context;

    public SpacesItemDecoration(@NonNull Context context, int verticalSpacing, int horizontalSpacing, int itemCount) {
        this.context = context;
        this.vertSpacing = verticalSpacing;
        this.horSpacing = horizontalSpacing;
        this.itemCount = itemCount;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        int position = parent.getChildViewHolder(view).getAdapterPosition();
        if (layoutManager instanceof GridLayoutManager) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
            int cols = gridLayoutManager.getSpanCount();
            int rows = itemCount / cols;
            double displayWidth = DisplayModule.getDisplayWidth(context);
            double viewWidth = view.getLayoutParams().width;
            int sidesMargins = (int) ((displayWidth - (double) cols * viewWidth) / ((double) cols * 2d));
            outRect.left = sidesMargins;
            outRect.right = sidesMargins;
            outRect.top = horSpacing;
            outRect.bottom = position / cols == rows - 1 ? horSpacing : 0;
        } else {
            outRect.set(vertSpacing, vertSpacing, vertSpacing, vertSpacing);
        }
    }
}
