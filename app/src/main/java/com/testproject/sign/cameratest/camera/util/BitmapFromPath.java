package com.testproject.sign.cameratest.camera.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class BitmapFromPath {

    public static Bitmap getFromPath(String path) {
        Bitmap result = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        if (path != null && path.length() > 0) {
            result = BitmapFactory.decodeFile(path, options);
        }
        return result;
    }

    public static Bitmap getFromUri(Context context, Uri uri) {
        InputStream imageStream = null;
        Bitmap bitmap = null;
        try {
            imageStream = context.getContentResolver().openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(imageStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (imageStream != null)
                try {
                    imageStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return bitmap;
    }
}
