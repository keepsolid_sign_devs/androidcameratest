package com.testproject.sign.cameratest.camera.util;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by yoba on 21.04.17.
 */

public class DiskBitmapCache {

    private static volatile DiskBitmapCache instance;

    private long maxSize;
    private String cachePath;

    private DiskBitmapCache() {

    }

    public static DiskBitmapCache getInstance() {
        if (instance == null) {
            synchronized (DiskBitmapCache.class) {
                instance = new DiskBitmapCache();
            }
        }

        return instance;
    }

    public void init(long maxSize, String cachePath) {
        this.maxSize = maxSize;
        this.cachePath = cachePath + "/disk_bitmap_cache/";
    }

    public Bitmap get(String key) {
        return BitmapFromPathOpenCV.getFromPath(getImgPath(key));
    }

    public void put(String key, Bitmap bitmap, int serializationQuality) {
        int newWidth = bitmap.getWidth() * serializationQuality / 100;
        int newHeight = bitmap.getHeight() * serializationQuality / 100;
        android.util.Log.e("lool", "put: " + newWidth + "x" + newHeight);

        Bitmap bitmapToSave = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        BitmapToFileOpenCV.saveImageAsPNG(bitmapToSave, cachePath, key, 0);
        bitmapToSave.recycle();
    }

    public void put(String key, Bitmap bitmap) {
        BitmapToFileOpenCV.saveImageAsPNG(bitmap, cachePath, key, 0);
    }

    public boolean delete(String key) {
        File file = new File(getImgPath(key));
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public String getImgPath(String key) {
        return cachePath + key + FileSystemHelper.PNG_EXTENSION;
    }

    public void dropCache() {
        File cacheFolder = new File(cachePath);
        if (cacheFolder.exists() && cacheFolder.isDirectory()) {
            for (File cacheImg : cacheFolder.listFiles()) {
                cacheImg.delete();
            }
            cacheFolder.delete();
        }
    }

    public long getSize() {
        File cacheFolder = new File(cachePath);
        long totalSize = 0;
        if (cacheFolder.exists() && cacheFolder.isDirectory()) {
            for (File cacheImg : cacheFolder.listFiles()) {
                totalSize += cacheImg.length();
            }
        }
        return totalSize;
    }

    public long getMaxSize() {
        return maxSize;
    }

}
