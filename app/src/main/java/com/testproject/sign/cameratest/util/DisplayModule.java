package com.testproject.sign.cameratest.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class DisplayModule {

	public static Boolean isTablet = null;

	public static int getDisplayWidth(Context context) {
		return getDisplaySize(context).x;
	}

	public static int getDisplayHeight(Context context) {
		return getDisplaySize(context).y;
	}

	public static int getLowestSize(Context context) {
		return getDisplayWidth(context) < getDisplayHeight(context)
				? getDisplayWidth(context)
						: getDisplayHeight(context);
	}

	private static Point getDisplaySize(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		return size;
	}

	public static boolean isTablet(Activity activity) {
		if (isTablet == null) {
			double tabletMinScreenInches = 6;
			DisplayMetrics dm = new DisplayMetrics();
			activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
			int width = dm.widthPixels;
			int height = dm.heightPixels;
			int dens = dm.densityDpi;
			double wi = (double) width / (double) dens;
			double hi = (double) height / (double) dens;
			double x = Math.pow(wi, 2);
			double y = Math.pow(hi, 2);
			double screenInches = Math.sqrt(x + y);
			isTablet = screenInches > tabletMinScreenInches;
		}
		return isTablet;
	}

	public static int getScreenOrientation(Activity activity) {
		Display getOrient = activity.getWindowManager().getDefaultDisplay();
		int orientation;
		if(getOrient.getWidth() < getOrient.getHeight()) {
			orientation = Configuration.ORIENTATION_PORTRAIT;
		} else {
			orientation = Configuration.ORIENTATION_LANDSCAPE;
		}
		return orientation;
	}
}
