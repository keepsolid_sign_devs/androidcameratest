package com.testproject.sign.cameratest.base_ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.SystemClock;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.testproject.sign.cameratest.R;
import com.testproject.sign.cameratest.listeners.EditDialogListener;

public class DialogManager {
    private static final String LOG_TAG = DialogManager.class.getSimpleName();
    private static DialogManager instance;
    private Activity activity;

    private ProgressDialog pDialog;
    private AlertDialog dialog;

    public void init(Activity activity) {
        Log.v(LOG_TAG, "init " + activity.getClass().getSimpleName());
        if (this.activity == null || this.activity != activity) {
            this.activity = activity;
            this.pDialog = null;
            this.dialog = null;
        }
    }

    public void onDestroyActivity() {
        hideAlert();
    }

    private DialogManager() {
    }

    public static synchronized DialogManager getInstance() {
        if (instance == null) {
            instance = new DialogManager();
        }
        return instance;
    }

    // </editor-fold>

    // <editor-fold desc="common alert">

    public void hideAlert() {
        if (activity == null) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isDialogVisible()) {
                    dialog.dismiss();
                }
            }
        });
    }

    public void showErrorAlert(final int reason) {
        showErrorAlert(reason, null);
    }

    public void showErrorAlert(String reason) {
        showErrorAlert(reason, null);
    }

    public void showErrorAlert(final int reason, final DialogInterface.OnClickListener onClickListener) {
        showErrorAlert(getString(reason), onClickListener);
    }

    public void showErrorAlert(final String reason, final DialogInterface.OnClickListener onClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(R.string.S_ERROR), reason, getString(R.string.S_CLOSE), onClickListener, true);
            }
        });
    }

    public void showWarningAlert(final int reason) {
        showWarningAlert(reason, null);
    }

    public void showWarningAlert(String reason) {
        showWarningAlert(reason, null);
    }

    public void showWarningAlert(@StringRes final int reason, final DialogInterface.OnClickListener onClickListener) {
        showWarningAlert(getString(reason), onClickListener);
    }

    public void showWarningAlert(final String reason, final DialogInterface.OnClickListener onClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(R.string.S_ATTENTION), reason,
                        getString(R.string.S_CLOSE), getString(R.string.S_OK), null, onClickListener);
            }
        });
    }

    public void showWarningAlert(@StringRes final int reason,
                                 @StringRes final int neutralBtn,
                                 @StringRes final int confirmationBtn,
                                 final DialogInterface.OnClickListener onNeutralClickListener,
                                 final DialogInterface.OnClickListener onConfirmationClickListener) {
        showDialog(R.string.S_ATTENTION, reason,
                        R.string.S_CLOSE, neutralBtn, confirmationBtn, null,
                        onNeutralClickListener, onConfirmationClickListener);
    }

    // </editor-fold>

    // <editor-fold desc="dialogs">


    public void showDialog(final String title, final String message, final int cancelBtn, final DialogInterface.OnClickListener onClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(title, message, getString(cancelBtn), onClickListener, false);
            }
        });
    }

    public void showDialog(final int title, final String message, final int cancelBtn, final DialogInterface.OnClickListener onClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(title), message, getString(cancelBtn), onClickListener, false);
            }
        });
    }

    public void showDialog(final int title, final View contentView, final int cancelBtn, final int positiveBtn,
                           final DialogInterface.OnClickListener onCancelClickListener,
                           final DialogInterface.OnClickListener onConfirmationClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(title), contentView, getString(cancelBtn), getString(positiveBtn), onCancelClickListener, onConfirmationClickListener);
            }
        });
    }

    public void showDialog(final int title, final int message, final int cancelBtn, final DialogInterface.OnClickListener onClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(title), getString(message), getString(cancelBtn), onClickListener, false);
            }
        });
    }


    public void showDialog(final int title, final int message, final int cancelBtn, final int positiveBtn,
                           final DialogInterface.OnClickListener onCancelClickListener,
                           final DialogInterface.OnClickListener onConfirmationClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(title), getString(message), getString(cancelBtn), getString(positiveBtn),
                        onCancelClickListener, onConfirmationClickListener);
            }
        });
    }

    public void showDialog(final int title, final String message, final int cancelBtn, final int positiveBtn,
                           final DialogInterface.OnClickListener onCancelClickListener,
                           final DialogInterface.OnClickListener onConfirmationClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(title), message, getString(cancelBtn), getString(positiveBtn),
                        onCancelClickListener, onConfirmationClickListener);
            }
        });
    }

    public void showDialog(@StringRes final int title, @StringRes final int message,
                           @StringRes final int cancelBtn, @StringRes final int neutralBtn, @StringRes final int positiveBtn,
                           final DialogInterface.OnClickListener onCancelClickListener,
                           final DialogInterface.OnClickListener onNeutralClickListener,
                           final DialogInterface.OnClickListener onConfirmationClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialog(getString(title), getString(message), getString(cancelBtn), getString(neutralBtn), getString(positiveBtn),
                        onCancelClickListener, onNeutralClickListener, onConfirmationClickListener);
            }
        });
    }

    /**
     * Method that creates standard material materialDialog but with given margin between buttons.
     *
     * @param margin A margin you would like to add to buttons
     */
    public void showDialogWithButtonMargins(final int margin,
                                            final int title, final int message, final int cancelBtn, final int positiveBtn,
                                            final DialogInterface.OnClickListener onCancelClickListener,
                                            final DialogInterface.OnClickListener onConfirmationClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialogWithButtonMargins(margin, getString(title), getString(message), getString(cancelBtn), getString(positiveBtn),
                        onCancelClickListener, onConfirmationClickListener);
            }
        });

    }

    public void showDialogWithButtonMargins(final int margin,
                                            final int title, final int message, final int cancelBtn, final int neutralBtn, final int positiveBtn,
                                            final DialogInterface.OnClickListener onCancelClickListener,
                                            final DialogInterface.OnClickListener onNeutralClickListener,
                                            final DialogInterface.OnClickListener onConfirmationClickListener) {
        if (activity == null) {
            return;
        }
        hideAlert();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = createDialogWithButtonMargins(margin, getString(title), getString(message), getString(cancelBtn), getString(neutralBtn), getString(positiveBtn),
                        onCancelClickListener, onNeutralClickListener, onConfirmationClickListener);
            }
        });

    }

    // </editor-fold>

    // <editor-fold desc="keyboard">

    public void showKeyboard(Context context, final EditText editText) {
        Log.d(LOG_TAG, "showKeyboard");
        editText.postDelayed(new Runnable() {

            public void run() {
                editText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                editText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));

            }
        }, 200);
    }

    public void hideKeyboard(final Activity context, final EditText[] editTexts) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                for (int i = 0; i < editTexts.length; i++) {
                    imm.hideSoftInputFromWindow(editTexts[i].getWindowToken(), 0);
                }
            }
        });
    }

    // </editor-fold>

    // <editor-fold desc="getters & setters">

    public boolean isDialogVisible() {
        return dialog != null && dialog.isShowing();
    }

    // </editor-fold>

    // <editor-fold desc="private">

    private String getString(int message) {
        if (activity != null) {
            return activity.getString(message);
        }
        return "";
    }

    private ProgressDialog getProgressDialog() {
        return pDialog;
    }

    private AlertDialog createDialog(String title, String message, String cancelBtn, final DialogInterface.OnClickListener onClickListener,
                                     boolean cancelable) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity, R.style.AlertDialogCustom);
        if (title != null) {
            alert.setTitle(title);
        }
        if (message != null) {
            alert.setMessage(message);
        }
        alert.setPositiveButton(cancelBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onClickListener != null) {
                    onClickListener.onClick(dialog, which);
                }
            }
        });
        AlertDialog dialog = alert.create();
        dialog.setCancelable(cancelable);
        if (activity != null && !activity.isFinishing()) {
            dialog.show();
        }
        return dialog;
    }

    public AlertDialog createDialog(String title, View contentView, String cancelBtn, String positiveBtn,
                                    final DialogInterface.OnClickListener onCancelClickListener,
                                    final DialogInterface.OnClickListener onConfirmationClickListener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity, R.style.AlertDialogCustom);


        if (!TextUtils.isEmpty(title)) {
            alert.setTitle(title);
        }

        alert.setNegativeButton(cancelBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onCancelClickListener != null) {
                    onCancelClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onConfirmationClickListener != null) {
                    onConfirmationClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setView(contentView);
        final AlertDialog dialog = alert.create();
        dialog.setCancelable(false);
        if (activity != null && !activity.isFinishing()) {
            dialog.show();
        }
        return dialog;
    }


    private AlertDialog createDialog(String title, String message, String cancelBtn, String positiveBtn,
                                     final DialogInterface.OnClickListener onCancelClickListener,
                                     final DialogInterface.OnClickListener onConfirmationClickListener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity, R.style.AlertDialogCustom);


        if (!TextUtils.isEmpty(title)) {
            alert.setTitle(title);
        }

        if (!TextUtils.isEmpty(message)) {
            alert.setMessage(message);
        }

        alert.setNegativeButton(cancelBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onCancelClickListener != null) {
                    onCancelClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onConfirmationClickListener != null) {
                    onConfirmationClickListener.onClick(dialog, which);
                }
            }
        });
        AlertDialog dialog = alert.create();
        dialog.setCancelable(false);
        if (activity != null && !activity.isFinishing()) {
            dialog.show();
        }
        return dialog;
    }

    private AlertDialog createDialog(String title, String message, String cancelBtn, String neutralBtn, String positiveBtn,
                                     final DialogInterface.OnClickListener onCancelClickListener,
                                     final DialogInterface.OnClickListener onNeutralClickListener,
                                     final DialogInterface.OnClickListener onConfirmationClickListener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity, R.style.AlertDialogCustom);
        if (title != null) {
            alert.setTitle(title);
        }
        if (message != null) {
            alert.setMessage(message);
        }
        alert.setNegativeButton(cancelBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onCancelClickListener != null) {
                    onCancelClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setNeutralButton(neutralBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onNeutralClickListener != null) {
                    onNeutralClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onConfirmationClickListener != null) {
                    onConfirmationClickListener.onClick(dialog, which);
                }
            }
        });
        AlertDialog dialog = alert.create();
        dialog.setCancelable(false);
        if (activity != null && !activity.isFinishing()) {
            dialog.show();
        }
        return dialog;
    }

    private AlertDialog createDialogWithButtonMargins(final int margin,
                                                      String title, String message, String cancelBtn, String positiveBtn,
                                                      final DialogInterface.OnClickListener onCancelClickListener,
                                                      final DialogInterface.OnClickListener onConfirmationClickListener) {

        AlertDialog.Builder alert = new AlertDialog.Builder(activity, R.style.AlertDialogCustom);

        if (!TextUtils.isEmpty(title)) {
            alert.setTitle(title);
        }

        if (!TextUtils.isEmpty(message)) {
            alert.setMessage(message);
        }

        alert.setNegativeButton(cancelBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onCancelClickListener != null) {
                    onCancelClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onConfirmationClickListener != null) {
                    onConfirmationClickListener.onClick(dialog, which);
                }
            }
        });
        final AlertDialog alertDialog = alert.create();
        alertDialog.setCancelable(false);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                applyButtonMargins(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE), margin);
            }
        });
        if (activity != null && !activity.isFinishing()) {
            alertDialog.show();
        }
        return alertDialog;
    }

    private AlertDialog createDialogWithButtonMargins(final int margin,
                                                      String title, String message, String cancelBtn, String neutralBtn, String positiveBtn,
                                                      final DialogInterface.OnClickListener onCancelClickListener,
                                                      final DialogInterface.OnClickListener onNeutralClickListener,
                                                      final DialogInterface.OnClickListener onConfirmationClickListener) {

        AlertDialog.Builder alert = new AlertDialog.Builder(activity, R.style.AlertDialogCustom);
        if (title != null) {
            alert.setTitle(title);
        }
        if (message != null) {
            alert.setMessage(message);
        }
        alert.setNegativeButton(cancelBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onCancelClickListener != null) {
                    onCancelClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setNeutralButton(neutralBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onNeutralClickListener != null) {
                    onNeutralClickListener.onClick(dialog, which);
                }
            }
        });
        alert.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onConfirmationClickListener != null) {
                    onConfirmationClickListener.onClick(dialog, which);
                }
            }
        });
        final AlertDialog alertDialog = alert.create();
        alertDialog.setCancelable(false);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                applyButtonMargins(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE), margin);
            }
        });
        if (activity != null && !activity.isFinishing()) {
            dialog.show();
        }
        return dialog;
    }

    public void applyButtonMargins(Button dialogButton, int margin) {
        if (dialogButton == null)
            return;
        if (!(dialogButton.getParent() instanceof LinearLayout))
            return;
        // Workaround for buttons too large in alternate languages.
        final LinearLayout linearLayout = (LinearLayout) dialogButton.getParent();
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(linearLayout.getPaddingLeft(), 0, linearLayout.getPaddingRight(),
                linearLayout.getPaddingBottom());
        for (int i = 0; i < linearLayout.getChildCount(); ++i) {
            if (linearLayout.getChildAt(i) instanceof Button) {
                final Button child = (Button) linearLayout.getChildAt(i);
                child.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
                final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) child.getLayoutParams();
                params.setMarginStart(margin);
                params.setMarginEnd(margin);
                child.setLayoutParams(params);
            }
        }
    }
    // </editor-fold>

    public void showEditableDialog(int title, String message, String hint, boolean isPassword, final EditDialogListener onEditDialogListener) {
        final View messageDialogView = activity.getLayoutInflater().inflate(R.layout.fragment_info_enter_text_dialog, null);
        final EditText dialogMessage = (EditText) messageDialogView.findViewById(R.id.et_message);
        dialogMessage.setText(message);
        if (isPassword) {
            dialogMessage.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            dialogMessage.setLines(1);
        }
        dialogMessage.setHint(hint);
        dialogMessage.post(new Runnable() {
            @Override
            public void run() {
                dialogMessage.requestFocusFromTouch();
                InputMethodManager lManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                lManager.showSoftInput(dialogMessage, 0);
                dialogMessage.setSelection(dialogMessage.getText().toString().length());
            }
        });

        DialogManager.getInstance().showDialog(title,
                messageDialogView,
                R.string.S_CANCEL,
                R.string.S_CONFIRM,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogMessage.clearFocus();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onEditDialogListener.onConfirmed(dialogMessage.getText().toString().trim());
                    }
                });
    }

}
