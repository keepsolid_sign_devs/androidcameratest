package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;

import com.testproject.sign.cameratest.camera.ui.EditImageView;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;


/**
 * Created by prog on 03.08.16.
 */
public class RotationEffect extends Effect {

    public RotationEffect(double value) {
        super(value, EffectsConstants.ROTATION_BITMAP);
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.ROTATION;
    }

    @Override
    public Bitmap execute(Bitmap originalImage) {
        switch (type) {
            case EffectsConstants.ROTATION_IMAGEVIEW:
            case EffectsConstants.ROTATION_BITMAP:
                return rotateBitmap(originalImage, (int) value);
        }
        return null;
    }

    private Bitmap rotateBitmap(Bitmap bitmap, int angle) {
        Mat src = new Mat();
        Mat dst = new Mat(bitmap.getHeight(), bitmap.getWidth(), CvType.CV_8UC4);

        Utils.bitmapToMat(bitmap, src);

        switch (angle) {
            case -270:
            case 90:
                Core.flip(src.t(), dst, 1);
                break;
            case -180:
            case 180:
                Core.flip(src, dst, -1);
                break;
            case -90:
            case 270:
                Core.flip(src.t(), dst, 0);
                break;
            case 360:
            case 0:
                dst = src;
                break;
        }

        Bitmap output = Bitmap.createBitmap(dst.width(), dst.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(dst, output);
        bitmap.recycle();

        return output;
    }

    @Override
    public void undo() {

    }
}
