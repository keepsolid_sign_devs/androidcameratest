package com.testproject.sign.cameratest.camera.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.testproject.sign.cameratest.camera.callbacks.OnTuneEventListener;

import java.util.ArrayList;

public class GalleryPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> imgKeys = new ArrayList<>();
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();
    private OnTuneEventListener.TuneMode tuneMode;

    public GalleryPagerAdapter(FragmentManager fm, ArrayList<String> imgKeys, OnTuneEventListener.TuneMode tuneMode) {
        super(fm);
        this.imgKeys = imgKeys;
        this.tuneMode = tuneMode;
    }

    public GalleryPagerAdapter(FragmentManager fm, ArrayList<String> imgKeys) {
        this(fm, imgKeys, OnTuneEventListener.TuneMode.NONE);
    }

    @Override
    public Fragment getItem(int position) {
        return ImageFragment.newInstance(imgKeys.get(position), tuneMode);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return imgKeys.size();
    }

    @Override
    public float getPageWidth(int position) {
        return 0.96f;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void updateImagesKeys(ArrayList<String> imgKeys) {
        this.imgKeys = imgKeys;
        notifyDataSetChanged();
    }
}
