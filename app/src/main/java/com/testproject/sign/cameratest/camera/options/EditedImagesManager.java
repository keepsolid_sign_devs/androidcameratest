package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 11/6/17.
 */

public class EditedImagesManager {

    public static EditedImagesManager instance;

    private Set<EditedImage> editedImages;

    private EditedImagesManager() {
        editedImages = new HashSet<>();
    }

    public static EditedImagesManager getInstance() {
        if (instance == null) {
            instance = new EditedImagesManager();
        }
        return instance;
    }

    public ArrayList<EditedImage> getEditedImages() {
        return new ArrayList<>(editedImages);
    }

    public @Nullable EditedImage get(String imageId) {
        for (EditedImage editedImage : editedImages) {
            if (editedImage.getImageId().equals(imageId)) {
                return editedImage;
            }
        }
        return null;
    }

    public void add(EditedImage image) {
        editedImages.add(image);
    }

    public void add(String imageId) {
        add(new EditedImage(imageId));
    }

    public boolean remove(String imageId) {
        for (EditedImage image : editedImages) {
            if (image.getImageId().equals(imageId)) {
                return editedImages.remove(image);
            }
        }
        return false;
    }

    public boolean remove(EditedImage image) {
        return editedImages.remove(image);
    }

    public void removeAll() {
        editedImages.clear();
    }

}
