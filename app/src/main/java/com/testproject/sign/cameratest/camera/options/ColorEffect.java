package com.testproject.sign.cameratest.camera.options;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import com.testproject.sign.cameratest.camera.ui.EditImageView;

import static com.testproject.sign.cameratest.camera.options.EffectsConstants.COLOR_BINARIZATION;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.COLOR_CVT;
import static com.testproject.sign.cameratest.camera.options.EffectsConstants.COLOR_DEF_VALUE;


/**
 * Created by prog on 03.08.16.
 */
public class ColorEffect extends Effect {

    public ColorEffect(double value) {
        super(value);
        type = value < 25? COLOR_DEF_VALUE : value >= 25 && value < 75?
                COLOR_CVT : COLOR_BINARIZATION;
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.COLOR;
    }

    @Override
    public Bitmap execute(Bitmap originalImage) {
        switch (type) {
            case EffectsConstants.COLOR_CVT:
                return cvt(originalImage);
            case EffectsConstants.COLOR_BINARIZATION:
                return binarization(originalImage);
            case EffectsConstants.COLOR_DEF_VALUE:
                return originalImage;
        }
        return null;
    }

    private Bitmap cvt(Bitmap originalImage) {
        return ImageProcessor.cvt(originalImage);
    }

    private Bitmap binarization(Bitmap originalImage) {
        return ImageProcessor.binarization(originalImage);
    }

    @Override
    public void undo() {

    }
}
