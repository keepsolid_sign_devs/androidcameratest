package com.testproject.sign.cameratest.camera.callbacks;

public interface OnTuneEventListener {

    int DEFAULT_TUNE_VALUE = 50;

    enum TuneMode {
        NONE, COLOR, BRIGHTNESS, CONTRAST
    }

    int onTuneModeChanged(TuneMode mode);
    void onTuneValueChanged(double value, boolean isConfirmed);
    void onConfirmButtonPressed();
    void onCancelTune();
}
